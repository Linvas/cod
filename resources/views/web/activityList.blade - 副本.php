@extends('web.layouts.main')
@section('content')

<div class="tai_header-bot @if($web_route !='web.index') scrollTop @endif">
    <div class="wrapper">
        <div class="account-box">
			         @if (Auth::guard('member')->guest())
                     <form method="POST" action="{{ route('member.login.post') }}">
                    <input type="text" id="login_account" placeholder="账号" class="username" required name="name">
                    <input type="password" id="login_password" placeholder="密码" class="psw" required name="password">
							 <div class="check-code-wrapper">
                            <input type="text" placeholder="请输入验证码" required name="captcha" onfocus="re_captcha_re();">
                            <img onclick="javascript:re_captcha();" src="/kit/captcha/1"
                                     id="c2c98f0de5a04167a9e427d883690ff6" style="display: inline-block;width: 70px;">
                            </a>
                            <script>
                                function re_captcha() {
                                    $url = "/kit/captcha";
                                    $url = $url + "/" + Math.random();
                                    document.getElementById('c2c98f0de5a04167a9e427d883690ff6').src = $url;
                                }
                        </script>
                    </div>
                    <button class="login-box modal-login_submit ajax-submit-btn" type="button">立即登录</button>

                    <a href="/r" class="join-btn">免费开户</a>
                </form>
                </div>
            </div>
        </div>
    @else
                <div class="">
                    <ul class="account-info">
                        <li>
                            帐号 :
                            <span class="account">{{ $_member->name }}</span>
                        </li>
                        <li>
                            账户余额 :
                            <span class="account">${{ $_member->money }}</span>
                        </li>
                    </ul>
                    <ul class="account-nav">
                        <li>
                            <a href="{{ route('member.userCenter') }}">
                                个人中心
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('member.member_drawing') }}">
                                线上取款
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('member.finance_center') }}">
                                线上存款
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('member.indoor_transfer') }}">
                                额度转换
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('member.safe_psw') }}">
                                修改取款密码
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('member.login_psw') }}">
                                修改密碼
                            </a>
                        </li>
                    </ul>
                    <div class="action-box">
                        <a class="logout-btn quit_btn"   href="{{ route('member.logout') }}"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();" >登出</a>
                    </div>
                    <form id="logout-form" action="{{ route('member.logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </div>
            @endif
              </div>
    </div>
</div>
		

<div class="pro-b-bg"></div>
{{--<div class="pro-b-banner container">--}}
    {{--<img src="{{ asset('/web/images/pro-banner.jpg') }}" alt="">--}}
{{--</div>--}}

<div class="container">
    <ul class="pro-ul">
        @foreach($data as $item)
            <li>
                <div>
                    <a href="javascript:;">
                        <img src="{{ $item->title_img }}" alt="">
                    </a>
                </div>
                <h2 class="ov-h">{{ $item->title }}</h2>
                <p>{{ $item->subtitle }}</p>
                <span>
                    活动时间：{{ $item->date_desc }}
                    <a href="{{ route('web.activityDetail', ['id' => $item->id]) }}">
                      活动详情
                      <em>></em>
                    </a>
                </span>
            </li>
        @endforeach
        {{--<li>--}}
            {{--<div>--}}
                {{--<a href="javascript:;">--}}
                    {{--<img src="{{ asset('/web/images/pro-banner-10b.jpg') }}" alt="">--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<h2 class="ov-h">信息认证红利 完成个人信息验证就能轻轻松松领礼金！</h2>--}}
            {{--<p>信息认证红利是会员完成通过相关的条件后，系统给予的现金奖励。用户可到“个人中心”的“自助优惠”页面进行申领操作</p>--}}
            {{--<span>--}}
        {{--活动时间：长期有效--}}
        {{--<a href="javascript:;">--}}
          {{--活动详情--}}
          {{--<em>></em>--}}
        {{--</a>--}}
      {{--</span>--}}
        {{--</li>--}}
        {{--<li>--}}
            {{--<div>--}}
                {{--<a href="javascript:;">--}}
                    {{--<img src="{{ asset('/web/images/pro-banner-10b.jpg') }}" alt="">--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<h2 class="ov-h">信息认证红利 完成个人信息验证就能轻轻松松领礼金！</h2>--}}
            {{--<p>信息认证红利是会员完成通过相关的条件后，系统给予的现金奖励。用户可到“个人中心”的“自助优惠”页面进行申领操作</p>--}}
            {{--<span>--}}
        {{--活动时间：长期有效--}}
        {{--<a href="javascript:;">--}}
          {{--活动详情--}}
          {{--<em>></em>--}}
        {{--</a>--}}
      {{--</span>--}}
        {{--</li>--}}
    </ul>
</div>

<div class="body">

</div>
@endsection