﻿@extends('web.layouts.main')
@section('after.js')
<script type="text/javascript">

if(/AppleWebKit.*mobile/i.test(navigator.userAgent) || (/MIDP|SymbianOS|NOKIA|SAMSUNG|LG|NEC|TCL|Alcatel|BIRD|DBTEL|Dopod|PHILIPS|HAIER|LENOVO|MOT-|Nokia|SonyEricsson|SIE-|Amoi|ZTE/.test(navigator.userAgent))){

    if(window.location.href.indexOf("?mobile")<0){

        try{

            if(/Android|webOS|iPhone|iPod|BlackBerry/i.test(navigator.userAgent)){

                window.location.href="http://m.aaa6688.com";

            }else if(/iPad/i.test(navigator.userAgent)){

                window.location.href="http://m.aaa6688.com";

            }else{

                window.location.href="http://m.aaa6688.com"

            }

        }catch(e){}

    }

}

</script>

<div class="tai_header-bot @if($web_route !='web.index') scrollTop @endif">
    <div class="wrapper">
        <div class="account-box">
			         @if (Auth::guard('member')->guest())
                     <form method="POST" action="{{ route('member.login.post') }}">
                    <input type="text" id="login_account" placeholder="账号" class="username" required name="name">
                    <input type="password" id="login_password" placeholder="密码" class="psw" required name="password">
							 <div class="check-code-wrapper">
                            <input type="text" placeholder="请输入验证码" required name="captcha" onfocus="re_captcha_re();">
                            <img onclick="javascript:re_captcha();" src="/kit/captcha/1"
                                     id="c2c98f0de5a04167a9e427d883690ff6" style="display: inline-block;width: 70px;">
                            </a>
                            <script>
                                function re_captcha() {
                                    $url = "/kit/captcha";
                                    $url = $url + "/" + Math.random();
                                    document.getElementById('c2c98f0de5a04167a9e427d883690ff6').src = $url;
                                }
                        </script>
                    </div>
                    <button class="login-box modal-login_submit ajax-submit-btn" type="button">立即登录</button>

                    <a href="/r" class="join-btn">免费开户</a>
                </form>
                </div>
            </div>
        </div>
    @else
                <div class="">
                    <ul class="account-info">
                        <li>
                            帐号 :
                            <span class="account">{{ $_member->name }}</span>
                        </li>
                        <li>
                            账户余额 :
                            <span class="account">${{ $_member->money }}</span>
                        </li>
                    </ul>
                    <ul class="account-nav">
                        <li>
                            <a href="{{ route('member.userCenter') }}">
                                个人中心
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('member.member_drawing') }}">
                                线上取款
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('member.finance_center') }}">
                                线上存款
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('member.indoor_transfer') }}">
                                额度转换
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('member.safe_psw') }}">
                                修改取款密码
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('member.login_psw') }}">
                                修改密碼
                            </a>
                        </li>
                    </ul>
                    <div class="action-box">
                        <a class="logout-btn quit_btn"   href="{{ route('member.logout') }}"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();" >登出</a>
                    </div>
                    <form id="logout-form" action="{{ route('member.logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </div>
            @endif
           </div>
		   
@endsection
@section('content')
    <div class="tai_banner"></div>

    <div class="tai_news">
        <div class="container">
            <marquee behavior="scroll" direction="left"  onMouseOut="this.start()" onMouseOver="this.stop()">
                @foreach($_system_notices as $v)
                    <span>~{{ $v->title }}~</span>
                    <span>{{ $v->content }}</span>
                @endforeach
            </marquee>
        </div>
    </div>

    <div id="content">
        
<div id="home" ng-controller="LobbiesCtrl">
    
    <div id="game-box">
        <div class="wrapper">
            <ul>
                <li data-img="slot">
                   <a href="{{ route('web.eGame') }}">
                        <div class="title">电子游艺</div>
                        <div class="sub-title">SLOTS</div>
                    </a>
                </li>
                <li data-img="live">
                    <a href="{{ route('web.liveCasino') }}">
                        <div class="title">真人娱乐</div>
                        <div class="sub-title">CASINO</div>
                    </a>
                </li>
                <li data-img="board">
                    <a href="{{ route('web.liveCasino') }}">
                        <div class="title">棋牌游戏</div>
                        <div class="sub-title">CARD</div>
                    </a>
                </li>
                <li data-img="fish">
                    <a href="{{ route('web.catchFish') }}">
                        <div class="title">捕鱼游戏</div>
                        <div class="sub-title">FISH</div>
                    </a>
                </li>
            </ul>
            <p class="slogan"></p>
        </div>
    </div>
    <ul id="process">
        <li><span>开户</span>ACCOUNT</li>
        <li><span>存款</span>DEPOSIT</li>
        <li><span>取款</span>WITHDRAWALS</li>
        <li><span>信誉</span>REPUTATION</li>
        <li><span>尊贵</span>ROYAL</li>
        <li><span>诚信</span>INTEGRITY</li>
    </ul>
    <div id="brand-adventage">
        <ul class="left">
            <li class="item01">
                <h3>亚洲第一博彩品牌</h3>
                <h4>亚洲实力最强劲，最完善的实体赌场</h4>
            </li>
            <li class="item02">
                <h3>存提速度行业顶尖</h3>
                <h4>存款最快30秒，提款1分钟</h4>
            </li>
            <li class="item03">
                <h3>玩家数据安全保障</h3>
                <h4>采用256位SSL加密系统保障安全数据</h4>
            </li>
        </ul>
        <ul class="right">
            <li class="item04">
                <h3>游戏产品丰富完善</h3>
                <h4>欧亚当下热门游戏产品一网打尽</h4>
            </li>
            <li class="item05">
                <h3>大额无忧资金安全</h3>
                <h4>全程担保会员每一笔存款和取款</h4>
            </li>
            <li class="item06">
                <h3>尊贵VIP独享特权</h3>
                <h4>加入VIP即可享受独家尊贵特权</h4>
            </li>
        </ul>
    </div>
</div>

    </div>


    {{--<div id="modal-tit" class="modal modal-login">--}}
        {{--<div class="modal-content" style="width: 650px;height: 414px;padding: 0">--}}
            {{--<a href="" class="close bg-icon" style="top: 0;right: -20px;opacity: 1;background-color: #ccc"></a>--}}
            {{--<a href="http://wpa.qq.com/msgrd?v=3&uin=189983035&site=qq&menu=yes">--}}
                {{--<img src="{{ asset('/web/images/modal-tit.jpg') }}" alt="">--}}
            {{--</a>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<script>--}}
        {{--(function($){--}}
            {{--$(function(){--}}
                {{--$('#modal-tit').modal();--}}
            {{--})--}}
        {{--})(jQuery);--}}
    {{--</script>--}}


<script>


    (function ($) {
        $(function () {
            $(window).on('scroll',function(){
                var windowScroll = $('body').scrollTop();
                console.log(windowScroll);
                if(windowScroll>=310){
                    $('.tai_header-bot').addClass('scrollTop');
                }else{
                    $('.tai_header-bot').removeClass('scrollTop');
                }
            })

            $('.flexslider').flexslider({
                animation: 'fade',
                directionNav: false
            });

            $('.menuBox').on('click', 'li', function () {
                var index = $(this).index();
                var $contentBox_item=$(this).closest('.menuBox').next('.contentBox').find('.contentBox_item');
                $(this).addClass('active').siblings('li').removeClass('active');
                $contentBox_item.removeClass('active').eq(index).addClass('active');
            });

            //公告
            $('#mar0').on('click',function(){
                var notice_index=layer.open({
                    type: 1,
                    title: false,
                    closeBtn: 0,
                    area: ['680px'],
                    skin: 'layui-layer-nobg', //没有背景色
                    shadeClose: true,
                    content: $('.notice_layer')
                });

                $('.notice_layer').on('click','.close',function(){
                    layer.close(notice_index)
                })
            })


            //superslide
           /* jQuery(".txtScroll-top").slide({mainCell:".bd ul",autoPage:true,effect:"top",autoPlay:true,vis:9,pnLoop:true});*/

           //superslide
           var listMarqueIndex=0;
           var listMarqueShow=7;  //要显示的个数
           var listMarqueLength=$('.txtScroll-top .ntb-egzj li').length;  //总共显示的条数
           if(listMarqueLength>listMarqueShow){  //大于要显示的个数才执行动画
            var listMarque=setInterval(function(){

                console.log(listMarqueIndex);
                if(listMarqueLength-listMarqueIndex>listMarqueShow){
                    listMarqueIndex++;
                    $('.txtScroll-top .ntb-egzj li').removeClass('on')
                    $('.txtScroll-top .ntb-egzj li').eq(listMarqueIndex).addClass('on');
                    $('.txtScroll-top .ntb-egzj').animate({
                    "top":"-=45px"
                   },800);
                }else{
                    $('.txtScroll-top .ntb-egzj').animate({"top":'0'})
                    listMarqueIndex=0;
                    $('.txtScroll-top .ntb-egzj li').removeClass('on')
                    $('.txtScroll-top .ntb-egzj li').eq(listMarqueIndex).addClass('on')
                }
               },4000);
           }

            $('.disabled').on('click', function () {
                layer.msg('暂未开放，敬请期待', {icon: 6});
            });
        })
    })(jQuery)
</script>
<script id="jsID" type="text/javascript">

</script>
@endsection
