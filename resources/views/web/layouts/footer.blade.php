    <footer id="footer">
        <div class="wrapper">
            <ul id="footer-contact">
                <li class="tel-m">澳门热线</span>{{ $_system_config->phone1 }}</span></li>
                <li class="tel-int">国际热线</span>{{ $_system_config->phone1 }}</span></li>
                <li class="mail">联系邮箱 </span>{{ $_system_config->qq }}</span></li>
                <li class="onclick="javascript:window.open('{{ $_system_config->service_link }}','','width=1024,height=768')"><span>7X24</span>小时在线客服</li>
            </ul>
            <ul id="footer-nav">
                <li><a href="javascript:;">关于我们</a>&nbsp;&nbsp;/&nbsp;</li>
                <li><a href="javascript:;">联络我们</a>&nbsp;&nbsp;/&nbsp;</li>
                <li><a href="javascript:;">存款帮助</a>&nbsp;&nbsp;/&nbsp;</li>
                <li><a href="javascript:;">取款帮助</a>&nbsp;&nbsp;/&nbsp;</li>
                <li><a href="javascript:;">常见问题</a>&nbsp;&nbsp;/&nbsp;</li>
                <li><a href="javascript:;">合作伙伴</a>&nbsp;&nbsp;/&nbsp;</li>
            </ul>
            <p id="footer-suggest">
                Copyright © {{ $_system_config->site_name }} Reserved
                <span>澳门威尼斯人</span>所提供的产品和服务，是由澳门特别行政区 The Macau Special Administrative Region.授权和监管。选择我们，您将拥有可靠的资金保障和优质的服务。<br />
               Copyright © {{ $_system_config->site_name }} Reserved
            </p>
            <div id="footer-logo"></div>
        </div>
    </footer>
    </div>
</div>