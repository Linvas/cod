@extends('wap.layouts.main')
@section('after.css')
    <link type="text/css" rel="stylesheet" href="{{ asset('/wap/css/font-awesome.min.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('/wap/css/mmenu.all.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('/wap/css/ssc.css') }}"/>
    <link type="text/css" rel="stylesheet" href="{{ asset('/wap/css/member.css') }}">
@endsection
@section('before.js')
    <script type="text/javascript" src="{{ asset('/wap/js/mmenu.all.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/wap/js/member.js') }}"></script>
@endsection
@section('content')
    <div class="container-fluid gm_main">
        <div class="head">
            <a class="f_l" href="javascript:history.go(-1)"><img src="{{ asset('/wap/images/user_back.png') }}" alt=""></a>
            <span>绑定支付宝账号</span>
            <a class="f_r" href="javascript:history.go(-1)" style="visibility: hidden"><img src="{{ asset('/wap/images/user_back.png') }}" alt=""></a>
        </div>
        @include('wap.layouts.aside')
        <div id="type" style="display: none">
            <ul class="g_type">
                <li>
                    @include('wap.layouts.aside_game_list')
                </li>
            </ul>
        </div>

        <div class="userInfo setCard">
            <form action="{{ route('wap.post_bind_ali_pay') }}" method="post" name="form1">
                <dl>
                    <dt>收款人信息</dt>
                    <dd>
                        <div class="pull-left">
                            会员账户
                        </div>
                        <div class="pull-right">
                            {{ $_member->name }}
                        </div>
                    </dd>
                    <dd>
                        <div class="pull-left">收款人姓名</div>
                        <div class="pull-right">{{ $_member->real_name }}</div>
                    </dd>
                </dl>
                <dl class="set_card">
                    <dt>
                        设置支付宝账号 <br>
                        <span><em>*</em>设置后将无法修改信息，请核实后填写</span>
                    </dt>
                    <dd>
                        <div class="pull-left">支付宝账号</div>
                        <input id="pay_num" class="pull-left" type="text" placeholder="" name="ali_pay_account">
                    </dd>
                    <dd>
                        <div class="pull-left">真实姓名</div>
                        <input id="pay_address" class="pull-left" type="text" placeholder="" name="real_name" value="{{ $_member->real_name }}">
                    </dd>
                    <dd>
                        <input type="button" value="确定" class="submit_btn ajax-submit-btn">
                    </dd>
                </dl>
            </form>
        </div>
    </div>
@endsection