@extends('wap.layouts.main')
@section('after.css')

    <link type="text/css" rel="stylesheet" href="{{ asset('/wap/css/main.css') }}">
@endsection
@section('content')
    @include('wap.layouts.header')
    @include('wap.layouts.nav')
    <div class="news_warp">
        <span class="pull-left"><img src="{{ asset('/wap/images/on.png') }}" alt=""></span>
        <em class="pull-left">官方公告</em>
        <div class="container-fluid notice">
            <?php
            $n_str = '';
            foreach ($system_notices as $item)
                $n_str .= $item->content . '&nbsp;&nbsp;&nbsp;';
            ?>
            <div id="news" class="move">{{ $n_str }}</div>
        </div>
    </div>
    <div class="daohang">
        <dl><a href="{{ route('wap.recharge') }}" target="_self" title="账户充值" class="dhye1 clear">
                <dt><img src="{{ asset('/wap/images/nav1.png') }}" alt=""></dt>
                <dd>账户充值</dd>
            </a>
        </dl>
        <dl><a href="{{ route('wap.transfer') }}" target="_self" title="额度转换" class="dhye3 clear">
                <dt><img src="{{ asset('/wap/images/nav2.png') }}" alt=""></dt>
                <dd>额度转换</dd>
            </a>
        </dl>
        <dl><a href="{{ route('wap.userinfo') }}" target="_self" title="会员中心" class="dhye2 clear">
                <dt><img src="{{ asset('/wap/images/nav3.png') }}" alt=""></dt>
                <dd>会员中心</dd>
            </a>
        </dl>
        <div class="clearfix"></div>
    </div>
    <div class="container-fluid games">
        <ul class="clear">

            @if(in_array('AG', $_api_list))
                <li>
                    <a href="{{ route('ag.playGame') }}?devices=1&gametype=8">

                        <img src="{{ asset('/wap/images/game/game_7.png') }}" alt="">
                        <p>AG电游</p>
                    </a>
                </li>
            @endif
            @if(in_array('MG', $_api_list))
                <li>
                    <a href="{{ route('wap.mg_eGame_list') }}">
                        <img src="{{ asset('/wap/images/game/game_5.png') }}" alt="">
                        <p>MG电游</p>
                    </a>
                </li>
            @endif
            @if(in_array('BBIN', $_api_list))
                <li>
                    <a href="{{ route('bbin.playGame') }}?gametype=game&devices=1">
                        <img src="{{ asset('/wap/images/game/game_2.png') }}" alt="">
                        <p>BBIN电游</p>
                    </a>
                </li>
            @endif
            @if(in_array('DT', $_api_list))
                <li>
                    <a href="{{ route('dt.rng_game_list') }}">
                        <img src="{{ asset('/wap/images/game/game_8.png') }}" alt="">
                        <p>DT电游</p>
                    </a>
                </li>
            @endif
            @if(in_array('PT', $_api_list))
                <li>
                    <a href="{{ route('pt.rng_game_list') }}">
                        {{--<img class="isComing" src="{{ asset('/wap/images/game/isComing.png') }}" alt="">--}}
                        <img src="{{ asset('/wap/images/game/game_1.png') }}" alt="">
                        <p>PT电游</p>
                    </a>
                </li>
            @endif
            @if(in_array('AG', $_api_list))
                <li>
                    <a href="{{ route('ag.playGame') }}?devices=1">

                        <img src="{{ asset('/wap/images/game/game_3.png') }}" alt="">
                        <p>AG视讯</p>
                    </a>
                </li>
            @endif
            @if(in_array('BBIN', $_api_list))
                <li>
                    <a href="{{ route('bbin.playGame') }}?gametype=live&devices=1">

                        <img src="{{ asset('/wap/images/game/game_4.png') }}" alt="">
                        <p>BBIN视讯</p>
                    </a>
                </li>
            @endif
            @if(in_array('SA', $_api_list))
                <li>
                    <a href="{{ route('sa.playGame') }}?devices=1">

                        <img src="{{ asset('/wap/images/game/game_6.png') }}" alt="">
                        <p>SA视讯</p>
                    </a>
                </li>
            @endif
            @if(in_array('CG', $_api_list))
                <li>
                    <a href="{{ route('cg.playGame') }}?devices=1">

                        <img src="{{ asset('/wap/images/game/game-live2.png') }}" alt="">
                        <p>CG视讯</p>
                    </a>
                </li>
            @endif
            @if(in_array('BG', $_api_list))
                <li>
                    <a href="{{ route('bg.playGame') }}?devices=1">

                        <img src="{{ asset('/wap/images/game/game-live1.png') }}" alt="">
                        <p>BG视讯</p>
                    </a>
                </li>
            @endif
            @if(in_array('BBIN', $_api_list))
                <li>
                    <a href="{{ route('bbin.playGame') }}?gametype=ltlottery&devices=1">

                        <img src="{{ asset('/wap/images/game/game_11.png') }}" alt="">
                        <p>BBIN彩票</p>
                    </a>
                </li>
            @endif
            @if(in_array('EG', $_api_list))
                <li>
                    <a href="{{ route('eg.playGame') }}?device=1">

                        <img src="{{ asset('/wap/images/game/game-lottory1.png') }}" alt="">
                        <p>EG彩票</p>
                    </a>
                </li>
            @endif
            @if(in_array('YC', $_api_list))
                <li>
                    <a href="{{ route('yc.playGame') }}?device=1">

                        <img src="{{ asset('/wap/images/game/game-lottory1.png') }}" alt="">
                        <p>YC彩票</p>
                    </a>
                </li>
            @endif
            @if(in_array('BBIN', $_api_list))
                <li>
                    <a href="{{ route('bbin.playGame') }}?gametype=ball&devices=1">
                        <img src="{{ asset('/wap/images/game/game_12.png') }}" alt="">
                        <p>BBIN体育</p>
                    </a>
                </li>
            @endif
            @if(in_array('IBC', $_api_list))
                <li>
                    <a href="{{ route('ibc.playGame') }}?devices=1">
                        <img src="{{ asset('/wap/images/game/game-sports1.png') }}" alt="">
                        <p>沙巴体育</p>
                    </a>
                </li>
            @endif
            @if(in_array('AG', $_api_list))
                <li>
                    <a href="{{ route('ag.playGame') }}?devices=1&gameType=6">
                        {{--<img class="isComing" src="{{ asset('/wap/images/game/isComing.png') }}" alt="">--}}
                        <img src="{{ asset('/wap/images/game/game_9.png') }}" alt="">
                        <p>AG捕鱼</p>
                    </a>
                </li>
            @endif
        </ul>
    </div>
@endsection