<table cellspacing="0" cellpadding="0" border="0" class="table table-bordered">
    <tr>
        <td class="tit">
            <img src="{{ asset('/wap/images/game/aside_game1.png') }}" alt="">全部游戏
        </td>
    </tr>
    @if(in_array('AG', $_api_list))
        <tr>
            <td>
                <a href="{{ route('ag.playGame') }}?devices=1&gametype=8">
                    <img src="{{ asset('/wap/images/game/aside_game1.png') }}" alt="">
                    <span>AG电游</span>
                    <i class="icon-angle-right"></i>
                    <img class="game_img" src="{{ asset('/wap/images/game/aside_game_7.png') }}" alt="">
                </a>
            </td>
        </tr>
    @endif
    @if(in_array('MG', $_api_list))
        <tr>
            <td>
                <a href="{{ route('wap.mg_eGame_list') }}">
                    <img src="{{ asset('/wap/images/game/aside_game1.png') }}" alt="">
                    <span>MG电游</span>
                    <i class="icon-angle-right"></i>
                    <img class="game_img" src="{{ asset('/wap/images/game/aside_game_5.png') }}" alt="">
                </a>
            </td>
        </tr>
    @endif
    @if(in_array('BBIN', $_api_list))
        <tr>
            <td>
                <a href="{{ route('bbin.playGame') }}?gametype=game&devices=1">
                    <img src="{{ asset('/wap/images/game/aside_game1.png') }}" alt="">
                    <span>BBIN电游</span>
                    <i class="icon-angle-right"></i>
                    <img class="game_img" src="{{ asset('/wap/images/game/aside_game_2.png') }}" alt="">

                </a>
            </td>
        </tr>
    @endif
    @if(in_array('DT', $_api_list))
        <tr>
            <td>
                <a href="{{ route('dt.rng_game_list') }}">
                    <img src="{{ asset('/wap/images/game/aside_game1.png') }}" alt="">
                    <span>DT电游</span>
                    <i class="icon-angle-right"></i>
                    <img class="game_img" src="{{ asset('/wap/images/game/aside_game_8.png') }}" alt="">
                    {{--<img class="game_tips" src="{{ asset('/wap/images/game/isComing.png') }}" alt="">--}}
                </a>
            </td>
        </tr>
    @endif
    @if(in_array('PT', $_api_list))
        <tr>
            <td>
                <a href="{{ route('pt.rng_game_list') }}">
                    <img src="{{ asset('/wap/images/game/aside_game1.png') }}" alt="">
                    <span>PT电游</span>
                    <i class="icon-angle-right"></i>
                    <img class="game_img" src="{{ asset('/wap/images/game/aside_game_1.png') }}" alt="">
                    {{--<img class="game_tips" src="{{ asset('/wap/images/game/isComing.png') }}" alt="">--}}
                </a>
            </td>
        </tr>
    @endif
    <tr>
        <td class="space"></td>
    </tr>
    {{--<tr>--}}
    {{--<td>--}}
    {{--<a href="{{ route('ag.playGame') }}?id=12">--}}
    {{--<img src="{{ asset('/wap/images/game/aside_game2.png') }}" alt="">--}}
    {{--<span>AG旗舰</span>--}}
    {{--<i class="icon-angle-right"></i>--}}
    {{--<img class="game_img" src="{{ asset('/wap/images/game/aside_game_3.png') }}" alt="">--}}
    {{--</a>--}}
    {{--</td>--}}
    {{--</tr>--}}
    @if(in_array('AG', $_api_list))
        <tr>
            <td>
                <a href="{{ route('ag.playGame') }}?devices=1">
                    <img src="{{ asset('/wap/images/game/aside_game2.png') }}" alt="">
                    <span>AG视讯</span>
                    <i class="icon-angle-right"></i>
                    <img class="game_img" src="{{ asset('/wap/images/game/aside_game_6.png') }}" alt="">
                </a>
            </td>
        </tr>
    @endif
    @if(in_array('BBIN', $_api_list))
        <tr>
            <td>
                <a href="{{ route('bbin.playGame') }}?gametype=live&devices=1">
                    <img src="{{ asset('/wap/images/game/aside_game2.png') }}" alt="">
                    <span>BBIN视讯</span>
                    <i class="icon-angle-right"></i>
                    <img class="game_img" src="{{ asset('/wap/images/game/aside_game_4.png') }}" alt="">
                </a>
            </td>
        </tr>
    @endif
    @if(in_array('SA', $_api_list))
        <tr>
            <td>
                <a href="{{ route('sa.playGame') }}?devices=1">
                    <img src="{{ asset('/wap/images/game/aside_game2.png') }}" alt="">
                    <span>SA视讯</span>
                    <i class="icon-angle-right"></i>
                    <img class="game_img" src="{{ asset('/wap/images/game/aside_game_3.png') }}" alt="">

                </a>
            </td>
        </tr>
    @endif
    @if(in_array('CG', $_api_list))
        <tr>
            <td>
                <a href="{{ route('cg.playGame') }}?devices=1">
                    <img src="{{ asset('/wap/images/game/aside_game2.png') }}" alt="">
                    <span>CG视讯</span>
                    <i class="icon-angle-right"></i>
                    <img class="game_img" src="{{ asset('/wap/images/game/aside_game_10.png') }}" alt="">

                </a>
            </td>
        </tr>
    @endif
    @if(in_array('BG', $_api_list))
        <tr>
            <td>
                <a href="{{ route('bg.playGame') }}?devices=1">
                    <img src="{{ asset('/wap/images/game/aside_game2.png') }}" alt="">
                    <span>BG视讯</span>
                    <i class="icon-angle-right"></i>
                    <img class="game_img" src="{{ asset('/wap/images/game/aside_game_10.png') }}" alt="">

                </a>
            </td>
        </tr>
    @endif
    <tr>
        <td class="space"></td>
    </tr>
    @if(in_array('AG', $_api_list))
        <tr>
            <td>
                <a href="{{ route('ag.playGame') }}?devices=1&gameType=6">
                    <img src="{{ asset('/wap/images/game/aside_game1.png') }}" alt="">
                    <span>AG捕鱼</span>
                    <i class="icon-angle-right"></i>
                    <img class="game_img" src="{{ asset('/wap/images/game/aside_game_9.png') }}" alt="">
                    {{--<img class="game_tips" src="{{ asset('/wap/images/game/isComing.png') }}" alt="">--}}
                </a>
            </td>
        </tr>
    @endif
    @if(in_array('BBIN', $_api_list))
        <tr>
            <td>
                <a href="{{ route('bbin.playGame') }}?gametype=ltlottery&devices=1">
                    <img src="{{ asset('/wap/images/game/aside_game1.png') }}" alt="">
                    <span>BBIN彩票</span>
                    <i class="icon-angle-right"></i>
                    <img class="game_img" src="{{ asset('/wap/images/game/aside_game_11.png') }}" alt="">

                </a>
            </td>
        </tr>
    @endif
    @if(in_array('EG', $_api_list))
        <tr>
            <td>
                <a href="{{ route('eg.playGame') }}?devices=1">
                    <img src="{{ asset('/wap/images/game/aside_game1.png') }}" alt="">
                    <span>EG彩票</span>
                    <i class="icon-angle-right"></i>
                    <img class="game_img" src="{{ asset('/wap/images/game/aside_game_11.png') }}" alt="">

                </a>
            </td>
        </tr>
    @endif
    @if(in_array('BBIN', $_api_list))
        <tr>
            <td>
                <a href="{{ route('bbin.playGame') }}?gametype=ball&devices=1">
                    <img src="{{ asset('/wap/images/game/aside_game1.png') }}" alt="">
                    <span>BBIN体育</span>
                    <i class="icon-angle-right"></i>
                    <img class="game_img" src="{{ asset('/wap/images/game/aside_game_12.png') }}" alt="">

                </a>
            </td>
        </tr>
    @endif
    @if(in_array('IBC', $_api_list))
        <tr>
            <td>
                <a href="{{ route('ibc.playGame') }}?devices=1">
                    <img src="{{ asset('/wap/images/game/aside_game1.png') }}" alt="">
                    <span>沙巴体育</span>
                    <i class="icon-angle-right"></i>
                    <img class="game_img" src="{{ asset('/wap/images/game/aside_game_12.png') }}" alt="">

                </a>
            </td>
        </tr>
    @endif
</table>