@extends('wap.layouts.main')
@section('content')
    @extends('wap.layouts.header')
    <div class="m_container">
        <div class="m_body">
            <div class="m_member-title clear textCenter">
                <a class="pull-left" href="javascript:history.go(-1);">&nbsp;返回</a>
                {{ $data->title }}
            </div>
            <div class="m_userCenter-line"></div>
            <div class="m_activityDetail">
                {!! $data->content !!}
            </div>
        </div>
    </div>
@endsection