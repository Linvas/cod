/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 100121
Source Host           : localhost:3306
Source Database       : qisjk

Target Server Type    : MYSQL
Target Server Version : 100121
File Encoding         : 65001

Date: 2018-02-25 13:28:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for activities
-- ----------------------------
DROP TABLE IF EXISTS `activities`;
CREATE TABLE `activities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '标题',
  `subtitle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '副标题',
  `title_img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '列表图片',
  `content` text COLLATE utf8mb4_unicode_ci COMMENT '活动说明',
  `type` tinyint(4) NOT NULL DEFAULT '3' COMMENT '1返水2红利3充值',
  `money` decimal(8,2) DEFAULT NULL COMMENT '活动所需达到的金额',
  `rate` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '赠送比例',
  `gift_limit_money` decimal(8,2) DEFAULT NULL COMMENT '赠送的上限金额',
  `date_desc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '活动时间描述',
  `start_at` timestamp NULL DEFAULT NULL COMMENT '活动开始时间',
  `end_at` timestamp NULL DEFAULT NULL COMMENT '活动截止时间',
  `on_line` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0上线1下线',
  `is_hot` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0正常1热门',
  `sort` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '排序',
  `title_content` text COLLATE utf8mb4_unicode_ci COMMENT '标题内容',
  `rule_content` text COLLATE utf8mb4_unicode_ci COMMENT '活动规则',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of activities
-- ----------------------------
INSERT INTO `activities` VALUES ('1', '充值活动1', '充值活动1', '/uploads/2017-10-08/5f9d5245dcedb5057a0bd3089556949f375c2315.jpg', '<p>&nbsp;充值活动1活动说明</p><p>充值活动1活动说明</p><p><br/></p>', '3', '100.00', '30%', '500.00', '充值活动1', '2017-10-01 00:00:00', '2017-10-31 02:55:44', '1', '0', '1', '充值活动1<p><br/></p><p>充值活动1</p><p><br/></p><p>充值活动1</p>', '充值活动1活动规则<p>充值活动1活动规则</p><p>充值活动1活动规则</p>', '2017-10-08 02:56:07', '2017-12-22 14:18:38');
INSERT INTO `activities` VALUES ('2', '1', '1', null, null, '1', null, null, null, '长期有效', null, null, '0', '0', '1', null, null, '2017-10-17 16:47:18', '2018-01-08 22:07:24');
INSERT INTO `activities` VALUES ('3', '红利活动', '红利活动', null, null, '2', null, null, null, '长期有效', null, null, '0', '0', '1', null, null, '2017-10-17 16:47:37', '2018-01-08 22:07:28');
INSERT INTO `activities` VALUES ('4', '反水活动', '反水活动', '/uploads/2018-01-08/7e6a50729124909623aea7c61ee20cce933dbe18.png', null, '1', null, null, null, '长期有效', null, null, '0', '0', '1', null, null, '2017-10-17 16:47:59', '2018-01-08 22:11:12');
INSERT INTO `activities` VALUES ('6', '红包随便送', '公司老板带小姨子跑了', '/uploads/2017-10-23/c4cf81583b7184783075c7f938a1492b588f090f.jpg', '<p>添加客服微信就能随意领取彩金</p>', '4', '0.00', '1000', '999999.99', '长期有效', '2017-10-23 00:00:00', '2017-10-30 03:16:35', '0', '0', '1', '<p>公司老板带小姨子跑了，即日起彩金随便送</p>', '<p>24小时随意领取</p>', '2017-10-23 03:20:29', '2018-01-08 22:07:20');
INSERT INTO `activities` VALUES ('7', '这是测试', '这是测试', '/uploads/2018-01-08/6d6de39c9bdd8ec76b38aaa1ba42216e60cdbea6.jpg', '这是测试', '3', '1.00', '1', '2.00', '长期有效', '2018-01-09 00:00:00', '2018-01-18 22:05:39', '0', '0', '1', '<p>这是测试<br/></p>', '<p>这是测试<br/></p>', '2018-01-08 22:10:08', '2018-01-08 22:10:37');

-- ----------------------------
-- Table structure for admin_action_money_log
-- ----------------------------
DROP TABLE IF EXISTS `admin_action_money_log`;
CREATE TABLE `admin_action_money_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '管理员ID',
  `member_id` int(11) NOT NULL COMMENT '会员ID',
  `old_money` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '原余额',
  `new_money` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '修改后的余额',
  `old_fs_money` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '原返水余额',
  `new_fs_money` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '修改后的返水余额',
  `remark` text COLLATE utf8mb4_unicode_ci COMMENT '描述',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of admin_action_money_log
-- ----------------------------

-- ----------------------------
-- Table structure for api
-- ----------------------------
DROP TABLE IF EXISTS `api`;
CREATE TABLE `api` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `api_name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '平台名称',
  `api_title` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '平台描述名称',
  `api_domain` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '请求的基础域名',
  `api_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'key 值',
  `api_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '1',
  `api_money` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '余额',
  `is_demo` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0正式 1测试',
  `prefix` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '账号前缀',
  `on_line` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0上线1下线',
  `sort` int(10) unsigned NOT NULL DEFAULT '10' COMMENT '排序',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of api
-- ----------------------------
INSERT INTO `api` VALUES ('3', 'AG', null, '', '', '', null, null, null, '1023.00', '0', null, '0', '10', '2017-02-03 09:00:51', '2018-01-06 15:39:55');
INSERT INTO `api` VALUES ('4', 'BBIN', null, '', '', '', null, null, null, '101.00', '0', null, '0', '10', '2017-02-03 09:01:51', '2018-01-05 00:08:18');
INSERT INTO `api` VALUES ('5', 'AB', null, null, '', '', null, null, null, '30.00', '0', null, '0', '10', '2017-02-03 09:02:51', '2018-01-04 16:02:35');
INSERT INTO `api` VALUES ('6', 'PT', null, null, '', '', null, null, null, '2.90', '0', null, '0', '10', '2017-02-03 09:03:51', '2018-01-04 16:02:36');
INSERT INTO `api` VALUES ('7', 'MG', null, null, '', '', null, null, null, '4.00', '0', null, '0', '10', '2017-02-03 09:04:51', '2018-01-04 16:02:38');
INSERT INTO `api` VALUES ('8', 'TTG', null, null, '', '', null, null, null, '20.00', '0', null, '0', '10', '2017-02-03 09:05:51', '2018-01-04 16:02:41');
INSERT INTO `api` VALUES ('9', 'IBC', null, null, '', '', null, null, null, '30.00', '0', null, '0', '10', '2017-02-03 09:06:51', '2018-01-04 16:02:42');
INSERT INTO `api` VALUES ('10', 'YC', null, null, '', '', null, null, null, '33.00', '0', null, '0', '10', '2017-02-03 09:07:51', '2018-01-10 13:51:12');
INSERT INTO `api` VALUES ('11', 'CG', null, null, '', '', null, null, null, '39.00', '0', null, '0', '10', '2017-02-03 09:08:51', '2018-01-04 16:02:43');
INSERT INTO `api` VALUES ('12', 'SA', null, null, '', '', null, null, null, '147.00', '0', null, '0', '10', '2017-02-03 09:09:51', '2018-01-04 16:02:44');
INSERT INTO `api` VALUES ('13', 'BG', null, null, '', '', null, null, null, '20.00', '0', null, '0', '10', '2017-02-03 09:10:51', '2017-12-19 16:50:19');
INSERT INTO `api` VALUES ('14', 'DT', null, null, '', '', null, null, null, '40.00', '0', null, '0', '10', '2017-02-03 09:11:51', '2017-10-24 00:37:40');
INSERT INTO `api` VALUES ('2', 'BI', null, 'api.gebbs.net', '{?FjO(Fq', '90eab33c-2216-4280-985d-1f30c4a1da1c', '{?FjO(Fq', 'http://api.yucaiyun.cn', '123456', '0.00', '0', 'v1', '1', '10', '2017-02-03 09:14:51', '2018-01-01 19:35:00');
INSERT INTO `api` VALUES ('21', 'EG', null, null, null, null, null, null, null, '0.00', '0', null, '0', '10', '2017-02-03 09:17:51', '2018-01-03 02:00:03');
INSERT INTO `api` VALUES ('22', 'MW', null, null, '', null, '', '', null, '88.00', '0', '', '0', '10', '2017-10-04 16:00:52', '2017-10-28 13:11:39');
INSERT INTO `api` VALUES ('24', 'SS', null, null, null, null, null, null, null, '15.00', '0', '', '0', '10', '2017-10-04 21:46:35', '2017-12-17 17:50:16');
INSERT INTO `api` VALUES ('25', 'BS', null, null, null, null, null, null, null, '89.00', '0', '', '0', '10', '2017-10-04 21:46:44', '2017-10-25 22:36:42');
INSERT INTO `api` VALUES ('23', 'PNG', null, null, null, null, null, null, null, '119.00', '0', '', '0', '10', '2017-10-04 16:17:28', '2017-10-24 00:37:43');
INSERT INTO `api` VALUES ('26', 'SW', null, null, null, null, null, null, null, '90.00', '0', '', '0', '10', '2017-10-18 23:02:48', '2017-10-24 00:37:45');

-- ----------------------------
-- Table structure for api_activity
-- ----------------------------
DROP TABLE IF EXISTS `api_activity`;
CREATE TABLE `api_activity` (
  `api_id` int(10) unsigned NOT NULL,
  `activity_id` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of api_activity
-- ----------------------------

-- ----------------------------
-- Table structure for bank_cards
-- ----------------------------
DROP TABLE IF EXISTS `bank_cards`;
CREATE TABLE `bank_cards` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `card_no` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '卡号',
  `card_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '卡类型 储蓄卡',
  `bank_id` int(11) NOT NULL COMMENT '银行ID',
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '办卡预留手机号',
  `username` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '持卡人姓名',
  `bank_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '持卡人姓名',
  `on_line` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 上线1下线',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of bank_cards
-- ----------------------------
INSERT INTO `bank_cards` VALUES ('1', '4343242344', '1', '2', '4234324', '324234324324', '32423432', '0', '2018-01-05 00:42:32', '2018-01-05 00:42:40');

-- ----------------------------
-- Table structure for black_list_ip
-- ----------------------------
DROP TABLE IF EXISTS `black_list_ip`;
CREATE TABLE `black_list_ip` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'IP 地址',
  `remark` text COLLATE utf8mb4_unicode_ci COMMENT '备注',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of black_list_ip
-- ----------------------------

-- ----------------------------
-- Table structure for daili_money_log
-- ----------------------------
DROP TABLE IF EXISTS `daili_money_log`;
CREATE TABLE `daili_money_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `yl_money` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '盈利情况',
  `money` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '佣金',
  `remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_month` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of daili_money_log
-- ----------------------------

-- ----------------------------
-- Table structure for dividend
-- ----------------------------
DROP TABLE IF EXISTS `dividend`;
CREATE TABLE `dividend` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `member_id` int(11) NOT NULL COMMENT '用户ID',
  `type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '转换类型 1充值红利2平台红利3返水',
  `describe` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '描述',
  `money` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '金额',
  `before_money` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '发生前的金额',
  `after_money` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '发生后的金额',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态',
  `remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '备注',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of dividend
-- ----------------------------

-- ----------------------------
-- Table structure for drawing
-- ----------------------------
DROP TABLE IF EXISTS `drawing`;
CREATE TABLE `drawing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bill_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '交易流水号',
  `member_id` int(11) NOT NULL COMMENT '用户id',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '充值人名称、昵称',
  `money` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '提款金额',
  `payment_desc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '账户 支付宝账户 微信账户 银行卡号',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1待确认2成功3失败',
  `before_money` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '提款前金额',
  `after_money` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '提款后金额',
  `score` decimal(8,2) NOT NULL DEFAULT '0.00' COMMENT '积分',
  `counter_fee` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '手续费',
  `fail_reason` text COLLATE utf8mb4_unicode_ci COMMENT '失败原因',
  `bank_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '银行名称',
  `bank_card` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '银行卡号',
  `bank_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '开户地址',
  `confirm_at` timestamp NULL DEFAULT NULL COMMENT '确认提款成功时间',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '管理员ID',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of drawing
-- ----------------------------

-- ----------------------------
-- Table structure for feedback
-- ----------------------------
DROP TABLE IF EXISTS `feedback`;
CREATE TABLE `feedback` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '反馈类型',
  `content` text COLLATE utf8mb4_unicode_ci COMMENT '内容',
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '手机',
  `is_read` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0未读1已读',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of feedback
-- ----------------------------

-- ----------------------------
-- Table structure for fs_level
-- ----------------------------
DROP TABLE IF EXISTS `fs_level`;
CREATE TABLE `fs_level` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `game_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '游戏类型',
  `level` tinyint(4) NOT NULL DEFAULT '0' COMMENT '等级',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '等级名称',
  `quota` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '额度',
  `rate` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '返水比例',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of fs_level
-- ----------------------------

-- ----------------------------
-- Table structure for fs_log
-- ----------------------------
DROP TABLE IF EXISTS `fs_log`;
CREATE TABLE `fs_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL COMMENT '用户ID',
  `bet_money` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '下注金额',
  `yx_money` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '有效投注',
  `yingli` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '网站盈利',
  `fs_level` tinyint(4) NOT NULL COMMENT '返水等级',
  `fs_rate` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '返水比率%',
  `fs_money` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '返水金额',
  `fs_order` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '返水批次号',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of fs_log
-- ----------------------------

-- ----------------------------
-- Table structure for game_lists
-- ----------------------------
DROP TABLE IF EXISTS `game_lists`;
CREATE TABLE `game_lists` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `api_id` int(11) NOT NULL COMMENT '平台ID',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '游戏名称',
  `en_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '英文名称',
  `type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '游戏分类',
  `client_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1PC 2手机',
  `game_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '游戏类型',
  `game_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '游戏ID',
  `game_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '游戏名',
  `img_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '手机图片',
  `img_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '手机图片',
  `img_pc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'PC图片',
  `on_line` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0上线1下线',
  `is_hot` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0正常1热门',
  `sort` int(10) unsigned NOT NULL DEFAULT '100' COMMENT '排序',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1229 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of game_lists
-- ----------------------------
INSERT INTO `game_lists` VALUES ('1', '3', '水果拉霸', null, '1', '1', '3', '501', '', '501.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('2', '3', '复古花园', null, '1', '1', '3', '509', '', '509.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('3', '3', '太空漫遊', null, '1', '1', '3', '508', '', '508.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('4', '3', '性感女仆', null, '1', '1', '3', '537', '', '537.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('5', '3', '日本武士', null, '1', '1', '3', '513', '', '513.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('6', '3', '侏罗纪', null, '1', '1', '3', '531', '', '531.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('7', '3', '麻将老虎机', null, '1', '1', '3', '515', '', '515.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('8', '3', '武财神', null, '1', '1', '3', '535', '', '535.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('9', '3', '开心农场', null, '1', '1', '3', '517', '', '517.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('10', '3', '甜一甜屋', null, '1', '1', '3', '512', '', '512.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('11', '3', '关东煮', null, '1', '1', '3', '510', '', '510.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('12', '3', '海底漫遊', null, '1', '1', '3', '519', '', '519.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('13', '3', '西洋棋老虎机', null, '1', '1', '3', '516', '', '516.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('14', '3', '空中战争', null, '1', '1', '3', '526', '', '526.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('15', '3', '牧场咖啡', null, '1', '1', '3', '511', '', '511.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('16', '3', '鬼马小丑', null, '1', '1', '3', '520', '', '520.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('17', '3', '惊吓鬼屋', null, '1', '1', '3', '522', '', '522.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('18', '3', '越野机车', null, '1', '1', '3', '528', '', '528.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('19', '3', '土地神', null, '1', '1', '3', '532', '', '532.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('20', '3', '夏日营地', null, '1', '1', '3', '518', '', '518.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('21', '3', '疯狂马戏团', null, '1', '1', '3', '523', '', '523.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('22', '3', '埃及奥秘', null, '1', '1', '3', '529', '', '529.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('23', '3', '布袋和尚', null, '1', '1', '3', '533', '', '533.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('24', '3', '摇滚狂迷', null, '1', '1', '3', '527', '', '527.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('25', '3', '正财神', null, '1', '1', '3', '534', '', '534.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('26', '3', '幸运8', null, '1', '1', '3', '601', '', '601.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('27', '3', '闪亮女郎', null, '1', '1', '3', '602', '', '602.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('28', '3', '龙珠', null, '1', '1', '3', '600', '', '600.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('29', '3', '海盗王', null, '1', '1', '3', '605', '', '605.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('30', '3', '欢乐时光', null, '1', '1', '3', '530', '', '530.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('31', '3', '海洋剧场', null, '1', '1', '3', '524', '', '524.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('32', '3', '象棋老虎机', null, '1', '1', '3', '514', '', '514.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('33', '3', '偏财神', null, '1', '1', '3', '536', '', '536.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('34', '3', '水上乐园', null, '1', '1', '3', '525', '', '525.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('35', '3', '小熊猫', null, '1', '1', '3', '607', '', '607.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('36', '4', '金钱豹', null, '1', '1', '3', '5707', '', 'Game_5707.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('37', '4', '三国', null, '1', '1', '3', '5106', '', 'Game_5106.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('38', '4', '浓情巧克力', null, '1', '1', '3', '5706', '', 'Game_5706.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('39', '4', '聚宝盆', null, '1', '1', '3', '5705', '', 'Game_5705.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('40', '4', '斗牛', null, '1', '1', '3', '5704', '', 'Game_5704.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('41', '4', '发达啰', null, '1', '1', '3', '5703', '', 'Game_5703.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('42', '4', '大红帽与小野狼', null, '1', '1', '3', '5407', '', 'Game_5407.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('43', '4', '连环夺宝', null, '1', '1', '3', '5901', '', 'Game_5901.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('44', '4', '喜福牛年', null, '1', '1', '3', '5835', '', 'Game_5835.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('45', '4', '斗鸡', null, '1', '1', '3', '5095', '', 'Game_5095.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('46', '4', '百家乐大转轮', null, '1', '1', '3', '5073', '', 'Game_5073.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('47', '4', '钻石水果盘', null, '1', '1', '3', '5043', '', 'Game_5043.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('48', '4', '明星97II', null, '1', '1', '3', '5044', '', 'Game_5044.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('49', '4', '惑星战记', null, '1', '1', '3', '5005', '', 'Game_5005.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('50', '4', 'Staronic', null, '1', '1', '3', '5006', '', 'Game_5006.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('51', '4', '激爆水果盘', null, '1', '1', '3', '5007', '', 'Game_5007.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('52', '4', '足球拉霸', null, '1', '1', '3', '5066', '', 'Game_5004.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('53', '4', '筒子拉霸', null, '1', '1', '3', '5065', '', 'Game_5003.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('54', '4', '扑克拉霸', null, '1', '1', '3', '5064', '', 'Game_5002.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('55', '4', '水果拉霸', null, '1', '1', '3', '5063', '', 'Game_5001.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('56', '4', '疯狂水果盘', null, '1', '1', '3', '5058', '', 'Game_5058.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('57', '4', '明星97', null, '1', '1', '3', '5057', '', 'Game_5057.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('58', '4', '超级7', null, '1', '1', '3', '5061', '', 'Game_5061.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('59', '4', '龙在囧途', null, '1', '1', '3', '5062', '', 'Game_5062.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('60', '4', '动物奇观五', null, '1', '1', '3', '5060', '', 'Game_5060.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('61', '4', '三国', null, '1', '1', '3', '5106', '', 'Game_5106.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('62', '4', '斗鸡', null, '1', '1', '3', '5095', '', 'Game_5095.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('63', '4', '金瓶梅', null, '1', '1', '3', '5093', '', 'Game_5093.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('64', '4', '封神榜', null, '1', '1', '3', '5092', '', 'Game_5092.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('65', '4', '三国拉霸', null, '1', '1', '3', '5091', '', 'Game_5091.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('66', '4', '金瓶梅2', null, '1', '1', '3', '5094', '', 'Game_5094.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('67', '4', '幸运财神', null, '1', '1', '3', '5030', '', 'Game_5030.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('68', '4', '圣诞派对', null, '1', '1', '3', '5029', '', 'Game_5029.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('69', '4', '特务危机', null, '1', '1', '3', '5048', '', 'Game_5048.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('70', '4', '玉蒲团', null, '1', '1', '3', '5049', '', 'Game_5049.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('71', '4', '中秋月光派对', null, '1', '1', '3', '5028', '', 'Game_5028.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('72', '4', '功夫龙', null, '1', '1', '3', '5027', '', 'Game_5027.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('73', '4', '法海斗白蛇', null, '1', '1', '3', '5025', '', 'Game_5025.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('74', '4', '2012 伦敦奥运', null, '1', '1', '3', '5026', '', 'Game_5026.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('75', '4', '2014 FIFA', null, '1', '1', '3', '5204', '', 'Game_5204.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('76', '4', '水果乐园', null, '1', '1', '3', '5019', '', 'Game_5019.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('77', '4', '传统', null, '1', '1', '3', '5013', '', 'Game_5013.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('78', '4', '史前丛林冒险', null, '1', '1', '3', '5016', '', 'Game_5016.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('79', '4', '齐天大圣', null, '1', '1', '3', '5018', '', 'Game_5018.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('80', '4', '爱你一万年', null, '1', '1', '3', '5203', '', 'Game_5203.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('81', '4', '月光宝盒', null, '1', '1', '3', '5202', '', 'Game_5202.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('82', '4', '火焰山', null, '1', '1', '3', '5201', '', 'Game_5201.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('83', '4', '星际大战', null, '1', '1', '3', '5017', '', 'Game_5017.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('84', '4', 'FIFA2010', null, '1', '1', '3', '5015', '', 'Game_5015.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('85', '4', '大红帽与小野狼', null, '1', '1', '3', '5407', '', 'Game_5407.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('86', '4', '沙滩排球', null, '1', '1', '3', '5404', '', 'Game_5404.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('87', '4', '夜市人生', null, '1', '1', '3', '5402', '', 'Game_5402.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('88', '4', '秘境冒险', null, '1', '1', '3', '5601', '', 'Game_5601.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('89', '4', '神舟27', null, '1', '1', '3', '5406', '', 'Game_15027.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('90', '4', '钻石水果盘', null, '1', '1', '3', '5043', '', 'Game_5043.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('91', '4', '明星97II', null, '1', '1', '3', '5044', '', 'Game_5044.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('92', '4', '惑星战记', null, '1', '1', '3', '5005', '', 'Game_5005.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('93', '4', 'Staronic', null, '1', '1', '3', '5006', '', 'Game_5006.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('94', '4', '激爆水果盘', null, '1', '1', '3', '5007', '', 'Game_5007.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('95', '4', '喜福猴年', null, '1', '1', '3', '5837', '', 'Game_5837.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('96', '4', '喜福牛年', null, '1', '1', '3', '5835', '', 'Game_5835.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('97', '4', '发大财', null, '1', '1', '3', '5823', '', 'Game_5823.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('98', '4', '金莲', null, '1', '1', '3', '5825', '', 'Game_5825.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('99', '4', '阿基里斯', null, '1', '1', '3', '5802', '', 'Game_5802.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('100', '4', '阿兹特克宝藏', null, '1', '1', '3', '5803', '', 'Game_5803.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('101', '4', '大明星', null, '1', '1', '3', '5804', '', 'Game_5804.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('102', '4', '凯萨帝国', null, '1', '1', '3', '5805', '', 'Game_5805.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('103', '4', '奇幻花园', null, '1', '1', '3', '5806', '', 'Game_5806.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('104', '4', '浪人武士', null, '1', '1', '3', '5808', '', 'Game_5808.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('105', '4', '空战英豪', null, '1', '1', '3', '5809', '', 'Game_5809.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('106', '4', '航海时代', null, '1', '1', '3', '5810', '', 'Game_5810.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('107', '4', '恶龙传说', null, '1', '1', '3', '5824', '', 'Game_5824.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('108', '4', '金矿工', null, '1', '1', '3', '5826', '', 'Game_5826.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('109', '4', '老船长', null, '1', '1', '3', '5827', '', 'Game_5827.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('110', '4', '霸王龙', null, '1', '1', '3', '5828', '', 'Game_5828.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('111', '4', '龙卷风', null, '1', '1', '3', '5836', '', 'Game_5836.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('112', '4', '海豚世界', null, '1', '1', '3', '5801', '', 'Game_5801.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('113', '4', '狂欢夜', null, '1', '1', '3', '5811', '', 'Game_5811.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('114', '4', '国际足球', null, '1', '1', '3', '5821', '', 'Game_5821.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('115', '4', '高球之旅', null, '1', '1', '3', '5831', '', 'Game_5831.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('116', '4', '高速卡车', null, '1', '1', '3', '5832', '', 'Game_5832.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('117', '4', '沉默武士', null, '1', '1', '3', '5833', '', 'Game_5833.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('118', '4', '钻石列车', null, '1', '1', '3', '5083', '', 'Game_5083.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('119', '4', '圣兽传说', null, '1', '1', '3', '5084', '', 'Game_5084.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('120', '4', '百家乐大转轮', null, '1', '1', '3', '5073', '', 'Game_5073.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('121', '4', '黄金大转轮', null, '1', '1', '3', '8070', '', 'Game_5070.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('122', '4', '乐透转轮', null, '1', '1', '3', '5080', '', 'Game_5080.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('123', '4', '水果大转轮', null, '1', '1', '3', '5077', '', 'Game_5077.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('124', '4', '数字大转轮', null, '1', '1', '3', '5076', '', 'Game_5076.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('125', '4', '3D数字大转轮', null, '1', '1', '3', '5079', '', 'Game_5079.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('126', '4', '百搭二王', null, '1', '1', '3', '5040', '', 'Game_5040.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('127', '4', '加勒比扑克', null, '1', '1', '3', '5035', '', 'Game_5035.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('128', '4', '红狗', null, '1', '1', '3', '5089', '', 'Game_5089.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('129', '4', '斗大', null, '1', '1', '3', '5088', '', 'Game_5088.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('130', '4', '王牌5PK', null, '1', '1', '3', '5034', '', 'Game_5034.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('131', '4', '皇家德州扑克', null, '1', '1', '3', '5131', '', 'Game_5131.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('132', '4', '奖金21点', null, '1', '1', '3', '5118', '', 'Game_5118.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('133', '4', '维加斯21点', null, '1', '1', '3', '5117', '', 'Game_5117.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('134', '4', '西班牙21点', null, '1', '1', '3', '5116', '', 'Game_5116.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('135', '4', '经典21点', null, '1', '1', '3', '5115', '', 'Game_5115.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('136', '4', '彩金轮盘', null, '1', '1', '3', '5108', '', 'Game_5103.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('137', '4', '法式轮盘', null, '1', '1', '3', '5109', '', 'Game_5104.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('138', '4', '美式轮盘', null, '1', '1', '3', '5107', '', 'Game_5102.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('139', '4', '金刚爬楼', null, '1', '1', '3', '5009', '', 'Game_5009.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('140', '4', '连环夺宝', null, '1', '1', '3', '5901', '', 'Game_5901.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('141', '4', '糖果派对', null, '1', '1', '3', '5902', '', 'Game_5902.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('142', '4', '鱼虾蟹', null, '1', '1', '3', '5039', '', 'Game_5039.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('143', '4', '猴子爬树', null, '1', '1', '3', '5008', '', 'Game_5008.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('144', '6', '探索', null, '1', '2', '3', 'rng2', '', 'f9171b615fdb4d57f43e4e966d4b6dab.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('145', '6', '企鹅的假期', null, '1', '2', '3', 'pgv', '', 'd3e023803856678a7da2592601c93fab.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('146', '6', '疯狂水果', null, '1', '2', '3', 'fnfrj', '', 'W88-Slots-Funky-Fruits_1.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('147', '6', '三重猴子', null, '1', '2', '3', 'trpmnk', '', '775e181dad45be8f915d0024988df391.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('148', '6', '高速之王', null, '1', '2', '3', 'hk', '', 'W88-Slots-Highway-Kings_1.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('149', '6', '狂野赌徒', null, '1', '2', '3', 'gtswg', '', '022aeb45b32b7710749b9a86d1360a73_573631f2bd927.jpeg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('150', '6', '疯狂麻将', null, '1', '2', '3', 'fkmj', '', '2d3ebbe40d4e697bd1afdeafa122f3d3.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('151', '6', '百家乐', null, '1', '2', '3', 'ba', '', 'W88-Slots-Baccarat_4.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('152', '6', '海滨嘉年华', null, '1', '2', '3', 'bl', '', 'W88-Slots-Beach-Life_1.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('153', '6', '赌场德州扑克', null, '1', '2', '3', 'cheaa', '', 'W88-Slots-Casino-Hold-\'Em_0.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('154', '6', '夜间外出', null, '1', '2', '3', 'ano', '', 'W88-Slots-A-Night-Out_1.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('155', '6', '巨大累积奖池', null, '1', '2', '3', 'jpgt', '', '192a65e89f0718e510a3f17500195962_570488c733e07.jpeg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('156', '6', '烈焰钻石', null, '1', '2', '3', 'ght_a', '', '78c1c5ab98f211fdb6a149e0b04faa2f.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('157', '6', '招财进宝', null, '1', '2', '3', 'zcjb', '', 'W88-Slots-Zhao-Cai-Jin-Bao_1.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('158', '6', '舞龙累积奖池', null, '1', '2', '3', 'wlgjp', '', 'ebcb9bbad14156c56737ade353a1487b_57709976a6688.jpeg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('159', '6', '魔豆的赏金', null, '1', '2', '3', 'ashbob', '', 'cbf83605ec08fcbc5455ff36a5e89119_5742b84fac88b.jpeg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('160', '6', '巴西桑巴', null, '1', '2', '3', 'gtssmbr', '', 'W88-Slots-Samba-Brazil_1.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('161', '6', '熊之舞', null, '1', '2', '3', 'bob', '', '1031bb7a2fe32866903998bebd8a72e0_5735853610a45.jpeg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('162', '6', '沙漠宝藏II', null, '1', '2', '3', 'dt2', '', 'W88-Slots-Desert-Treasure-II_1.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('163', '6', '美丽佳人', null, '1', '2', '3', 'ashfta', '', '4ad063a9a40409a3f477a0c5af4d65d9_570487a867601.jpeg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('164', '6', '足球狂欢节', null, '1', '2', '3', 'gtsfc', '', 'W88-Slots-Football-Carnival_2.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('165', '6', '黄金游戏', null, '1', '2', '3', 'glg', '', 'W88-Slots-Golden-Games_1.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('166', '6', '冰讯时代', null, '1', '2', '3', 'ir', '', 'e67a0fc551ce1ce895f33e3989d8ce63_57362784230d3.jpeg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('167', '6', '吉祥8', null, '1', '2', '3', 'gtsjxb', '', 'ad2cf68d46541e445ece7e4267c00202_577088dadfafd.jpeg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('168', '6', '约翰韦恩', null, '1', '2', '3', 'gtsjhw', '', 'W88-Slots-John-Wayne_1.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('169', '6', '幸运女郎', null, '1', '2', '3', 'mfrt', '', '6316a2c9a9808d3f30e503e901a93062.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('170', '6', '万圣节财富', null, '1', '2', '3', 'hlf', '', 'W88-Slots-Halloween-Fortune_1.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('171', '6', '万世魔星', null, '1', '2', '3', 'ashlob', '', '16a94e37113efef09551b2c8773b4dd4_5704891d4597b.jpeg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('172', '6', '小猪和狼', null, '1', '2', '3', 'paw', '', 'W88-Slots-Piggies-and-The-Wolf_1.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('173', '6', '粉红豹', null, '1', '2', '3', 'pnp', '', '73e39f46c087a61eb9cc512d3100b4fa_57048982dcf10.jpeg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('174', '6', '奖金美式轮盘', null, '1', '2', '3', 'rodz_g', '', 'W88-Slots-Premium-American-Roulette_0.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('175', '6', '奖金欧式轮盘', null, '1', '2', '3', 'ro_g', '', 'W88-Slots-Premium-European-Roulette_0.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('176', '6', '精彩交易', null, '1', '2', '3', 'ashtmd', '', '3e99ae4536eb9b7634fddc5c75c5bd55.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('177', '6', '疯狂维京人', null, '1', '2', '3', 'gts52', '', 'W88-Slots-Vikingmania_1.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('178', '6', '狂热水果', null, '1', '2', '3', 'fmn', '', 'W88-Slots-Fruit-Mania_1.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('179', '6', '五路财神', null, '1', '2', '3', 'wlcsh', '', '11496380d4fb4deb3c883a46e375a2f9_57bbe8fb77701.jpeg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('180', '6', '圣诞老人的惊喜', null, '1', '2', '3', 'ssp', '', 'W88-Slots-Santa-Surprise_1.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('181', '6', '埃斯梅拉达', null, '1', '2', '3', 'esm', '', 'W88-Slots-Esmeralda_2.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('182', '6', '堂吉诃德的财富', null, '1', '2', '3', 'mfrt', '', '8e16a584a977f861dd958d3b5da5f1c7_573630d17d90e.jpeg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('183', '6', '复活节大惊喜', null, '1', '2', '3', 'eas', '', 'W88-Slots-Easter-Surprise_1.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('184', '6', '好运来', null, '1', '2', '3', 'sol', '', 'W88-Slots-Streak-of-Luck_1.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('185', '6', '年年有余', null, '1', '2', '3', 'nian', '', 'W88-Slots-Nian-Nian-You-Yu_1.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('186', '6', '幸运月', null, '1', '2', '3', 'ashfmf', '', 'W88-Slots-FullMoon-Fortunes_1.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('187', '6', '弗兰克•戴图理:神奇7', null, '1', '2', '3', 'fdt', '', 'W88-Slots-Frankie-Detorri\'s-Magic-Seven_1.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('188', '6', '怀特王', null, '1', '2', '3', 'whk', '', 'W88-Slots-White-King_0.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('189', '6', '沉默的武士', null, '1', '2', '3', 'sis', '', '8ce2f4a61507e74ae51c1e0da0521bff_57362e4ce5b1e.jpeg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('190', '6', '沙漠宝藏', null, '1', '2', '3', 'mobdt', '', 'W88-Slots-Desert-Treasure_1.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('191', '6', '泰国天堂', null, '1', '2', '3', 'tpd2', '', '8c5fda46d95eeb2195e3419ea689ba30_57362fc62e9ff.jpeg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('192', '6', '玛丽莲梦露', null, '1', '2', '3', 'gtsmrln', '', 'W88-Slots-Marilyn-Monroe_1.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('193', '6', '甜蜜派对', null, '1', '2', '3', 'cnpr', '', 'W88-Slots-Sweet-Party_1.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('194', '6', '疯狂7', null, '1', '2', '3', 'c7', '', 'W88-Slots-Crazy-7_1.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('195', '6', '疯狂乐透', null, '1', '2', '3', 'lm', '', 'W88-Slots-Lotto-Madness_1.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('196', '6', '神秘夏洛克', null, '1', '2', '3', 'shmst', '', 'W88-Slots-Sherlock-Mystery_1.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('197', '6', '舞龙', null, '1', '2', '3', 'wlg', '', 'W88-Slots-Wu-Long_0.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('198', '6', '船长的宝藏', null, '1', '2', '3', 'ct', '', 'W88-Slots-Captain\'s-Treasure_1.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('199', '6', '艺伎故事', null, '1', '2', '3', 'ges', '', '10d98569598be5e2ae30223875e9eee5_57358d9a2b3a2.jpeg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('200', '6', '财富宝箱', null, '1', '2', '3', 'ashcpl', '', '50c7cae995562dfc67c7959a8f726299_5704870459119.jpeg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('201', '6', '辛巴达黄金航程', null, '1', '2', '3', 'ashsbd', '', 'ce1e33106839ff71a5a815a2250d138e_57362ef3652a2.jpeg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('202', '6', '返利先生', null, '1', '2', '3', 'mrcb', '', 'W88-Slots-Mr.-Cash-Back_1.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('203', '6', '银子弹', null, '1', '2', '3', 'sib', '', 'W88-Slots-Silver-Bullet_1.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('204', '6', '马博士', null, '1', '2', '3', 'dlm', '', 'W88-Slots-Dr-Lovemore_1.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('205', '6', '黑豹', null, '1', '2', '3', 'pmn', '', '2ec0af56643c7975f63cabfa0cff1c53_57362c9402d93.jpeg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('206', '6', '海豚礁', null, '1', '2', '3', 'dnr', '', 'ae3a1838cf6647c561aab54f6585fb11_5742d1a7aec6d.jpeg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('207', '6', '幸运万圣节2', null, '1', '2', '3', 'hlf2', '', '801691b8ded9850f0d5352c990918046.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('208', '6', '弓兵', null, '1', '1', '3', 'arc', '', 'arc.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('209', '6', '亚特兰蒂斯女王', null, '1', '1', '3', 'gtsatq', '', 'gtsatq.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('210', '6', '熊之舞', null, '1', '1', '3', 'bob', '', 'bob.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('211', '6', '猫女王', null, '1', '1', '3', 'catqk', '', 'catqk.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('212', '6', '樱桃之恋', null, '1', '1', '3', 'chl', '', 'chl.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('213', '6', '牛仔和外星人', null, '1', '1', '3', 'gtscbl', '', 'gtscbl.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('214', '6', '海豚礁', null, '1', '1', '3', 'dnr', '', 'dnr.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('215', '6', '足球狂欢节', null, '1', '1', '3', 'gtsfc', '', 'gtsfc.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('216', '6', '戴图理的神奇七', null, '1', '1', '3', 'fdt', '', 'fdt.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('217', '6', '生命之神', null, '1', '1', '3', 'athn', '', 'gts46.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('218', '6', '湛蓝深海', null, '1', '1', '3', 'bib', '', 'bib.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('219', '6', '丛林心脏', null, '1', '1', '3', 'ashhotj', '', 'ashhotj.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('220', '6', '超级高速公路之王', null, '1', '1', '3', 'gtshwkp', '', 'gtshwkp.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('221', '6', '激情桑巴', null, '1', '1', '3', 'gtssmbr', '', 'gtssmbr.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('222', '6', '亚马逊的秘密', null, '1', '1', '3', 'samz', '', 'samz.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('223', '6', '夏洛克的秘密', null, '1', '1', '3', 'shmst', '', 'shmst.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('224', '6', '沉默的武士', null, '1', '1', '3', 'sis', '', 'sis.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('225', '6', '泰国天堂', null, '1', '1', '3', 'tpd2', '', 'tpd2.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('226', '6', '爵士俱乐部', null, '1', '1', '3', 'gtsjzc', '', 'gtsjzc.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('227', '6', '真爱', null, '1', '1', '3', 'trl', '', 'trl.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('228', '6', '疯狂维京海盗', null, '1', '1', '3', 'gts52', '', 'gts52.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('229', '6', '白狮王', null, '1', '1', '3', 'whk', '', 'whk.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('230', '6', '赌徒', null, '1', '1', '3', 'gtswg', '', 'gtswg.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('231', '6', '豪华的开心假期', null, '1', '1', '3', 'vcstd', '', 'vcstd.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('232', '6', '浮冰流', null, '1', '1', '3', 'gtsir', '', 'gtsir.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('233', '6', '堂吉诃德的财富', null, '1', '1', '3', 'donq', '', 'donq.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('234', '6', '飞龙在天', null, '1', '1', '3', 'gtsflzt', '', 'gtsflzt.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('235', '6', '原始亚马逊', null, '1', '1', '3', 'ashamw', '', 'ashamw.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('236', '6', '招财童子', null, '1', '1', '3', 'zctz', '', 'zctz.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('237', '6', '孙悟空', null, '1', '1', '3', 'gtsswk', '', 'gtsswk.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('238', '6', '白狮', null, '1', '1', '3', 'bs', '', 'bs.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('239', '6', '神奇堆栈', null, '1', '1', '3', 'mgstk', '', 'mgstk.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('240', '6', '四象', null, '1', '1', '3', 'sx', '', 'sx.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('241', '6', '捍卫战士', null, '1', '1', '3', 'topg', '', 'topg.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('242', '6', '金钱蛙', null, '1', '1', '3', 'jqw', '', 'jqw.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('243', '6', '权杖女王', null, '1', '1', '3', 'qnw', '', 'qnw.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('244', '6', '欧莱里之黄金大田', null, '1', '1', '3', 'spud', '', 'spud.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('245', '6', '超级888', null, '1', '1', '3', 'chao', '', 'chao.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('246', '6', '宝石女王', null, '1', '1', '3', 'gemq', '', 'gemq.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('247', '6', '龙龙龙', null, '1', '1', '3', 'longlong', '', 'longlong.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('248', '6', '布法罗闪电战', null, '1', '1', '3', 'bfb', '', 'bfb.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('249', '6', '舞龙', null, '1', '1', '3', 'wlg', '', 'wlg.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('250', '6', '招财进宝', null, '1', '1', '3', 'zcjb', '', 'zcjb.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('251', '6', '开心假期', null, '1', '1', '3', 'er', '', 'er.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('252', '6', '万圣节财富', null, '1', '1', '3', 'hlf', '', 'hlf.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('253', '6', '百幕大三角', null, '1', '1', '3', 'bt', '', 'bt.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('254', '6', '中国厨房', null, '1', '1', '3', 'cm', '', 'cm.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('255', '6', '疯狂之七', null, '1', '1', '3', 'c7', '', 'c7.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('256', '6', '龙族', null, '1', '1', '3', 'gtsdgk', '', 'gtsdgk.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('257', '6', '惊喜复活节', null, '1', '1', '3', 'eas', '', 'eas.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('258', '6', '青春之泉', null, '1', '1', '3', 'foy', '', 'foy.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('259', '6', '酷炫水果农场', null, '1', '1', '3', 'fff', '', 'fff.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('260', '6', '古怪猴子', null, '1', '1', '3', 'fm', '', 'fm.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('261', '6', '黄金游戏', null, '1', '1', '3', 'glg', '', 'glg.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('262', '6', '鬼屋', null, '1', '1', '3', 'hh', '', 'hh.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('263', '6', '热力宝石', null, '1', '1', '3', 'gts50', '', 'gts50.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('264', '6', '无敌金刚', null, '1', '1', '3', 'kkg', '', 'kkg.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('265', '6', 'Cash back先生', null, '1', '1', '3', 'mcb', '', 'mcb.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('266', '6', '海王星王国', null, '1', '1', '3', 'nk', '', 'nk.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('267', '6', '舞线', null, '1', '1', '3', 'pl', '', 'pl.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('268', '6', '洛基传奇', null, '1', '1', '3', 'rky', '', 'rky.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('269', '6', '圣诞奇迹', null, '1', '1', '3', 'ssp', '', 'ssp.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('270', '6', '木乃伊迷城', null, '1', '1', '3', 'mmy', '', 'mmy.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('271', '6', '顶级王牌-明星', null, '1', '1', '3', 'ttc', '', 'ttc.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('272', '6', '三个朋友', null, '1', '1', '3', 'ta', '', 'ta.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('273', '6', '丛林巫师', null, '1', '1', '3', 'ub', '', 'ub.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('274', '6', '我心狂野', null, '1', '1', '3', 'wis', '', 'wis.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('275', '6', '黄金翅膀', null, '1', '1', '3', 'wis', '', 'wis.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('276', '6', '沙漠宝藏2', null, '1', '1', '3', 'dt2', '', 'dt2.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('277', '6', '满月财富', null, '1', '1', '3', 'ashfmf', '', 'ashfmf.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('278', '6', '角斗士', null, '1', '1', '3', 'glr', '', 'glr.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('279', '6', '约翰韦恩', null, '1', '1', '3', 'gtsjhw', '', 'gtsjhw.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('280', '6', '幸运熊猫', null, '1', '1', '3', 'gts51', '', 'gts51.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('281', '6', '玛丽莲·梦露', null, '1', '1', '3', 'gtsmrln', '', 'gtsmrln.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('282', '6', '现金魔块', null, '1', '1', '3', 'gtscb', '', 'gtscb.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('283', '6', '三个小丑刮刮乐', null, '1', '1', '3', 'tclsc', '', 'tclsc.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('284', '6', '经典老虎机刮刮乐', null, '1', '1', '3', 'scs', '', 'scs.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('285', '6', '3卡吹噓', null, '1', '1', '3', 'ash3brg', '', 'ash3brg.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('286', '6', '美式轮盘', null, '1', '1', '3', 'rodz', '', 'rodz.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('287', '6', '百家乐', null, '1', '1', '3', 'ba', '', 'ba.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('288', '6', '赌场HOLD\'EM游戏', null, '1', '1', '3', 'cheaa', '', 'cheaa.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('289', '6', '欧洲轮盘', null, '1', '1', '3', 'ro', '', 'ro.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('290', '6', '美式奖金轮盘赌', null, '1', '1', '3', 'rodz_g', '', 'rodz_g.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('291', '6', '欧式奖金轮盘赌', null, '1', '1', '3', 'ro_g', '', 'ro_g.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('292', '6', '五虎将', null, '1', '1', '3', 'ftg', '', 'ftg.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('293', '6', '诸神时代', null, '1', '1', '3', 'aogs', '', 'aogs.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('294', '6', '印加帝国头奖', null, '1', '1', '3', 'aztec', '', 'aztec.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('295', '6', '美国21点', null, '1', '1', '3', 'bja', '', 'bja.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('296', '6', '海滩生活', null, '1', '1', '3', 'bl', '', 'bl.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('297', '6', '甜蜜派对', null, '1', '1', '3', 'cnpr', '', 'cnpr.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('298', '6', '船长的宝藏', null, '1', '1', '3', 'ct', '', 'ct.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('299', '6', '情圣博士', null, '1', '1', '3', 'dlm', '', 'dlm.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('300', '6', '沙漠宝藏', null, '1', '1', '3', 'dt', '', 'dt.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('301', '6', '所有人的大奖', null, '1', '1', '3', 'evj', '', 'evj.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('302', '6', '终极足球', null, '1', '1', '3', 'fbr', '', 'fbr.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('303', '6', '完美二十一点', null, '1', '1', '3', 'pfbj_mh5', '', 'pfbj_mh5.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('304', '6', '水果狂', null, '1', '1', '3', 'fmn', '', 'fmn.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('305', '6', '趣味水果', null, '1', '1', '3', 'fnfrj', '', 'fnfrj.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('306', '6', '惊异之林', null, '1', '1', '3', 'fow', '', 'fow.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('307', '6', '艺伎故事', null, '1', '1', '3', 'ges', '', 'ges.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('308', '6', '角斗士彩池游戏', null, '1', '1', '3', 'glrj', '', 'glrj.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('309', '6', '金色召集', null, '1', '1', '3', 'grel', '', 'grel.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('310', '6', '一夜奇遇', null, '1', '1', '3', 'hb', '', 'hb.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('311', '6', '高速公路之王', null, '1', '1', '3', 'hk', '', 'hk.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('312', '6', '六福兽', null, '1', '1', '3', 'kfp', '', 'kfp.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('313', '6', '爱之船', null, '1', '1', '3', 'lvb', '', 'lvb.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('314', '6', '魔幻吃角子老虎', null, '1', '1', '3', 'ms', '', 'ms.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('315', '6', '粉红豹', null, '1', '1', '3', 'pnp', '', 'pnp.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('316', '6', '金字塔女王', null, '1', '1', '3', 'qop', '', 'qop.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('317', '6', '银弹', null, '1', '1', '3', 'sib', '', 'sib.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('318', '6', '五路财神', null, '1', '1', '3', 'wlcsh', '', 'wlcsh.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('319', '14', '英雄荣耀', null, '1', '1', '3', 'crystal', '', 'crystal.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('320', '14', '福禄寿', null, '1', '1', '3', 'fls', '', 'fls.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('321', '14', '四圣兽', null, '1', '1', '3', 'fourss', '', 'fourss.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('322', '14', '劲爆篮球', null, '1', '1', '3', 'btball5x20', '', 'btball5x20.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('323', '14', '白蛇传', null, '1', '1', '3', 'whitesnake', '', 'whitesnake.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('324', '14', '龙珠', null, '1', '1', '3', 'dragonball', '', 'dragonball.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('325', '14', '封神榜', null, '1', '1', '3', 'tlod', '', 'tlod.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('326', '14', '西游降妖', null, '1', '1', '3', 'jtw', '', 'jtw.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('327', '14', '新年到', null, '1', '1', '3', 'newyear', '', 'newyear.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('328', '14', '财神到', null, '1', '1', '3', 'tgow', '', 'tgow.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('329', '14', '三国-赤壁之战', null, '1', '1', '3', 'san', '', 'san.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('330', '14', '海贼王', null, '1', '1', '3', 'onepiece3x1', '', 'onepiece3x1.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('331', '14', '武松传', null, '1', '1', '3', 'watermargin', '', 'watermargin.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('332', '14', '拳皇', null, '1', '1', '3', 'kof', '', 'kof.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('333', '14', '灌篮高手', null, '1', '1', '3', 'sd', '', 'sd.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('334', '14', '灌篮高手pro', null, '1', '1', '3', 'sd5', '', 'sd5.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('335', '14', '火影忍者', null, '1', '1', '3', 'naruto', '', 'naruto.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('336', '14', '圣斗士星矢', null, '1', '1', '3', 'seiya', '', 'seiya.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('337', '14', '梦幻森林', null, '1', '1', '3', 'fantasyforest3x1', '', 'fantasyforest3x1.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('338', '14', '水浒传', null, '1', '1', '3', 'watermargin5x25', '', 'watermargin5x25.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('339', '14', '西游记', null, '1', '1', '3', 'xiyouji5x25', '', 'xiyouji5x25.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('340', '14', '阿拉丁神灯', null, '1', '1', '3', 'aladdin5x243', '', 'aladdin5x243.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('341', '14', '降妖传奇', null, '1', '1', '3', 'xiyouji5x9', '', 'xiyouji5x9.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('342', '14', '哆啦A梦', null, '1', '1', '3', 'doraemon3x5', '', 'doraemon3x5.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('343', '14', '高达', null, '1', '1', '3', 'gundam3x5', '', 'gundam3x5.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('344', '14', '海盗无双', null, '1', '1', '3', 'onepiece', '', 'onepiece.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('345', '14', '街霸', null, '1', '1', '3', 'streetfighter3x1', '', 'streetfighter3x1.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('346', '14', '3D老虎机', null, '1', '1', '3', 'casino', '', 'casino.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('347', '14', '拳皇98', null, '1', '1', '3', 'kof5x9', '', 'kof5x9.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('348', '14', '五行世界', null, '1', '1', '3', 'fiveelements5x9', '', 'fiveelements5x9.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('349', '14', '仙剑奇缘', null, '1', '1', '3', 'xjqy5x9', '', 'xjqy5x9.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('350', '14', '龙凤呈祥', null, '1', '1', '3', 'dnp', '', 'dnp.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('351', '14', '阿拉丁神灯', null, '1', '2', '3', 'aladdin5x243', '', '阿拉丁神灯.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('352', '14', '劲爆篮球', null, '1', '2', '3', 'btball5x20', '', '劲爆篮球.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('353', '14', '英雄荣耀', null, '1', '2', '3', 'crystal', '', '英雄荣耀.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('354', '14', '龙凤呈祥', null, '1', '2', '3', 'dnp', '', '龙凤呈祥.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('355', '14', '哆啦A梦', null, '1', '2', '3', 'doraemon3x5', '', '哆啦A梦.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('356', '14', '梦幻森林', null, '1', '2', '3', 'fantasyforest3x1', '', '梦幻森林.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('357', '14', '五行世界', null, '1', '2', '3', 'fiveelements5x9', '', '五行世界.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('358', '14', '赤壁之战', null, '1', '2', '3', 'san', '', '赤壁之战.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('359', '14', '高达', null, '1', '2', '3', 'gundam3x5', '', '高达.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('360', '14', '西游降妖', null, '1', '2', '3', 'jtw', '', '西游降妖.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('361', '14', '拳皇', null, '1', '2', '3', 'kof', '', '拳皇.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('362', '14', '拳皇98', null, '1', '2', '3', 'kof5x9', '', '拳皇98.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('363', '14', '火影忍者', null, '1', '2', '3', 'naruto', '', '火影忍者.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('364', '14', '新年到', null, '1', '2', '3', 'newyear', '', '新年到.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('365', '14', '海贼王', null, '1', '2', '3', 'onepiece', '', '海贼王.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('366', '14', '海贼王2', null, '1', '2', '3', 'onepiece3x1', '', '海贼王2.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('367', '14', '灌篮高手', null, '1', '2', '3', 'sd', '', '灌篮高手.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('368', '14', '灌篮高手pro', null, '1', '2', '3', 'sd5', '', '灌篮高手pro.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('369', '14', '圣斗士', null, '1', '2', '3', 'seiya', '', '圣斗士.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('370', '14', '街霸', null, '1', '2', '3', 'streetfighter3x1', '', '街霸.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('371', '14', '财神到', null, '1', '2', '3', 'tgow', '', '财神到.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('372', '14', '封神榜', null, '1', '2', '3', 'tlod', '', '封神榜.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('373', '14', '武松传', null, '1', '2', '3', 'watermargin', '', '武松传.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('374', '14', '水浒传', null, '1', '2', '3', 'watermargin5x25', '', '水浒传.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('375', '14', '白蛇传', null, '1', '2', '3', 'whitesnake', '', '白蛇传.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('376', '14', '西游记', null, '1', '2', '3', 'xiyouji5x25', '', '西游记.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('377', '14', '降妖传奇', null, '1', '2', '3', 'xiyouji5x9', '', '降妖传奇.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('378', '14', '仙剑奇缘', null, '1', '2', '3', 'xjqy5x9', '', '仙剑奇缘.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('379', '14', '3D老虎机', null, '1', '2', '3', 'casino', '', '3D老虎机.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('380', '14', '四圣兽', null, '1', '2', '3', 'fourss', '', '四圣兽.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('381', '14', '福禄寿', null, '1', '2', '3', 'fls', '', '福禄寿.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('382', '14', '龙珠Z', null, '1', '2', '3', 'dragonball', '', '龙珠Z.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('383', '12', '丧尸猎人', null, '1', '1', '3', 'EG-SLOT-S007', '', 'zombieHunter.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('384', '12', '美女沙排', null, '1', '1', '3', 'EG-SLOT-S006', '', 'volleybeauties.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('385', '12', '魔鬼天使', null, '1', '1', '3', 'EG-SLOT-S005', '', 'AngelsDemons.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('386', '12', '幸运喵星人', null, '1', '1', '3', 'EG-SLOT-S004', '', 'BeckoningGirls.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('387', '12', '红楼春梦', null, '1', '1', '3', 'EG-SLOT-A008', '', 'redChamber.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('388', '12', '三星报喜', null, '1', '1', '3', 'EG-SLOT-A002', '', 'ThreeStarGod.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('389', '12', '龙虎', null, '1', '1', '3', 'EG-SLOT-A004', '', 'DragonTiger.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('390', '12', '张保仔', null, '1', '1', '3', 'EG-SLOT-A018', '', 'CheungPoTsai.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('391', '12', '梦幻女神', null, '1', '1', '3', 'EG-SLOT-A005', '', 'FantasyGoddess.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('392', '12', '济公', null, '1', '1', '3', 'EG-SLOT-A006', '', 'JiGong.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('393', '12', '过大年', null, '1', '1', '3', 'EG-SLOT-A001', '', 'NewYearRich.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('394', '12', '运财金鸡', null, '1', '1', '3', 'EG-SLOT-A020', '', 'goldenchicken.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('395', '12', '脆爆水果', null, '1', '1', '3', 'EG-SLOT-A015', '', 'FruitPoppers.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('396', '12', '南北狮王', null, '1', '1', '3', 'EG-SLOT-A017', '', 'LionDance.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('397', '12', '锦衣卫', null, '1', '1', '3', 'EG-SLOT-A003', '', 'guard.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('398', '12', '比基尼狂热', null, '1', '1', '3', 'EG-SLOT-A013', '', 'Bikini.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('399', '12', '趣怪丧尸', null, '1', '1', '3', 'EG-SLOT-A012', '', 'CreepyCuddlers.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('400', '12', '同校生', null, '1', '1', '3', 'EG-SLOT-A009', '', 'Classmate.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('401', '12', '黄飞鸿', null, '1', '1', '3', 'EG-SLOT-S003', '', 'WongFaiHung.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('402', '12', '热带宝藏', null, '1', '1', '3', 'EG-SLOT-A016', '', 'Fish.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('403', '12', '欢乐农场', null, '1', '1', '3', 'EG-SLOT-A010', '', 'FunnyFarm.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('404', '8', '猴年大吉', null, '1', '1', '3', '1035', 'YearOfTheMonkey', '45ec50a0287ca9a73991bc41495f1bc4_57f4703cb9589.jpeg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-10-22 21:34:49');
INSERT INTO `game_lists` VALUES ('405', '8', '疯狂的猴子', null, '1', '1', '3', '1016', 'MadMonkey', 'MadMonkey.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-10-23 04:35:52');
INSERT INTO `game_lists` VALUES ('406', '8', '幸运生肖', null, '1', '1', '3', '14229', 'BG_Zoodiac', 'ZodiacWild.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('407', '8', '21点幸运7', null, '1', '1', '3', '25', 'Lucky7Blackjack', 'Lucky7Blackjack.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('408', '8', '一杆进洞', null, '1', '1', '3', '18', 'HoleInOne', 'HoleInOne.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('409', '8', '三张牌扑克', null, '1', '1', '3', '32', 'ThreeCardPoker', 'ThreeCardPoker.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('410', '8', '丝绸之路', null, '1', '1', '3', '1024', 'TheSilkRoad', '54e1eb317ef57803bc3a62a3f55ad932_57f7614093d42.jpeg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('411', '8', '中子星', null, '1', '1', '3', '1031', 'NeutronStar', 'Neutron-Star.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('412', '8', '二十一点', null, '1', '1', '3', '5', 'Blackjack', 'Blackjack_0.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('413', '8', '五个海盗', null, '1', '1', '3', '1012', 'FivePirates', 'Five-Pirates.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('414', '8', '亚瑟的探索', null, '1', '1', '3', '63', 'ArthursQuest', 'ArthursQuest.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('415', '8', '亚瑟的探索 II', null, '1', '1', '3', '462', 'ArthursQuestIISlots', 'ArthursQuestII_0.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('416', '8', '亚马逊河大冒险', null, '1', '1', '3', '414', 'AmazonAdventureSlots', 'AmazonAdventure.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('417', '8', '企鹅冲浪', null, '1', '1', '3', '449', 'SurfsUpSlots', 'SurfsUp.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('418', '8', '伟大的卡西尼', null, '1', '1', '3', '453', 'TheGreatCasiniSlots', 'TheGreatCasini.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('419', '8', '头彩假日', null, '1', '1', '3', '413', 'JackpotHolidaySlots', 'JackpotHoliday.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('420', '8', '决战将军', null, '1', '1', '3', '14420', 'PP_ReligionofChampions', 'ShogunShowdown.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('421', '8', '出租车', null, '1', '1', '3', '516', 'Taxi', 'Taxi.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('422', '8', '加勒比海宝藏', null, '1', '1', '3', '6', 'CasinoStudPoker', 'Caribbean-Stud-Poker.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('423', '8', '吉李II:赏金猎人', null, '1', '1', '3', '1009', 'KatLeeII', 'Kat-Lee-Bounty-Hunter-2.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('424', '8', '龙王', null, '1', '1', '3', '14442', 'PP_LuckyDragons', '3f55499c3edfe2aab0989c021844081e_56653d8fbceb8.jpeg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('425', '8', '吸血鬼和狼人', null, '1', '1', '3', '480', 'VampiresVsWerewolves', 'VampiresvsWerewolves.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('426', '8', '哇哦', null, '1', '1', '3', '14235', 'BG_KawaiiDragons', 'Goooal.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('427', '8', '啤酒节', null, '1', '1', '3', '65', 'Oktoberfest', 'Oktoberfest.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('428', '8', '国际赛车', null, '1', '1', '3', '1028', 'GrandPrix', 'Grand-Prix.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('429', '8', '地狱的钞票', null, '1', '1', '3', '40', 'FiveReelSlots', 'CashInferno.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('430', '8', '埃及艳后', null, '1', '1', '3', '1034', 'Cleopatra', 'Cleopatra.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('431', '8', '塞伦盖提的钻石', null, '1', '1', '3', '14425', 'PP_BlackDiamond', 'SerengetiDiamonds.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('432', '8', '天使的触摸', null, '1', '1', '3', '477', 'AngelsTouch', 'AngelsTouch_0.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('433', '8', '天龙8s', null, '1', '1', '3', '475', 'DracosFire', 'Dragon8s.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('434', '8', '夺钞票', null, '1', '1', '3', '64', 'BullsEyeBucks', 'CashGrab.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('435', '8', '夺钞票2', null, '1', '1', '3', '64', 'BullsEyeBucks', 'CashGrabII.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('436', '8', '好莱坞卷轴', null, '1', '1', '3', '15', 'HollywoodReels', 'HollywoodReels.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('437', '8', '威尼斯万岁', null, '1', '1', '3', '438', 'VivaVeneziaSlots', 'VivaVenezia.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('438', '8', '威尼斯野玫瑰', null, '1', '1', '3', '1039', 'RoseOfVenice', 'Rose-of-Venice.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('439', '8', '完美对子21点', null, '1', '1', '3', '455', 'PerfectPairsBJSidebet', 'PerfectPairsBlackjack.jpg', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('440', '7', '冰上曲棍球', null, '1', '1', '3', 'Breakaway', '', 'BTN_BreakAway.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('441', '7', '幸运的锦鲤', null, '1', '1', '3', 'LuckyKoi', '', 'BTN_LuckyKoi.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('442', '7', '欧式黄金轮盘', null, '1', '1', '3', 'EuroRouletteGold', '', 'BTN_EuropeanRouletteGold.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('443', '7', '亚洲风情', null, '1', '1', '3', 'AsianBeauty', '', 'BTN_AsianBeauty.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('444', '7', '纯铂金', null, '1', '1', '3', 'pureplatinum', '', 'BTN_pureplatinum.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('445', '7', '复古旋转', null, '1', '1', '3', 'RetroReels', '', 'BTN_RetroReels.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('446', '7', '雷神', null, '1', '1', '3', 'Thunderstruck', '', 'BTN_Thunderstruck.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('447', '7', '足球明星', null, '1', '1', '3', 'FootballStar', '', 'BTN_FootballStar.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('448', '7', '花花公子', null, '1', '1', '3', 'Playboy', '', 'BTN_Playboy.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('449', '7', '好日子', null, '1', '1', '3', 'TheFinerReelsofLife', '', 'BTN_TheFinerReelsofLife.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('450', '7', '功夫小胖猪', null, '1', '1', '3', 'KaratePig', '', 'BTN_KaratePig.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('451', '7', '美女密探', null, '1', '1', '3', 'RubyAgentJaneBlonde', '', 'BTN_AgentJaneBlonde.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('452', '7', '终结者2', null, '1', '1', '3', 'Terminator2', '', 'BTN_Terminator2.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('453', '7', '钻石浮华', null, '1', '1', '3', 'RetroReelsDiamondGlitz', '', 'BTN_RetroReelsDiamondGlitz.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('454', '7', '阿瓦隆2', null, '1', '1', '3', 'Avalon2', '', 'BTN_AvalonII-L-QuestForTheGrail.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('455', '7', '财富联盟', null, '1', '1', '3', 'LeaguesOfFortune', '', 'BTN_LeaguesOfFortune.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('456', '7', '人人有奖', null, '1', '1', '3', 'InItToWinIt', '', 'BTN_InItToWinIt.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('457', '7', '混沌加农炮', null, '1', '1', '3', 'LooseCannon', '', 'BTN_LooseCannon.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('458', '7', '恐怖实验室', null, '1', '1', '3', 'DrWattsUp', '', 'BTN_DrWattsUp.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('459', '7', '狂野之鹰', null, '1', '1', '3', 'UntamedCrownedEagle', '', 'BTN_UntamedCrownedEagle.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('460', '7', '女孩与枪', null, '1', '1', '3', 'GirlsWithGuns', '', 'BTN_GirlsWithGuns-L-JungleHeat.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('461', '7', '女孩与枪II', null, '1', '1', '3', 'GirlsWithGunsFrozenDawn', '', 'BTN_GirlswithGuns-L-FrozenDawn.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('462', '7', '抢劫银行', null, '1', '1', '3', 'BustTheBank', '', 'BTN_BustTheBank.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('463', '7', '上流社会', null, '1', '1', '3', 'HighSociety', '', 'BTN_HighSociety.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('464', '7', '神秘梦境', null, '1', '1', '3', 'MysticDreams', '', 'BTN_MysticDreams.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('465', '7', '圣诞老人的秘密', null, '1', '1', '3', 'SecretSanta', '', 'BTN_SecretSanta.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('466', '7', '为粉红而战', null, '1', '1', '3', 'RacingForPinks', '', 'BTN_RacingForPinks.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('467', '7', '星梦之吻', null, '1', '1', '3', 'StarlightKiss', '', 'BTN_StarlightKiss.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('468', '7', '寻访海豚', null, '1', '1', '3', 'DolphinQuest', '', 'BTN_DolphinQuest.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('469', '7', '藏宝时间', null, '1', '1', '3', 'BootyTime', '', 'BTN_BootyTime.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('470', '7', '新娘吉拉', null, '1', '1', '3', 'Bridezilla', '', 'BTN_Bridezilla.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('471', '7', '小猪财富', null, '1', '1', '3', 'PiggyFortunes', '', 'BTN_PiggyFortunes.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('472', '7', '财富转轮特别版', null, '1', '1', '3', 'WheelOfWealthSE', '', 'BTN_WheelOfWealthSpecialEdition.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('473', '7', '丛林摇摆', null, '1', '1', '3', 'BushTelegraph', '', 'BTN_BushTelegraph.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('474', '7', '地球生物', null, '1', '1', '3', 'WhatonEarth', '', 'BTN_WhatonEarth.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('475', '7', '反转马戏团', null, '1', '1', '3', 'TheTwistedCircus', '', 'BTN_TheTwistedCircus.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('476', '7', '疯狂的帽子', null, '1', '1', '3', 'MadHatters', '', 'BTN_MadHatters.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('477', '7', '古墓丽影', null, '1', '1', '3', 'TombRaider', '', 'BTN_TombRaider.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('478', '7', '古墓丽影2', null, '1', '1', '3', 'RubyTombRaiderII', '', 'BTN_TombRaider2.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('479', '7', '海底世界', null, '1', '1', '3', 'MermaidsMillions', '', 'BTN_MermaidsMillions.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('480', '7', '挥金如土', null, '1', '1', '3', 'Cashville', '', 'BTN_Cashville.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('481', '7', '芥末寿司', null, '1', '1', '3', 'RubyWasabiSan', '', 'BTN_WasabiSan.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('482', '7', '卡萨努瓦', null, '1', '1', '3', 'Cashanova', '', 'BTN_Cashanova.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('483', '7', '恐龙迪诺', null, '1', '1', '3', 'DinoMight', '', 'BTN_DinoMight.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('484', '7', '骷髅陷阱', null, '1', '1', '3', 'SkullDuggery', '', 'BTN_SkullDuggery.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('485', '7', '雷神2', null, '1', '1', '3', 'Thunderstruck2', '', 'BTN_Thunderstruck2.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('486', '7', '洛伯杰克', null, '1', '1', '3', 'RoboJack', '', 'BTN_RoboJack.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('487', '7', '蜜蜂乐园', null, '1', '1', '3', 'PollenNation', '', 'BTN_PollenNation.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('488', '7', '女巫的财富', null, '1', '1', '3', 'RubyWitchesWealth', '', 'BTN_WitchesWealth.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('489', '7', '圣诞老人的疯狂', null, '1', '1', '3', 'SantasWildRide', '', 'BTN_SantasWildRide.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('490', '7', '水果财富', null, '1', '1', '3', 'RubyWheelofWealth', '', 'BTN_WheelofWealth.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('491', '7', '万圣节', null, '1', '1', '3', 'RubyHalloweenies', '', 'BTN_Halloweenies.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('492', '7', '炫富一族', null, '1', '1', '3', 'Loaded', '', 'BTN_Loaded.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('493', '7', '野性的狼群', null, '1', '1', '3', 'UntamedWolfPack', '', 'BTN_UntamedWolfPack.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('494', '7', '野性的孟加拉虎', null, '1', '1', '3', 'UntamedBengalTiger', '', 'BTN_UntamedBengalTiger.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('495', '7', '银行爆破', null, '1', '1', '3', 'BreakDaBankAgain', '', 'BTN_BreakDaBankAgain.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('496', '7', '月光', null, '1', '1', '3', 'Moonshine', '', 'BTN_Moonshine.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('497', '7', '征服钱海', null, '1', '1', '3', 'BigKahuna', '', 'BTN_BigKahuna.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('498', '7', '终极杀手', null, '1', '1', '3', 'RubyHitman', '', 'BTN_HitMan.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('499', '7', '星云', null, '1', '1', '3', 'Starscape', '', 'BTN_Starscape.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('500', '7', '射击', null, '1', '1', '3', 'Shoot', '', 'BTN_Shoot.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('501', '7', '狗爸爸', null, '1', '1', '3', 'RubyDogfather', '', 'BTN_RubyDogfather.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('502', '7', '地狱男爵', null, '1', '1', '3', 'RubyHellBoy', '', 'BTN_RubyHellBoy.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('503', '7', '幻影现金', null, '1', '1', '3', 'PhantomCash', '', 'BTN_PhantomCash.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('504', '7', '龙之家', null, '1', '1', '3', 'RubyHouseofDragons', '', 'BTN_RubyHouseofDragons.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('505', '7', '亚瑟王', null, '1', '1', '3', 'RubyKingArthur', '', 'BTN_RubyKingArthur.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('506', '7', '财富之轮', null, '1', '1', '3', 'Spectacular', '', 'BTN_SpectacularWheelOfWealth.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('507', '7', '财运疯狂', null, '1', '1', '3', 'CashCrazy', '', 'BTN_CashCrazy.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('508', '7', '超级厨王', null, '1', '1', '3', 'Belissimo', '', 'BTN_Belissimo.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('509', '7', '超级飞行员', null, '1', '1', '3', 'RubyFlyingAce', '', 'BTN_FlyingAce.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('510', '7', '黄金海岸', null, '1', '1', '3', 'RubyGoldCoast', '', 'BTN_GoldCoast.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('511', '7', '黄金龙', null, '1', '1', '3', 'gdragon', '', 'BTN_GoldenDragon.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('512', '7', '急速转轮', null, '1', '1', '3', 'RubyRapidReels', '', 'BTN_RapidReels.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('513', '7', '酷巴克', null, '1', '1', '3', 'CoolBuck', '', 'BTN_CoolBuck.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('514', '7', '累计奖金快车', null, '1', '1', '3', 'jexpress', '', 'BTN_JackpotExpress.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('515', '7', '铃儿响叮当', null, '1', '1', '3', 'RubyJingleBells', '', 'BTN_JingleBells.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('516', '7', '罗马财富', null, '1', '1', '3', 'RomanRiches', '', 'BTN_RomanRiches.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('517', '7', '奇妙7', null, '1', '1', '3', 'fan7', '', 'BTN_Fantastic7s.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('518', '7', '抢银行', null, '1', '1', '3', 'BreakDaBank', '', 'BTN_BreakDaBank.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('519', '7', '燃尼巨蟒', null, '1', '1', '3', 'zebra', '', 'BTN_ZanyZebra.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('520', '7', '三魔法', null, '1', '1', '3', 'TripleMagic', '', 'BTN_TripleMagic.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('521', '7', '沙发土豆', null, '1', '1', '3', 'CouchPotato', '', 'BTN_CouchPotato.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('522', '7', '双魔', null, '1', '1', '3', 'dm', '', 'BTN_DoubleMagic.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('523', '7', '双重韦密', null, '1', '1', '3', 'DoubleWammy', '', 'BTN_DoubleWammy.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('524', '7', '水果老虎机', null, '1', '1', '3', 'fruits', '', 'BTN_FruitSlots.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('525', '7', '现金蚬', null, '1', '1', '3', 'CashClams', '', 'BTN_CashClams.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('526', '7', '幸运曲奇', null, '1', '1', '3', 'FortuneCookie', '', 'BTN_FortuneCookie.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('527', '7', '摇滚船', null, '1', '1', '3', 'RockTheBoat', '', 'BTN_RockTheBoat.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('528', '7', '遗产L', null, '1', '1', '3', 'RubyLegacy', '', 'BTN_Legacy.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('529', '7', '宇宙猫', null, '1', '1', '3', 'cosmicc', '', 'BTN_CosmicCat.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('530', '7', '野生捕鱼', null, '1', '1', '3', 'WildCatch', '', 'BTN_WildCatch.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('531', '7', '甜蜜的收获', null, '1', '1', '3', 'SweetHarvest', '', 'BTN_SweetHarvest.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('532', '7', '棒球直击', null, '1', '1', '3', 'RubyHotShot', '', 'BTN_HotShot.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('533', '7', '不朽的爱情', null, '1', '1', '3', 'ImmortalRomance', '', 'BTN_ImmortalRomance.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('534', '7', '大狮鹫', null, '1', '1', '3', 'GreatGriffin', '', 'BTN_GreatGriffin.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('535', '7', '大熊猫', null, '1', '1', '3', 'UntamedGiantPanda', '', 'BTN_UntamedGiantPanda.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('536', '7', '东方财富', null, '1', '1', '3', 'OrientalFortune', '', 'BTN_OrientalFortune.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('537', '7', '疯狂的变色龙', null, '1', '1', '3', 'CrazyChameleons', '', 'BTN_CrazyChameleons.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('538', '7', '怪兽多多', null, '1', '1', '3', 'SoManyMonsters', '', 'BTN_SoManyMonsters.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('539', '7', '怪物躁狂症', null, '1', '1', '3', 'MonsterMania', '', 'BTN_MonsterMania.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('540', '7', '红衣女郎', null, '1', '1', '3', 'LadyInRed', '', 'BTN_LadyInRed.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('541', '7', '紅唇誘惑', null, '1', '1', '3', 'RedHotDevil', '', 'BTN_RedHotDevil.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('542', '7', '黄金囊地鼠', null, '1', '1', '3', 'GopherGold', '', 'BTN_GopherGold.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('543', '7', '黃金時代', null, '1', '1', '3', 'GoldenEra', '', 'BTN_GoldenEra.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('544', '7', '精灵宝石', null, '1', '1', '3', 'GeniesGems', '', 'BTN_GeniesGems.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('545', '7', '狂欢节', null, '1', '1', '3', 'Carnaval', '', 'BTN_Carnaval.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('546', '7', '昆虫派对', null, '1', '1', '3', 'RubyCashapillar', '', 'BTN_Cashapillar.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('547', '7', '雷电击', null, '1', '1', '3', 'ReelThunder', '', 'BTN_ReelThunder.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('548', '7', '马戏篷', null, '1', '1', '3', 'BigTop', '', 'BTN_BigTop.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('549', '7', '猫头鹰乐园', null, '1', '1', '3', 'WhatAHoot', '', 'BTN_WhatAHoot.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('550', '7', '燃烧的欲望', null, '1', '1', '3', 'RubyBurningDesire', '', 'BTN_BurningDesire.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('551', '7', '闪亮的圣诞节', null, '1', '1', '3', 'RubyDeckTheHalls', '', 'BTN_DeckTheHalls.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('552', '7', '守财奴', null, '1', '1', '3', 'RubyScrooge', '', 'BTN_Scrooge.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('553', '7', '寿司多多', null, '1', '1', '3', 'SoMuchSushi', '', 'BTN_SoMuchSushi.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('554', '7', '探索太阳', null, '1', '1', '3', 'SunQuest', '', 'BTN_SunQuest.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('555', '7', '糖果多多', null, '1', '1', '3', 'SoMuchCandy', '', 'BTN_SoMuchCandy.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('556', '7', '图腾宝藏', null, '1', '1', '3', 'RubyTotemTreasure', '', 'BTN_TotemTreasure.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('557', '7', '新年快樂', null, '1', '1', '3', 'HappyNewYear', '', 'BTN_HappyNewYear.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('558', '7', '乙烯基倒计时', null, '1', '1', '3', 'VinylCountdown', '', 'BTN_VinylCountdown.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('559', '7', '赢得向导', null, '1', '1', '3', 'wwizards', '', 'BTN_WinningWizards.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('560', '7', '招财鞭炮', null, '1', '1', '3', 'LuckyFirecracker', '', 'BTN_LuckyFirecracker.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('561', '7', '太阳神之许珀里翁', null, '1', '1', '3', 'TitansoftheSunHyperion', '', 'BTN_TitansoftheSunHyperion.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('562', '7', '爱丽娜', null, '1', '1', '3', 'Ariana', '', 'BTN_Ariana.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('563', '7', '橄榄球明星', null, '1', '1', '3', 'RugbyStar', '', 'BTN_RugbyStar.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('564', '7', '地府烈焰', null, '1', '1', '3', 'HotAsHades', '', 'BTN_HotAsHades.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('565', '7', '千金', null, '1', '1', '3', 'GoldenPrincess', '', 'BTN_GoldenPrincess.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('566', '7', '幸运生肖', null, '1', '1', '3', 'LuckyZodiac', '', 'BTN_LuckyZodiac.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('567', '7', '阿瓦隆', null, '1', '1', '3', 'RubyAvalon', '', 'BTN_Avalon.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('568', '7', '宝石迷阵', null, '1', '1', '3', 'ReelGems', '', 'BTN_ReelGems.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('569', '7', '春假', null, '1', '1', '3', 'SpringBreak', '', 'BTN_SpringBreak.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('570', '7', '疯狂赛道', null, '1', '1', '3', 'RubyGoodToGo', '', 'BTN_GoodToGo.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('571', '7', '复古卷轴钻石耀眼', null, '1', '1', '3', 'RetroReelsDiamondGlitz', '', 'BTN_RetroReelsDiamondGlitz.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('572', '7', '复古旋转', null, '1', '1', '3', 'RetroReels', '', 'BTN_RetroReels.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('573', '7', '宫廷历险', null, '1', '1', '3', 'AdventurePalace', '', 'BTN_AdventurePalace.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('574', '7', '怪兽曼琪肯', null, '1', '1', '3', 'RubyMunchkins', '', 'BTN_Munchkins.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('575', '7', '哈维斯的晚餐', null, '1', '1', '3', 'RubyHarveys', '', 'BTN_Harveys.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('576', '7', '海滨财富', null, '1', '1', '3', 'MonteCarloRiches', '', 'BTN_RivieraRiches.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('577', '7', '嗬嗬嗬', null, '1', '1', '3', 'HoHoHo', '', 'BTN_HoHoHo.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('578', '7', '黄金工厂', null, '1', '1', '3', 'GoldFactory', '', 'BTN_GoldFactory.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('579', '7', '卷行使价', null, '1', '1', '3', 'ReelStrike', '', 'BTN_ReelStrike.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('580', '7', '卡萨缦都', null, '1', '1', '3', 'RubyKathmandu', '', 'BTN_Kathmandu.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('581', '7', '酷狼', null, '1', '1', '3', 'CoolWolf', '', 'BTN_CoolWolf.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('582', '7', '秘密崇拜者', null, '1', '1', '3', 'SecretAdmirer', '', 'BTN_SecretAdmirer.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('583', '7', '女仕之夜', null, '1', '1', '3', 'LadiesNite', '', 'BTN_LadiesNite.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('584', '7', '派对鱼', null, '1', '1', '3', 'FishParty', '', 'BTN_FishParty.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('585', '7', '神奇墨水', null, '1', '1', '3', 'HotInk', '', 'BTN_HotInk.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('586', '7', '水果怪兽', null, '1', '1', '3', 'rubyelementals', '', 'BTN_Elementals.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('587', '7', '泰利嗬', null, '1', '1', '3', 'TallyHo', '', 'BTN_TallyHo.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('588', '7', '幸运女巫', null, '1', '1', '3', 'LuckyWitch', '', 'BTN_LuckyWitch.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('589', '7', '银芳', null, '1', '1', '3', 'SilverFang', '', 'BTN_SilverFang.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('590', '7', '疯狂假面', null, '1', '1', '3', 'MugshotMadness', '', 'BTN_MugshotMadness.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('591', '7', '玛雅公主', null, '1', '1', '3', 'MayanPrincess', '', 'BTN_MayanPrincess.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('592', '7', '章鱼', null, '1', '1', '3', 'Octopays', '', 'BTN_Octopays.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('593', '7', '对J高手', null, '1', '1', '3', 'Jackspwrpoker', '', 'BTN_Jackspwrpoker.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('594', '7', '红利扑克', null, '1', '1', '3', 'DoubleJoker', '', 'BTN_DoubleJoker.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('595', '7', 'A与人头扑克', null, '1', '1', '3', 'AcesfacesPwrPoker', '', 'BTN_AcesAndFaces.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('596', '7', '小丑扑克', null, '1', '1', '3', 'JokerPwrPoker', '', 'BTN_JokerPoker.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('597', '7', '换牌扑克', null, '1', '1', '3', 'DoubleDoubleBonus', '', 'BTN_DoubleDoubleBonus.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('598', '7', '路易斯安那双', null, '1', '1', '3', 'LouisianaDouble', '', 'BTN_LouisianaDouble.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('599', '7', '千斤顶或更好', null, '1', '1', '3', 'jacks', '', 'BTN_JacksOrBetter.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('600', '7', '数万或更好', null, '1', '1', '3', 'TensorBetterPwrPoker', '', 'BTN_TensOrBetter.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('601', '7', '万能两点', null, '1', '1', '3', 'DeucesWildPwrPoker', '', 'BTN_DeucesWild.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('602', '7', '超级马车赛', null, '1', '1', '3', 'PremierTrotting', '', 'BTN_PremierTrotting.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('603', '7', '超级赛马', null, '1', '1', '3', 'PremierRacing', '', 'BTN_PremierRacing.png', null, null, '0', '0', '100', '2017-09-16 19:17:48', '2017-09-16 19:17:48');
INSERT INTO `game_lists` VALUES ('604', '7', '电动宾果', null, '1', '1', '3', 'ElectroBingo', '', 'BTN_ElectroBingo.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('605', '7', '法老宾果', null, '1', '1', '3', 'PharaohBingo', '', 'BTN_PharaohBingo.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('606', '7', '国际鱼虾蟹骰宝', null, '1', '1', '3', 'CrownAndAnchor', '', 'BTN_CrownAndAnchor.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('607', '7', '魔法森林', null, '1', '1', '3', 'EnchantedWoods', '', 'BTN_EnchantedWoods.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('608', '7', '萨巴宾果', null, '1', '1', '3', 'SambaBingo', '', 'BTN_SambaBingo.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('609', '7', '细菌对对碰', null, '1', '1', '3', 'Germinator', '', 'BTN_Germinator.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('610', '7', 'A&8 红利5PK', null, '1', '1', '3', 'RubyAcesAndEights', '', 'BTN_RubyAcesAndEights.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('611', '7', 'All Aces 超级红利5PK', null, '1', '1', '3', 'RubyAllAces', '', 'BTN_RubyAllAces.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('612', '7', '百搭二王', null, '1', '1', '3', 'deuceswi', '', 'BTN_deuceswi.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('613', '7', '百搭二王与小丑', null, '1', '1', '3', 'DeucesandJoker', '', 'BTN_DeucesandJoker.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('614', '7', '百搭二王与小丑 (多组)', null, '1', '1', '3', 'DeucesJokerPwrPoker', '', 'BTN_DeucesJokerPwrPoker.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('615', '7', '超级百搭二王', null, '1', '1', '3', 'RubyBonusDeucesWild', '', 'BTN_RubyBonusDeucesWild.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('616', '7', '对十天王5PK', null, '1', '1', '3', 'TensorBetter', '', 'BTN_TensorBetter.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('617', '7', '红利5PK', null, '1', '1', '3', 'RubyBonusPokerDeluxe', '', 'BTN_RubyBonusPokerDeluxe.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('618', '7', '经典5PK', null, '1', '1', '3', 'acesfaces', '', 'BTN_acesfaces.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('619', '7', '美式经典5PK', null, '1', '1', '3', 'RubyAllAmerican', '', 'BTN_RubyAllAmerican.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('620', '7', '双倍红利5PK', null, '1', '1', '3', 'RubyDoubleBonusPoker', '', 'BTN_RubyDoubleBonusPoker.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('621', '7', '双倍小丑百搭5PK', null, '1', '1', '3', 'Jokerpok', '', 'BTN_Jokerpok.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('622', '7', '小丑百搭5PK(多组牌)', null, '1', '1', '3', 'DoubleJokerPwrPoker', '', 'BTN_DoubleJokerPwrPoker.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('623', '7', '百万动物园', null, '1', '1', '3', 'MegaMoolah', '', 'BTN_MegaMoolah.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('624', '7', '百万富翁', null, '1', '1', '3', 'MajorMillions', '', 'BTN_MegaMoolah.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('625', '7', '百万富翁5线', null, '1', '1', '3', 'MajorMillions5Reel', '', 'BTN_KingCashalot.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('626', '7', '百万伊西斯', null, '1', '1', '3', 'MegaMoolahIsis', '', 'BTN_MajorMillions.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('627', '7', '彩金二王', null, '1', '1', '3', 'JackpotDeuces', '', 'BTN_JackpotExpress1.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('628', '7', '富裕的国王', null, '1', '1', '3', 'KingCashaLot', '', 'BTN_KingCashaLot.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('629', '7', '加勒比海', null, '1', '1', '3', 'ProgCyberstud', '', 'BTN_ProgCyberstud.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('630', '7', '惊爆奖金', null, '1', '1', '3', 'WowPot', '', 'BTN_WowPot.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('631', '7', '尼罗河宝藏', null, '1', '1', '3', 'TreasureNile', '', 'BTN_TreasureNile.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('632', '7', '水果嘉年华(3线)', null, '1', '1', '3', 'FruitFiesta', '', 'MegaMoolahIsis.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('633', '7', '水果嘉年华(5线)', null, '1', '1', '3', 'FruitFiesta5Reel', '', 'LotsaLoot5Reel.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('634', '7', '图拉姆尼', null, '1', '1', '3', 'Tunzamunni', '', 'BTN_Tunzamunni.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('635', '7', '现金飞溅(3线)', null, '1', '1', '3', 'CashSplash', '', 'FruitFiesta.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('636', '7', '现金飞溅(5线)', null, '1', '1', '3', 'CashSplash5Reel', '', 'FruitFiesta.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('637', '7', '现金累积(3线)', null, '1', '1', '3', 'LotsofLoot', '', 'FruitFiesta.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('638', '7', '现金累积(5线)', null, '1', '1', '3', 'LotsaLoot5Reel', '', 'FruitFiesta5Reel.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('639', '7', '108好汉', null, '1', '1', '3', '108Heroes', '', 'BTN_108Heroes.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('640', '7', '21点矿坑', null, '1', '1', '3', 'BlackjackBonanza', '', 'BTN_BlackjackBonanza.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('641', '7', '777大哥大', null, '1', '1', '3', 'RubyGrand7s', '', 'BTN_RubyGrand7s.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('642', '7', 'K歌乐韵', null, '1', '1', '3', 'KaraokeParty', '', 'BTN_KaraokeParty.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('643', '7', '阿拉斯加垂钓', null, '1', '1', '3', 'AlaskanFishing', '', 'BTN_AlaskanFishing.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('644', '7', '埃及王朝', null, '1', '1', '3', 'ThroneOfEgypt', '', 'BTN_ThroneOfEgypt.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('645', '7', '爱虫', null, '1', '1', '3', 'LoveBugs', '', 'BTN_LoveBugs.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('646', '7', '爱尔兰之眼', null, '1', '1', '3', 'IrishEyes', '', 'BTN_IrishEyes.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('647', '7', '爱情医生', null, '1', '1', '3', 'DoctorLove', '', 'BTN_DoctorLove.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('648', '7', '奥林帕斯山的传说', null, '1', '1', '3', 'LegendOfOlympus', '', 'BTN_LegendOfOlympus.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('649', '7', '白金俱乐部', null, '1', '1', '3', 'PurePlatinum', '', 'BTN_PurePlatinum.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('650', '7', '板球明星', null, '1', '1', '3', 'Cricketstar', '', 'BTN_Cricketstar.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('651', '7', '伴娘我最大', null, '1', '1', '3', 'Bridesmaids', '', 'BTN_Bridesmaids.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('652', '7', '北极祕宝', null, '1', '1', '3', 'ArcticFortune', '', 'BTN_ArcticFortune.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('653', '7', '北极特务', null, '1', '1', '3', 'ArcticAgents', '', 'BTN_ArcticAgents.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('654', '7', '比基尼派对', null, '1', '1', '3', 'bikiniparty', '', 'BTN_bikiniparty.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('655', '7', '必胜', null, '1', '1', '3', 'RubySureWin', '', 'BTN_RubySureWin.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('656', '7', '冰雪圣诞村', null, '1', '1', '3', 'RubySantaPaws', '', 'BTN_RubySantaPaws.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('657', '7', '不给糖就捣蛋', null, '1', '1', '3', 'trickortreat', '', 'BTN_trickortreat.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('658', '7', '财炮连连', null, '1', '1', '3', 'GungPow', '', 'BTN_GungPow.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('659', '7', '城堡建筑师', null, '1', '1', '3', 'CastleBuilder', '', 'BTN_CastleBuilder.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('660', '7', '纯银3D', null, '1', '1', '3', 'SterlingSilver3D', '', 'BTN_SterlingSilver3D.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('661', '7', '刺热蝎子', null, '1', '1', '3', 'RubySizzlingScorpions', '', 'BTN_RubySizzlingScorpions.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('662', '7', '丛林吉姆', null, '1', '1', '3', 'RubyJungleJim', '', 'BTN_RubyJungleJim.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('663', '7', '丛林吉姆黄金国?', null, '1', '1', '3', 'JungleJim_ElDorado', '', 'BTN_JungleJim_ElDorado.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('664', '7', '丛林五霸', null, '1', '1', '3', 'big5', '', 'BTN_big5.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('665', '7', '丛林早餐', null, '1', '1', '3', 'RubyBigBreak', '', 'BTN_RubyBigBreak.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('666', '7', '大厨师', null, '1', '1', '3', 'BigChef', '', 'BTN_BigChef.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('667', '7', '大航海时代', null, '1', '1', '3', 'RubyAgeOfDiscovery', '', 'BTN_RubyAgeOfDiscovery.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('668', '7', '电音歌后', null, '1', '1', '3', 'ElectricDiva', '', 'BTN_ElectricDiva.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('669', '7', '东方珍兽', null, '1', '1', '3', 'wildorient', '', 'BTN_wildorient.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('670', '7', '东方之珠', null, '1', '1', '3', 'JewelsOfTheOrient', '', 'BTN_JewelsOfTheOrient.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('671', '7', '动物足球', null, '1', '1', '3', 'RubySoccerSafari', '', 'BTN_RubySoccerSafari.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('672', '7', '多台-银行抢匪2', null, '1', '1', '3', 'MSBreakDaBankAgain', '', 'BTN_MSBreakDaBankAgain.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('673', '7', '躲猫猫', null, '1', '1', '3', 'PeekaBoo5Reel', '', 'BTN_PeekaBoo5Reel.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('674', '7', '发财法老', null, '1', '1', '3', 'TootinCarMan ', '', 'BTN_TootinCarMan .png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('675', '7', '法老王的财富', null, '1', '1', '3', 'pharaohs', '', 'BTN_pharaohs.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('676', '7', '愤怒金猴', null, '1', '1', '3', 'RubyMoneyMadMonkey', '', 'BTN_RubyMoneyMadMonkey.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('677', '7', '丰满歌手', null, '1', '1', '3', 'FatLadySings', '', 'BTN_FatLadySings.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('678', '7', '疯狂80年代', null, '1', '1', '3', 'RubyCrazy80s', '', 'BTN_RubyCrazy80s.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('679', '7', '疯狂鳄鱼', null, '1', '1', '3', 'crocs', '', 'BTN_crocs.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('680', '7', '疯狂世界盃', null, '1', '1', '3', 'RubyWorldCupMania', '', 'BTN_RubyWorldCupMania.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('681', '7', '佛洛晚餐', null, '1', '1', '3', 'RubyFlosDiner', '', 'BTN_RubyFlosDiner.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('682', '7', '富裕人生', null, '1', '1', '3', 'LifeOfRiches', '', 'BTN_LifeOfRiches.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('683', '7', '海盗天堂', null, '1', '1', '3', 'pirates', '', 'BTN_pirates.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('684', '7', '海派甜心', null, '1', '1', '3', 'RubyCutesyPie', '', 'BTN_RubyCutesyPie.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('685', '7', '寒冰精灵', null, '1', '1', '3', 'RubyFrostBite', '', 'BTN_RubyFrostBite.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('686', '7', '黑绵羊咩咩叫', null, '1', '1', '3', 'BarBarBlackSheep5Reel', '', 'BTN_BarBarBlackSheep5Reel.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('687', '7', '红利炮竹', null, '1', '1', '3', 'crackerjack', '', 'BTN_crackerjack.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('688', '7', '猴子的宝藏', null, '1', '1', '3', 'monkeys', '', 'BTN_monkeys.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('689', '7', '蝴蝶仙境', null, '1', '1', '3', 'ButterFlies', '', 'BTN_ButterFlies.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('690', '7', '花粉之国', null, '1', '1', '3', 'PollenParty', '', 'BTN_PollenParty.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('691', '7', '欢乐骰子乐', null, '1', '1', '3', 'joyofsix', '', 'BTN_joyofsix.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('692', '7', '欢乐小丑', null, '1', '1', '3', 'jesters', '', 'BTN_jesters.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('693', '7', '环游世界', null, '1', '1', '3', 'RubyAroundTheWorld', '', 'BTN_RubyAroundTheWorld.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('694', '7', '黄金城市', null, '1', '1', '3', 'RubyCityofGold', '', 'BTN_RubyCityofGold.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('695', '7', '黄金哥布林', null, '1', '1', '3', 'goblinsgold', '', 'BTN_goblinsgold.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('696', '7', '黄金角斗士', null, '1', '1', '3', 'GladiatorsGold', '', 'BTN_GladiatorsGold.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('697', '7', '急冻钻石?', null, '1', '1', '3', 'FrozenDiamonds', '', 'BTN_FrozenDiamonds.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('698', '7', '疾风老鹰', null, '1', '1', '3', 'EaglesWings', '', 'BTN_EaglesWings.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('699', '7', '杰克与吉儿', null, '1', '1', '3', 'RRJackAndJill', '', 'BTN_RRJackAndJill.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('700', '7', '金毛骑士团', null, '1', '1', '3', 'JasonAndTheGoldenFleece', '', 'BTN_JasonAndTheGoldenFleece.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('701', '7', '金字塔的财富', null, '1', '1', '3', 'RiverofRiches', '', 'BTN_RiverofRiches.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('702', '7', '橘子摩卡', null, '1', '1', '3', 'RubyMochaOrange', '', 'BTN_RubyMochaOrange.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('703', '7', '开心点心', null, '1', '1', '3', 'WinSumDimSum', '', 'BTN_WinSumDimSum.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('704', '7', '凯蒂小屋', null, '1', '1', '3', 'KittyCabana', '', 'BTN_KittyCabana.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('705', '7', '酷热经典', null, '1', '1', '3', 'RetroReelsExtremeHeat', '', 'BTN_RetroReelsExtremeHeat.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('706', '7', '快乐假日', null, '1', '1', '3', 'HappyHolidays', '', 'BTN_HappyHolidays.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('707', '7', '拉美西斯宝藏', null, '1', '1', '3', 'RamessesRiches', '', 'BTN_RamessesRiches.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('708', '7', '篮球巨星', null, '1', '1', '3', 'BasketballStar', '', 'BTN_BasketballStar.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('709', '7', '劳斯莱斯', null, '1', '1', '3', 'royce', '', 'BTN_royce.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('710', '7', '老国王柯尔', null, '1', '1', '3', 'RROldKingColeV90', '', 'BTN_RROldKingColeV90.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('711', '7', '老虎月亮', null, '1', '1', '3', 'TigerMoon', '', 'BTN_TigerMoon.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('712', '7', '老鼠霸王', null, '1', '1', '3', 'RubyTheRatPack', '', 'BTN_RubyTheRatPack.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('713', '7', '力量之花', null, '1', '1', '3', 'flowerpower', '', 'BTN_flowerpower.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('714', '7', '烈火雄鹰', null, '1', '1', '3', 'FireHawk', '', 'BTN_FireHawk.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('715', '7', '猎犬酒店', null, '1', '1', '3', 'HoundHotel', '', 'BTN_HoundHotel.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('716', '7', '龙宫', null, '1', '1', '3', 'houseofdragons', '', 'BTN_houseofdragons.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('717', '7', '玫瑰之戒', null, '1', '1', '3', 'RubyRingsnRoses', '', 'BTN_RubyRingsnRoses.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('718', '7', '梅林的百万奖金', null, '1', '1', '3', 'MerlinsMillions', '', 'BTN_MerlinsMillions.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('719', '7', '美式酒吧', null, '1', '1', '3', 'BarsAndStripesV90', '', 'BTN_BarsAndStripesV90.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('720', '7', '迷失拉斯维加斯', null, '1', '1', '3', 'LostVegas', '', 'BTN_LostVegas.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('721', '7', '迷走星球', null, '1', '1', '3', 'Galacticons', '', 'BTN_Galacticons.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('722', '7', '咩咩黑羊', null, '1', '1', '3', 'RubyBarBarBlackSheep', '', 'BTN_RubyBarBarBlackSheep.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('723', '7', '命运女神', null, '1', '1', '3', 'RubyFortuna', '', 'BTN_RubyFortuna.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('724', '7', '命中红星', null, '1', '1', '3', 'RubyBullsEye', '', 'BTN_RubyBullsEye.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('725', '7', '魔鳄大帝', null, '1', '1', '3', 'Crocodopolis', '', 'BTN_Crocodopolis.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('726', '7', '魔法美人鱼', null, '1', '1', '3', 'EnchantedMermaid', '', 'BTN_EnchantedMermaid.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('727', '7', '魔法学园', null, '1', '1', '3', 'RubyMagicSpell', '', 'BTN_RubyMagicSpell.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('728', '7', '魔法阵', null, '1', '1', '3', 'RubySpellBound', '', 'BTN_RubySpellBound.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('729', '7', '魔术酋长', null, '1', '1', '3', 'chiefsmagic', '', 'BTN_chiefsmagic.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('730', '7', '魔术兔', null, '1', '1', '3', 'rabbitinthehat', '', 'BTN_rabbitinthehat.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('731', '7', '魔术箱', null, '1', '1', '3', 'MagicBoxes', '', 'BTN_MagicBoxes.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('732', '7', '内线交易', null, '1', '1', '3', 'RubyDonDeal', '', 'BTN_RubyDonDeal.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('733', '7', '宁静', null, '1', '1', '3', 'Serenity', '', 'BTN_Serenity.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('734', '7', '扭转世界', null, '1', '1', '3', 'RubyTwister', '', 'BTN_RubyTwister.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('735', '7', '女皇之心', null, '1', '1', '3', 'RRQueenOfHearts', '', 'BTN_RRQueenOfHearts.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('736', '7', '派对时刻', null, '1', '1', '3', 'partytime', '', 'BTN_partytime.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('737', '7', '泡泡矿坑', null, '1', '1', '3', 'BubbleBonanza', '', 'BTN_BubbleBonanza.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('738', '7', '漂亮猫咪', null, '1', '1', '3', 'PrettyKitty', '', 'BTN_PrettyKitty.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('739', '7', '七海的主权', null, '1', '1', '3', 'SovereignOfTheSevenSeas', '', 'BTN_SovereignOfTheSevenSeas.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('740', '7', '七武士', null, '1', '1', '3', 'RubySamuraiSevens', '', 'BTN_RubySamuraiSevens.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('741', '7', '企鹅家族', null, '1', '1', '3', 'PenguinSplash', '', 'BTN_PenguinSplash.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('742', '7', '千岛湖', null, '1', '1', '3', 'RubyThousandIslands', '', 'BTN_RubyThousandIslands.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('743', '7', '乔治与柏志', null, '1', '1', '3', 'RRGeorgiePorgie', '', 'BTN_RRGeorgiePorgie.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('744', '7', '青龙出海', null, '1', '1', '3', 'EmperorOfTheSea', '', 'BTN_EmperorOfTheSea.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('745', '7', '酋长的财富', null, '1', '1', '3', 'RubyChiefsFortune', '', 'BTN_RubyChiefsFortune.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('746', '7', '饶舌礼物', null, '1', '1', '3', 'GiftRap', '', 'BTN_GiftRap.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('747', '7', '忍者法宝', null, '1', '1', '3', 'ninjamagic', '', 'BTN_ninjamagic.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('748', '7', '森林之王-蛇和梯子', null, '1', '1', '3', 'RubyBigKahunaSnakesAndLadders', '', 'BTN_RubyBigKahunaSnakesAndLadders.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('749', '7', '神秘的百慕达', null, '1', '1', '3', 'TheBermudaMysteries', '', 'BTN_TheBermudaMysteries.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('750', '7', '神秘的诱惑', null, '1', '1', '3', 'magiccharms', '', 'BTN_magiccharms.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('751', '7', '神秘女枪手', null, '1', '1', '3', 'Pistoleras', '', 'BTN_Pistoleras.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('752', '7', '失落的国度', null, '1', '1', '3', 'ForsakenKingdom', '', 'BTN_ForsakenKingdom.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('753', '7', '暑假时光', null, '1', '1', '3', 'RubySummertime', '', 'BTN_RubySummertime.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('754', '7', '双倍剂量', null, '1', '1', '3', 'RubyDoubleDose', '', 'BTN_RubyDoubleDose.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('755', '7', '水果vs糖果', null, '1', '1', '3', 'FruitVSCandy', '', 'BTN_FruitVSCandy.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('756', '7', '水果大战', null, '1', '1', '3', 'RubyFrootLoot', '', 'BTN_RubyFrootLoot.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('757', '7', '水果沙拉', null, '1', '1', '3', 'RubyFruitSalad', '', 'BTN_RubyFruitSalad.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('758', '7', '松鼠幼稚园', null, '1', '1', '3', 'RubyCabinFever', '', 'BTN_RubyCabinFever.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('759', '7', '锁子甲', null, '1', '1', '3', 'Chainmailnew', '', 'BTN_Chainmailnew.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('760', '7', '太阳神之忒伊亚', null, '1', '1', '3', 'TitansoftheSunTheia', '', 'BTN_TitansoftheSunTheia.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('761', '7', '太阳征程?', null, '1', '1', '3', 'suntide', '', 'BTN_suntide.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('762', '7', '泰山传奇', null, '1', '1', '3', 'Tarzan', '', 'BTN_Tarzan.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('763', '7', '泰坦帝国', null, '1', '1', '3', 'StashoftheTitans', '', 'BTN_StashoftheTitans.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('764', '7', '天王星', null, '1', '1', '3', 'RubyAstronomical', '', 'BTN_RubyAstronomical.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('765', '7', '外星大袭击', null, '1', '1', '3', 'MaxDamageSlot', '', 'BTN_MaxDamageSlot.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('766', '7', '万兽之王', null, '1', '1', '3', 'lions', '', 'BTN_lions.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('767', '7', '网球冠军', null, '1', '1', '3', 'RubyCentreCourt', '', 'BTN_RubyCentreCourt.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('768', '7', '舞动佛罗里达', null, '1', '1', '3', 'RubyFloriditaFandango', '', 'BTN_RubyFloriditaFandango.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('769', '7', '舞龙', null, '1', '1', '3', 'dragondance', '', 'BTN_dragondance.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('770', '7', '西部边境', null, '1', '1', '3', 'westernfrontier', '', 'BTN_westernfrontier.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('771', '7', '侠盗猎车手', null, '1', '1', '3', '5ReelDrive', '', 'BTN_5ReelDrive.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('772', '7', '现金船长', null, '1', '1', '3', 'RubyCaptainCash', '', 'BTN_RubyCaptainCash.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('773', '7', '现金之王', null, '1', '1', '3', 'KingsOfCash', '', 'BTN_KingsOfCash.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('774', '7', '小丑8000', null, '1', '1', '3', 'RubyJoker8000', '', 'BTN_RubyJoker8000.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('775', '7', '小丑杰克', null, '1', '1', '3', 'RubyJackintheBox', '', 'BTN_RubyJackintheBox.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('776', '7', '星尘', null, '1', '1', '3', 'StarDust', '', 'BTN_StarDust.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('777', '7', '幸运鲍比', null, '1', '1', '3', 'Bobby7s', '', 'BTN_Bobby7s.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('778', '7', '幸运海洋', null, '1', '1', '3', 'oceans', '', 'BTN_oceans.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('779', '7', '幸运连线', null, '1', '1', '3', 'HighFive', '', 'BTN_HighFive.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('780', '7', '幸运龙宝贝', null, '1', '1', '3', 'Dragonz', '', 'BTN_Dragonz.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('781', '7', '幸运魔术师', null, '1', '1', '3', 'LuckyCharmer', '', 'BTN_LuckyCharmer.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('782', '7', '幸运双星', null, '1', '1', '3', 'luckytwins', '', 'BTN_luckytwins.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('783', '7', '幸运小妖', null, '1', '1', '3', 'LuckyLeprechaunsLoot', '', 'BTN_LuckyLeprechaunsLoot.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('784', '7', '幸运妖精', null, '1', '1', '3', 'LuckyLeprechaun', '', 'BTN_LuckyLeprechaun.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('785', '7', '幸运鑽石', null, '1', '1', '3', 'RubyDiamond7s', '', 'BTN_RubyDiamond7s.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('786', '7', '轩辕帝传', null, '1', '1', '3', 'Huangdi_TYE', '', 'BTN_Huangdi_TYE.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('787', '7', '旋转大战', null, '1', '1', '3', 'reelspinner', '', 'BTN_reelspinner.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('788', '7', '寻找天堂', null, '1', '1', '3', 'ParadiseFound ', '', 'BTN_ParadiseFound .png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('789', '7', '妖精之环', null, '1', '1', '3', 'RubyFairyRing', '', 'BTN_RubyFairyRing.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('790', '7', '伊西斯', null, '1', '1', '3', 'Isis', '', 'BTN_Isis.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('791', '7', '音速战机', null, '1', '1', '3', 'RubySonicBoom', '', 'BTN_RubySonicBoom.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('792', '7', '银行抢匪2', null, '1', '1', '3', 'RubyBreakDaBankAgainV90', '', 'BTN_RubyBreakDaBankAgainV90.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('793', '7', '樱桃红', null, '1', '1', '3', 'CherryRed', '', 'BTN_CherryRed.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('794', '7', '优质房仲', null, '1', '1', '3', 'RubyPrimePropertyV90', '', 'BTN_RubyPrimePropertyV90.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('795', '7', '游乐园', null, '1', '1', '3', 'RubyFunHouse', '', 'BTN_RubyFunHouse.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('796', '7', '增强马力', null, '1', '1', '3', 'RubySupeItUp', '', 'BTN_RubySupeItUp.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('797', '7', '正中红心', null, '1', '1', '3', 'BullseyeGameshow', '', 'BTN_BullseyeGameshow.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('798', '7', '重金属', null, '1', '1', '3', 'RubyHeavyMetal', '', 'BTN_RubyHeavyMetal.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('799', '7', '侏儸纪彩金', null, '1', '1', '3', 'jurassicjackpot', '', 'BTN_jurassicjackpot.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('800', '7', '侏儸纪彩金 (超级)', null, '1', '1', '3', 'jurassicbr', '', 'BTN_jurassicbr.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('801', '7', '侏儸纪公园', null, '1', '1', '3', 'jurassicpark', '', 'BTN_jurassicpark.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('802', '7', '珠宝大盗', null, '1', '1', '3', 'RubyJewelThief', '', 'BTN_RubyJewelThief.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('803', '7', '逐鹿三国', null, '1', '1', '3', '3empires', '', 'BTN_3empires.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('804', '7', '抓鬼躲猫猫', null, '1', '1', '3', 'RubyPeekaBoo', '', 'BTN_RubyPeekaBoo.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('805', '7', '自由精神-财富之轮', null, '1', '1', '3', 'RubyFreeSpirit', '', 'BTN_RubyFreeSpirit.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('806', '7', '鑽石交易', null, '1', '1', '3', 'RubyDiamondDeal', '', 'BTN_RubyDiamondDeal.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('807', '7', '3张牌扑克', null, '1', '1', '3', '3CardPoker', '', 'BTN_3CardPoker.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('808', '7', '3张牌扑克(多组牌)', null, '1', '1', '3', 'MH3CardPokerGold', '', 'BTN_MH3CardPokerGold.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('809', '7', '百家乐', null, '1', '1', '3', 'Baccarat', '', 'BTN_Baccarat.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('810', '7', '德洲扑克(多组牌)', null, '1', '1', '3', 'RubyMHHoldemHigh', '', 'BTN_RubyMHHoldemHigh.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('811', '7', '法式轮盘', null, '1', '1', '3', 'FrenchRoulette', '', 'BTN_FrenchRoulette.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('812', '7', '高限额百家乐', null, '1', '1', '3', 'HighLimitBaccarat', '', 'BTN_HighLimitBaccarat.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('813', '7', '红狗', null, '1', '1', '3', 'RedDog', '', 'BTN_RedDog.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('814', '7', '猴子基诺', null, '1', '1', '3', 'MonkeyKeno', '', 'BTN_MonkeyKeno.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('815', '7', '花旗骰', null, '1', '1', '3', 'Craps', '', 'BTN_Craps.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('816', '7', '换牌德扑', null, '1', '1', '3', 'RubyTriplePocketPoker', '', 'BTN_RubyTriplePocketPoker.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('817', '7', '皇家赛马', null, '1', '1', '3', 'RubyRoyalDerby', '', 'BTN_RubyRoyalDerby.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('818', '7', '黄金版3张牌扑克', null, '1', '1', '3', '3CardPokerGold', '', 'BTN_3CardPokerGold.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('819', '7', '黄金版百家乐', null, '1', '1', '3', 'BaccaratGold', '', 'BTN_BaccaratGold.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('820', '7', '黄金版複式轮盘', null, '1', '1', '3', 'MultiWheelRouletteGold', '', 'BTN_MultiWheelRouletteGold.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('821', '7', '加勒比海扑克', null, '1', '1', '3', 'Cyberstud', '', 'BTN_Cyberstud.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('822', '7', '快乐彩', null, '1', '1', '3', 'Keno', '', 'BTN_Keno.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('823', '7', '玛雅宾果', null, '1', '1', '3', 'mayanbingo', '', 'BTN_mayanbingo.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('824', '7', '美式轮盘', null, '1', '1', '3', 'AmericanRoulette', '', 'BTN_AmericanRoulette.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('825', '7', '欧洲21点', null, '1', '1', '3', 'Roulette', '', 'BTN_Roulette.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('826', '7', '扑克游戏', null, '1', '1', '3', 'FlipCard', '', 'BTN_FlipCard.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('827', '7', '扑克追击', null, '1', '1', '3', 'PokerPursuit', '', 'BTN_PokerPursuit.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('828', '7', '三张扑克(多组牌)', null, '1', '1', '3', 'HighSpeedPoker', '', 'BTN_HighSpeedPoker.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('829', '7', '骰宝', null, '1', '1', '3', 'Sicbo', '', 'BTN_Sicbo.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('830', '7', '完美欧洲21点', null, '1', '1', '3', 'RubyMHPerfectPairs', '', 'BTN_RubyMHPerfectPairs.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('831', '7', '义大利轮盘', null, '1', '1', '3', 'Spingo', '', 'BTN_Spingo.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('832', '7', '越位罚球', null, '1', '1', '3', 'RubyOffsideAndSeek', '', 'BTN_RubyOffsideAndSeek.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('833', '7', '总统轮盘', null, '1', '1', '3', 'RubyPremierRoulette', '', 'BTN_RubyPremierRoulette.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('834', '7', '鑽石总统轮盘', null, '1', '1', '3', 'PremierRouletteDE', '', 'BTN_PremierRouletteDE.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('835', '7', '阿嬷赛车', null, '1', '1', '3', 'RubyGrannyPrix', '', 'BTN_RubyGrannyPrix.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('836', '7', '保龄球', null, '1', '1', '3', 'RubyBowledOver', '', 'BTN_RubyBowledOver.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('837', '7', '超级零英雄', null, '1', '1', '3', 'RubySuperZeroes', '', 'BTN_RubySuperZeroes.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('838', '7', '打地鼠', null, '1', '1', '3', 'RubyWhackAJackpot', '', 'BTN_RubyWhackAJackpot.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('839', '7', '地穴探索', null, '1', '1', '3', 'RubyCryptCrusade', '', 'BTN_RubyCryptCrusade.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('840', '7', '放克篮球', null, '1', '1', '3', 'RubySlamFunk', '', 'BTN_RubySlamFunk.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('841', '7', '刮刮乐玩家', null, '1', '1', '3', 'IWCardSelector', '', 'BTN_IWCardSelector.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('842', '7', '刮刮乐玩家', null, '1', '1', '3', 'IWCardSelector', '', 'BTN_IWCardSelector.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('843', '7', '怪怪小精灵', null, '1', '1', '3', 'RubyHairyFairies', '', 'BTN_RubyHairyFairies.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('844', '7', '好运泡沫', null, '1', '1', '3', 'RubyFoamyFortunes', '', 'BTN_RubyFoamyFortunes.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('845', '7', '黄金地穴探索', null, '1', '1', '3', 'RubyCryptCrusadeGold', '', 'BTN_RubyCryptCrusadeGold.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('846', '7', '黄金强劫西部银行', null, '1', '1', '3', 'RubySixShooterLooterGold', '', 'BTN_RubySixShooterLooterGold.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('847', '7', '黄金兔子逃脱', null, '1', '1', '3', 'RubyBunnyBoilerGold', '', 'BTN_RubyBunnyBoilerGold.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('848', '7', '黄金外星探险', null, '1', '1', '3', 'RubySpaceEvaderGold', '', 'BTN_RubySpaceEvaderGold.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('849', '7', '急冻冰块', null, '1', '1', '3', 'RubyFreezingFuzzballs', '', 'BTN_RubyFreezingFuzzballs.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('850', '7', '金黄怪物足球', null, '1', '1', '3', 'RubyGoldenGhouls', '', 'BTN_RubyGoldenGhouls.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('851', '7', '恐怖麵包', null, '1', '1', '3', 'RubyDawnOfTheBread', '', 'BTN_RubyDawnOfTheBread.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('852', '7', '掠夺海洋', null, '1', '1', '3', 'RubyPlunderTheSea', '', 'BTN_RubyPlunderTheSea.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('853', '7', '孟买水晶球', null, '1', '1', '3', 'RubyMumbaiMagic', '', 'BTN_RubyMumbaiMagic.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('854', '7', '啤酒节', null, '1', '1', '3', 'RubyBeerFest', '', 'BTN_RubyBeerFest.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('855', '7', '强劫西部银行', null, '1', '1', '3', 'RubySixShooterLooter', '', 'BTN_RubySixShooterLooter.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('856', '7', '兔子逃脱', null, '1', '1', '3', 'RubyBunnyBoiler', '', 'BTN_RubyBunnyBoiler.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('857', '7', '外星探险', null, '1', '1', '3', 'RubySpaceEvader', '', 'BTN_RubySpaceEvader.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('858', '7', '网球刮刮乐', null, '1', '1', '3', 'RubyGameSetAndScratch', '', 'BTN_RubyGameSetAndScratch.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('859', '7', '财富轮盘', null, '1', '1', '3', 'RubyWheelOfRiches', '', 'BTN_RubyWheelOfRiches.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('860', '7', '彩色蜂窝', null, '1', '1', '3', 'Hexaline', '', 'BTN_Hexaline.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('861', '7', '彩色三角', null, '1', '1', '3', 'Triangulation', '', 'BTN_Triangulation.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('862', '7', '超级宾果', null, '1', '1', '3', 'RubySuperBonusBingo', '', 'BTN_RubySuperBonusBingo.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('863', '7', '冲浪度假', null, '1', '1', '3', 'RubyIWBigBreak', '', 'BTN_RubyIWBigBreak.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('864', '7', '冲浪海龟', null, '1', '1', '3', 'RubyTurtleyAwesome', '', 'BTN_RubyTurtleyAwesome.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('865', '7', '弹道宾果', null, '1', '1', '3', 'RubyBallisticBingo', '', 'BTN_RubyBallisticBingo.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('866', '7', '动物冠军', null, '1', '1', '3', 'RubyWildChampions', '', 'BTN_RubyWildChampions.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('867', '7', '法老王的宝物', null, '1', '1', '3', 'RubyPharaohsGems', '', 'BTN_RubyPharaohsGems.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('869', '7', '火山弹珠台', null, '1', '1', '3', 'RubyKashatoa', '', 'BTN_RubyKashatoa.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('870', '7', '剪刀石头布', null, '1', '1', '3', 'RubyHandToHandCombat', '', 'BTN_RubyHandToHandCombat.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('871', '7', '晋级扑克', null, '1', '1', '3', 'RubyCardClimber', '', 'BTN_RubyCardClimber.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('872', '7', '昆虫派对', null, '1', '1', '3', 'RubyIWCashapillar', '', 'BTN_RubyIWCashapillar.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('873', '7', '三转轮', null, '1', '1', '3', 'RubyThreeWheeler', '', 'BTN_RubyThreeWheeler.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('874', '7', '杀手俱乐部', null, '1', '1', '3', 'RubyKillerClubs', '', 'BTN_RubyKillerClubs.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('875', '7', '圣龙赐福', null, '1', '1', '3', 'RubyDragonsFortune', '', 'BTN_RubyDragonsFortune.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('876', '7', '万圣节派对', null, '1', '1', '3', 'RubyIWHalloweenies', '', 'BTN_RubyIWHalloweenies.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('877', '7', '幸运数字', null, '1', '1', '3', 'RubyLuckyNumbers', '', 'BTN_RubyLuckyNumbers.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('878', '7', '致富宾果', null, '1', '1', '3', 'RubyBingoBonanza', '', 'BTN_RubyBingoBonanza.png', null, null, '0', '0', '100', '2017-09-16 19:17:49', '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('881', '22', '超级斗地主', null, '1', '1', '3', '20', null, '20.jpg', null, null, '0', '0', '100', '2017-10-05 05:00:32', '2017-10-05 05:04:56');
INSERT INTO `game_lists` VALUES ('882', '23', '幸运钻石', null, '1', '1', '3', '', 'photosafari', '幸运钻石.png', null, null, '0', '0', '100', '2017-10-06 09:08:25', '2017-10-23 06:37:29');
INSERT INTO `game_lists` VALUES ('883', '25', '魔力棒', null, '1', '1', '3', '754', null, 'bb554.png', null, null, '0', '0', '100', '2017-10-06 18:13:55', '2017-10-06 18:14:00');
INSERT INTO `game_lists` VALUES ('940', '23', '万能西瓜', null, '1', '1', '3', null, 'wildmelon', '万能西瓜.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('885', '14', '猴年--大吉', null, '1', '1', '3', 'EGIGame', 'EGIGame', null, null, null, '0', '0', '1000', '2017-10-09 22:49:32', '2017-10-09 22:50:29');
INSERT INTO `game_lists` VALUES ('886', '8', '武松打虎', null, '1', '1', '3', '1037', 'TigerSlayer', 'watermargin.jpg', null, null, '0', '0', '1000', '2017-10-23 00:38:55', '2017-10-23 03:36:30');
INSERT INTO `game_lists` VALUES ('887', '26', '发财神龟', null, '1', '1', '3', 'sw_888t', '', 'sw_888t.jpg', null, '', '0', '0', '1000', '2017-10-23 00:47:40', '2017-10-23 02:47:21');
INSERT INTO `game_lists` VALUES ('888', '26', '捕鱼多福', null, '1', '1', '3', 'sw_fufish_intw', null, 'sw_fufish_intw.jpg', null, '', '0', '0', '1000', '2017-10-23 04:06:06', '2017-10-23 04:11:16');
INSERT INTO `game_lists` VALUES ('889', '26', '亚马逊美人', null, '1', '1', '3', 'sw_al', null, 'sw_al.jpg', null, '', '0', '0', '1000', '2017-10-23 04:12:00', '2017-10-23 04:24:22');
INSERT INTO `game_lists` VALUES ('890', '26', '捕鱼多福奖池', null, '1', '1', '3', 'sw_fufish-jp', null, 'sw_fufish-jp.jpg', null, '', '0', '0', '1000', '2017-10-23 04:12:48', '2017-10-23 04:24:22');
INSERT INTO `game_lists` VALUES ('891', '26', '大黑赐福', null, '1', '1', '3', 'sw_dhcf', null, 'sw_dhcf.jpg', null, '', '0', '0', '1000', '2017-10-23 04:13:15', '2017-10-23 04:24:09');
INSERT INTO `game_lists` VALUES ('892', '26', '闪电之神', null, '1', '1', '3', 'sw_gol', null, 'sw_gol.jpg', null, 'sw_gol.jpg', '0', '0', '1000', '2017-10-23 04:13:35', '2017-10-23 04:24:11');
INSERT INTO `game_lists` VALUES ('893', '26', '快乐海豚', null, '1', '1', '3', 'sw_dd', null, 'sw_dd.jpg', null, 'sw_dd.jpg', '0', '0', '1000', '2017-10-23 04:13:57', '2017-10-23 04:24:11');
INSERT INTO `game_lists` VALUES ('894', '26', '猩猩月亮', null, '1', '1', '3', 'sw_gm', null, 'sw_gm.jpg', null, '', '0', '0', '1000', '2017-10-23 04:14:20', '2017-10-23 04:24:13');
INSERT INTO `game_lists` VALUES ('895', '26', '双重奖励老虎机', null, '1', '1', '3', 'sw_db', null, 'sw_db.jpg', null, null, '0', '0', '1000', '2017-10-23 04:14:44', '2017-10-23 04:24:13');
INSERT INTO `game_lists` VALUES ('896', '26', '龙鲤传奇', null, '1', '1', '3', 'sw_lodk', null, 'sw_lodk.jpg', null, 'sw_lodk.jpg', '0', '0', '1000', '2017-10-23 04:15:11', '2017-10-23 04:24:13');
INSERT INTO `game_lists` VALUES ('897', '26', '丛林翻倍赢', null, '1', '1', '3', 'sw_dj', null, 'sw_dj.jpg', '', 'sw_dj.jpg', '0', '0', '1000', '2017-10-23 04:15:39', '2017-10-23 04:24:14');
INSERT INTO `game_lists` VALUES ('898', '26', '吉祥招财猫', null, '1', '1', '3', 'sw_mf', null, 'sw_mf.jpg', null, 'sw_mf.jpg', '0', '0', '1000', '2017-10-23 04:15:59', '2017-10-23 04:24:14');
INSERT INTO `game_lists` VALUES ('899', '26', '浴火凤凰', null, '1', '1', '3', 'sw_fp', null, 'sw_fp.jpg', null, 'sw_fp.jpg', '0', '0', '1000', '2017-10-23 04:16:26', '2017-10-23 04:24:15');
INSERT INTO `game_lists` VALUES ('900', '26', '美人鱼', null, '1', '1', '3', 'sw_mer', null, 'sw_mer.jpg', null, 'sw_mer.jpg', '0', '0', '1000', '2017-10-23 04:16:48', '2017-10-23 04:24:15');
INSERT INTO `game_lists` VALUES ('901', '26', '福宝宝', null, '1', '1', '3', 'sw_fbb', null, 'sw_fbb.jpg', null, 'sw_fbb.jpg', '0', '0', '1000', '2017-10-23 04:17:11', '2017-10-23 04:24:16');
INSERT INTO `game_lists` VALUES ('902', '26', '猴子先生', null, '1', '1', '3', 'sw_mrmnky', null, 'sw_mrmnky.jpg', null, 'sw_mrmnky.jpg', '0', '0', '1000', '2017-10-23 04:17:32', '2017-10-23 04:24:16');
INSERT INTO `game_lists` VALUES ('903', '26', '新年财富', null, '1', '1', '3', 'sw_nyf', null, 'sw_nyf.jpg', null, 'sw_nyf.jpg', '0', '0', '1000', '2017-10-23 04:17:53', '2017-10-23 04:24:17');
INSERT INTO `game_lists` VALUES ('904', '26', '熊猫厨神', null, '1', '1', '3', 'sw_pc', null, 'sw_pc.jpg', null, 'sw_pc.jpg', '0', '0', '1000', '2017-10-23 04:18:15', '2017-10-23 04:24:17');
INSERT INTO `game_lists` VALUES ('905', '26', '冰火女王', null, '1', '1', '3', 'sw_qoiaf', null, 'sw_qoiaf.jpg', null, 'sw_qoiaf.jpg', '0', '0', '1000', '2017-10-23 04:18:33', '2017-10-23 04:24:18');
INSERT INTO `game_lists` VALUES ('906', '26', '拉美斯西财富', null, '1', '1', '3', 'sw_rf', null, 'sw_rf.jpg', null, 'sw_rf.jpg', '0', '0', '1000', '2017-10-23 04:18:52', '2017-10-23 04:24:25');
INSERT INTO `game_lists` VALUES ('907', '26', '狂热重转', null, '1', '1', '3', 'sw_rm', null, 'sw_rm.jpg', null, 'sw_rm.jpg', '0', '0', '1000', '2017-10-23 04:19:19', '2017-10-23 04:24:26');
INSERT INTO `game_lists` VALUES ('908', '26', '崛起的武士', null, '1', '1', '3', 'sw_rs', null, 'sw_rs.jpg', null, 'sw_rs.jpg', '0', '0', '1000', '2017-10-23 04:19:42', '2017-10-23 04:24:27');
INSERT INTO `game_lists` VALUES ('909', '26', '三福', null, '1', '1', '3', 'sw_sf', null, 'sw_sf.jpg', null, 'sw_sf.jpg', '0', '0', '1000', '2017-10-23 04:20:15', '2017-10-23 04:24:27');
INSERT INTO `game_lists` VALUES ('910', '26', '神龙宝石', null, '1', '1', '3', 'sw_slbs', null, 'sw_slbs.jpg', null, 'sw_slbs.jpg', '0', '0', '1000', '2017-10-23 04:20:41', '2017-10-23 04:24:27');
INSERT INTO `game_lists` VALUES ('911', '26', '生财有道', null, '1', '1', '3', 'sw_scyd', null, 'sw_scyd.jpg', null, 'sw_scyd.jpg', '0', '0', '1000', '2017-10-23 04:21:04', '2017-10-23 04:24:28');
INSERT INTO `game_lists` VALUES ('912', '26', '水果财富', null, '1', '1', '3', 'sw_sgcf', null, 'sw_sgcf.jpg', null, 'sw_sgcf.jpg', '0', '0', '1000', '2017-10-23 04:21:27', '2017-10-23 04:24:29');
INSERT INTO `game_lists` VALUES ('913', '26', '冰雪女王', null, '1', '1', '3', 'sw_sq', null, 'sw_sq.jpg', null, 'sw_sq.jpg', '0', '0', '1000', '2017-10-23 04:21:50', '2017-10-23 04:24:30');
INSERT INTO `game_lists` VALUES ('914', '26', '钻石交响曲', null, '1', '1', '3', 'sw_sod', null, 'sw_sod.jpg', null, 'sw_sod.jpg', '0', '0', '1000', '2017-10-23 04:22:08', '2017-10-23 04:24:30');
INSERT INTO `game_lists` VALUES ('915', '26', '虎虎生财', null, '1', '1', '3', 'sw_tc', null, 'sw_tc.jpg', null, 'sw_tc.jpg', '0', '0', '1000', '2017-10-23 04:22:30', '2017-10-23 04:24:31');
INSERT INTO `game_lists` VALUES ('916', '26', '迎财神', null, '1', '1', '3', 'sw_ycs', null, 'sw_ycs.jpg', null, 'sw_ycs.jpg', '0', '0', '1000', '2017-10-23 04:22:49', '2017-10-23 04:24:31');
INSERT INTO `game_lists` VALUES ('917', '26', '两心知', null, '1', '1', '1', 'sw_h2h', null, 'sw_h2h.jpg', null, 'sw_h2h.jpg', '0', '0', '1000', '2017-10-23 04:23:05', '2017-10-23 04:24:32');
INSERT INTO `game_lists` VALUES ('918', '26', '天火凤凰', null, '1', '1', '3', 'sw_hp', null, 'sw_hp.jpg', null, 'sw_hp.jpg', '0', '0', '1000', '2017-10-23 04:23:23', '2017-10-23 04:24:32');
INSERT INTO `game_lists` VALUES ('919', '26', '黄金花园', null, '1', '1', '3', 'sw_ggdn', null, 'sw_ggdn.jpg', null, 'sw_ggdn.jpg', '0', '0', '1000', '2017-10-23 04:23:40', '2017-10-23 04:24:32');
INSERT INTO `game_lists` VALUES ('920', '26', '龙的传奇', null, '1', '1', '3', 'sw_ld', null, 'sw_ld.jpg', null, 'sw_ld.jpg', '0', '0', '1000', '2017-10-23 04:23:59', '2017-10-23 04:24:33');
INSERT INTO `game_lists` VALUES ('921', '22', '千炮捕鱼', null, '1', '1', '3', '50', null, '50.jpg', null, '', '0', '0', '1000', '2017-10-23 06:07:29', '2017-10-23 06:12:55');
INSERT INTO `game_lists` VALUES ('922', '22', '森林舞会', null, '1', '1', '3', '40', null, '40.jpg', null, '', '0', '0', '1000', '2017-10-23 06:07:51', '2017-10-23 06:12:54');
INSERT INTO `game_lists` VALUES ('923', '22', '水浒传', null, '1', '1', '3', '110', null, '110.jpg', null, '', '0', '0', '1000', '2017-10-23 06:08:08', '2017-10-23 06:12:54');
INSERT INTO `game_lists` VALUES ('924', '22', '百家乐', null, '1', '1', '3', '141', null, '141.jpg', null, '', '0', '0', '1000', '2017-10-23 06:08:29', '2017-10-23 06:12:49');
INSERT INTO `game_lists` VALUES ('925', '22', '太极', null, '1', '1', '3', '140', null, '140.jpg', null, '', '0', '0', '1000', '2017-10-23 06:08:57', '2017-10-23 06:12:49');
INSERT INTO `game_lists` VALUES ('926', '22', '舞狮报喜', null, '1', '1', '3', '144', null, '144.jpg', null, '', '0', '0', '1000', '2017-10-23 06:09:13', '2017-10-23 06:12:48');
INSERT INTO `game_lists` VALUES ('927', '22', '五龙争霸', null, '1', '1', '3', '111', null, '111.jpg', null, '', '0', '0', '1000', '2017-10-23 06:09:26', '2017-10-23 06:12:48');
INSERT INTO `game_lists` VALUES ('928', '22', '天龙虎地', null, '1', '1', '3', '114', null, '114.jpg', null, '', '0', '0', '1000', '2017-10-23 06:09:45', '2017-10-23 06:12:47');
INSERT INTO `game_lists` VALUES ('929', '22', '百乐牛牛', null, '1', '1', '3', '139', null, '139.jpg', null, '', '0', '0', '1000', '2017-10-23 06:10:00', '2017-10-23 06:12:47');
INSERT INTO `game_lists` VALUES ('930', '22', '水浒英雄', null, '1', '1', '3', '112', null, '112.jpg', null, '', '0', '0', '1000', '2017-10-23 06:10:15', '2017-10-23 06:12:46');
INSERT INTO `game_lists` VALUES ('931', '22', '皇家轮盘', null, '1', '1', '3', '138', null, '138.jpg', null, '', '0', '0', '1000', '2017-10-23 06:10:32', '2017-10-23 06:12:45');
INSERT INTO `game_lists` VALUES ('932', '22', '洪福齐天', null, '1', '1', '3', '120', null, '120.jpg', null, '', '0', '0', '1000', '2017-10-23 06:10:45', '2017-10-23 06:12:45');
INSERT INTO `game_lists` VALUES ('933', '22', '好运5扑克', null, '1', '1', '3', '22', null, '22.jpg', null, '', '0', '0', '1000', '2017-10-23 06:11:00', '2017-10-23 06:12:44');
INSERT INTO `game_lists` VALUES ('934', '22', '黄金777', null, '1', '1', '3', '115', null, '115.jpg', null, '', '0', '0', '1000', '2017-10-23 06:11:16', '2017-10-23 06:12:44');
INSERT INTO `game_lists` VALUES ('936', '22', '经典水果机', null, '1', '1', '3', '21', null, '21.jpg', null, '', '0', '0', '1000', '2017-10-23 06:11:54', '2017-12-17 13:32:43');
INSERT INTO `game_lists` VALUES ('937', '22', 'HUGA', null, '1', '1', '3', '125', null, '125.jpg', null, '', '0', '0', '1000', '2017-10-23 06:12:13', '2017-10-23 06:12:42');
INSERT INTO `game_lists` VALUES ('938', '22', '明星97', null, '1', '1', '3', '117', null, '117.jpg', null, '', '0', '0', '1000', '2017-10-23 06:12:29', '2017-10-23 06:12:42');
INSERT INTO `game_lists` VALUES ('948', '23', '海盗旗帜', null, '1', '1', '3', null, 'jollyroger', '海盗旗帜.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('941', '23', '幸运钻石', null, '1', '1', '3', null, 'luckydiamonds', '幸运钻石.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('942', '23', '迷你百家乐', null, '1', '1', '3', null, 'minibaccarat', '迷你百家乐.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('943', '23', '欧洲轮盘', null, '1', '1', '3', null, 'europeanroulette', '欧洲轮盘.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('944', '23', '英式轮盘', null, '1', '1', '3', null, 'englishroulette', '英式轮盘.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('945', '23', '法式轮盘', null, '1', '1', '3', null, 'frenchroulette', '法式轮盘.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('946', '23', '比大小', null, '1', '1', '3', null, 'beatme', '比大小.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('947', '23', '赌场桩牌扑克', null, '1', '1', '3', null, 'casinostudpoker', '赌场桩牌扑克.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('949', '23', '水果财富', null, '1', '1', '3', null, 'fruitbonanza', '水果财富.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('950', '23', '多手21点', null, '1', '1', '3', null, 'blackjackmh', '多手21点.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('951', '23', '欧洲21点', null, '1', '1', '3', null, 'europeanblackjackmh', '欧洲21点.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('952', '23', '双开式21点', null, '1', '1', '3', null, 'doubleexposureblackjackmh', '双开式21点.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('953', '23', '单副牌21点', null, '1', '1', '3', null, 'singledeckblackjackmh', '单副牌21点.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('954', '23', '赌场Hold’em', null, '1', '1', '3', null, 'casinoholdem', '赌场Hold’em.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('955', '23', '牌九扑克', null, '1', '1', '3', null, 'paigowpoker', '牌九扑克.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('956', '23', '新法式轮盘', null, '1', '1', '3', null, 'frenchroulettelp', '新法式轮盘.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('957', '23', '刮刮乐', null, '1', '1', '3', null, 'scratchahoy', '刮刮乐.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('958', '23', '一杆进洞', null, '1', '1', '3', null, 'holeinone', '一杆进洞.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('959', '23', '黑桃A', null, '1', '1', '3', null, 'aceofspades', '黑桃A.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('960', '23', '财富铃铛', null, '1', '1', '3', null, 'belloffortune', '财富铃铛.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('961', '23', '极速现金', null, '1', '1', '3', null, 'speedcash', '极速现金.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('962', '23', '礼品店', null, '1', '1', '3', null, 'giftshop', '礼品店.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('963', '23', '黄金球门', null, '1', '1', '3', null, 'goldengoal', '黄金球门.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('964', '23', '猫与钞票', null, '1', '1', '3', null, 'catsandcash', '猫与钞票.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('965', '23', '警匪游戏', null, '1', '1', '3', null, 'copsnrobbers', '警匪游戏.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('966', '23', '占卜师', null, '1', '1', '3', null, 'fortuneteller', '占卜师.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('967', '23', '非洲掠影', null, '1', '1', '3', null, 'photosafari', '非洲掠影.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('968', '23', '太空竞赛', null, '1', '1', '3', null, 'spacerace', '太空竞赛.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('969', '23', '5x魔术', null, '1', '1', '3', null, '5xmagic', '5x魔术.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('970', '23', '爱尔兰黄金', null, '1', '1', '3', null, 'irishgold', '爱尔兰黄金.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('971', '23', '魔法草原', null, '1', '1', '3', null, 'enchantedmeadow', '魔法草原.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('972', '23', '三輪大小', null, '1', '1', '3', null, 'triplechancehilo', '三輪大小.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('973', '23', '太阳神的财富', null, '1', '1', '3', null, 'richesofra', '太阳神的财富.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('974', '23', '龙船', null, '1', '1', '3', null, 'dragonship', '龙船.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('975', '23', '珍珠湖', null, '1', '1', '3', null, 'pearllagoon', '珍珠湖.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('976', '23', '珠宝盒', null, '1', '1', '3', null, 'jewelbox', '珠宝盒.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('977', '23', '阿兹特克人像', null, '1', '1', '3', null, 'aztecidols', '阿兹特克人像.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('978', '23', '枪手', null, '1', '1', '3', null, 'gunslinger', '枪手.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('979', '23', '神话', null, '1', '1', '3', null, 'myth', '神话.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('980', '23', '黄金奖杯2', null, '1', '1', '3', null, 'goldtrophy2', '黄金奖杯2.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('981', '23', '狂野血液', null, '1', '1', '3', null, 'wildblood', '狂野血液.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('982', '23', '矮精灵埃及之旅', null, '1', '1', '3', null, 'leprechaungoesegypt', '矮精灵埃及之旅.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('983', '23', '忍者水果', null, '1', '1', '3', null, 'ninjafruits', '忍者水果.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('984', '23', '巨魔猎人', null, '1', '1', '3', null, 'trollhunters', '巨魔猎人.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('985', '23', '财富崛起', null, '1', '1', '3', null, 'ragetoriches', '财富崛起.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('986', '23', '魔法水晶', null, '1', '1', '3', null, 'enchantedcrystals', '魔法水晶.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('987', '23', '能量小精灵', null, '1', '1', '3', null, 'energoonz', '能量小精灵.png', null, null, '0', '0', '100', null, '2017-09-16 19:17:49');
INSERT INTO `game_lists` VALUES ('988', '23', '杰克高手', null, '1', '1', '3', null, 'jacksorbetter', '杰克高手.png', null, null, '0', '0', '100', null, '2017-10-05 05:04:56');
INSERT INTO `game_lists` VALUES ('989', '23', '德塞斯野生', null, '1', '1', '3', null, 'deuceswild', '德塞斯野生.png', null, null, '0', '0', '100', null, '2017-10-23 06:37:29');
INSERT INTO `game_lists` VALUES ('990', '23', '小丑扑克', null, '1', '1', '3', null, 'jokerpoker', '小丑扑克.png', null, null, '0', '0', '100', null, '2017-10-06 18:14:00');
INSERT INTO `game_lists` VALUES ('991', '23', '国王高手', null, '1', '1', '3', null, 'kingsorbetter', '国王高手.png', null, null, '0', '0', '100', null, '2017-10-09 22:50:29');
INSERT INTO `game_lists` VALUES ('992', '23', '牌十高手', null, '1', '1', '3', null, 'tensorbetter', '牌十高手.png', null, null, '0', '0', '100', null, '2017-10-23 03:36:30');
INSERT INTO `game_lists` VALUES ('993', '23', '双倍奖金', null, '1', '1', '3', null, 'doublebonus', '双倍奖金.png', null, null, '0', '0', '100', null, '2017-10-23 02:47:21');
INSERT INTO `game_lists` VALUES ('994', '23', '德塞斯小丑', null, '1', '1', '3', null, 'deucesandjoker', '德塞斯小丑.png', null, null, '0', '0', '100', null, '2017-10-23 04:11:16');
INSERT INTO `game_lists` VALUES ('995', '23', '累积奖金扑克', null, '1', '1', '3', null, 'jackpotpoker', '累积奖金扑克.png', null, null, '0', '0', '100', null, '2017-10-23 04:24:22');
INSERT INTO `game_lists` VALUES ('996', '23', '中国新年', null, '1', '1', '3', null, 'chinesenewyear', '中国新年.png', null, null, '0', '0', '100', null, '2017-10-23 04:24:22');
INSERT INTO `game_lists` VALUES ('997', '23', '财富淑女', null, '1', '1', '3', null, 'ladyoffortune', '财富淑女.png', null, null, '0', '0', '100', null, '2017-10-23 04:24:09');
INSERT INTO `game_lists` VALUES ('998', '23', '印度珍珠', null, '1', '1', '3', null, 'pearlsofindia', '印度珍珠.png', null, null, '0', '0', '100', null, '2017-10-23 04:24:11');
INSERT INTO `game_lists` VALUES ('999', '23', '神秘小丑', null, '1', '1', '3', null, 'mysteryjoker', '神秘小丑.png', null, null, '0', '0', '100', null, '2017-10-23 04:24:11');
INSERT INTO `game_lists` VALUES ('1000', '23', '疯狂奶牛', null, '1', '1', '3', null, 'crazycows', '疯狂奶牛.png', null, null, '0', '0', '100', null, '2017-10-23 04:24:13');
INSERT INTO `game_lists` VALUES ('1001', '23', '黄金入场券', null, '1', '1', '3', null, 'goldenticket', '黄金入场券.png', null, null, '0', '0', '100', null, '2017-10-23 04:24:13');
INSERT INTO `game_lists` VALUES ('1002', '23', '五彩宝石', null, '1', '1', '3', null, 'gemix', '五彩宝石.png', null, null, '0', '0', '100', null, '2017-10-23 04:24:13');
INSERT INTO `game_lists` VALUES ('1003', '23', '高塔任务', null, '1', '1', '3', null, 'towerquest', '高塔任务.png', null, null, '0', '0', '100', null, '2017-10-23 04:24:14');
INSERT INTO `game_lists` VALUES ('1004', '23', '欢乐圣诞节', null, '1', '1', '3', '', 'merryxmas', '欢乐圣诞节.png', null, null, '0', '0', '100', null, '2017-10-23 04:24:14');
INSERT INTO `game_lists` VALUES ('1005', '23', '黄金传奇', null, '1', '1', '3', null, 'goldenlegend', '黄金传奇.png', null, null, '0', '0', '100', null, '2017-10-23 04:24:15');
INSERT INTO `game_lists` VALUES ('1006', '23', '酷炫一族', '', '1', '1', '3', '', 'pimped', '酷炫一族.png', '', '', '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1007', '23', '旋转派对', '', '1', '1', '3', '', 'spinparty', '旋转派对.png', '', '', '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1008', '23', '复活节彩蛋', '', '1', '1', '3', '', 'eastereggs', '复活节彩蛋.png', '', '', '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1009', '23', '北部荒野', '', '1', '1', '3', '', 'wildnorth', '北部荒野.png', '', '', '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1010', '23', '超级翻转', '', '1', '1', '3', '', 'superflip', '超级翻转.png', '', '', '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1011', '23', '克拉肯的眼睛', '', '1', '1', '3', '', 'eyeofthekraken', '克拉肯的眼睛.png', '', '', '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1012', '23', '皇室化妆舞会', '', '1', '1', '3', '', 'masquerade', '皇室化妆舞会.png', '', '', '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1013', '23', '天际战神', '', '1', '1', '3', '', 'cloudquest', '天际战神.png', '', '', '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1014', '23', '宝石巫师', '', '1', '1', '3', '', 'wizardofgems', '宝石巫师.png', '', '', '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1015', '23', '冷酷亡灵', '', '1', '1', '3', '', 'grimmuerto', '冷酷亡灵.png', '', '', '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1016', '23', '桑巴嘉年华', '', '1', '1', '3', '', 'sambacarnival', '桑巴嘉年华.png', '', '', '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1017', '23', '火焰小丑', '', '1', '1', '3', '', 'firejoker', '火焰小丑.png', '', '', '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1018', '23', '圣诞小丑', '', '1', '1', '3', '', 'christmasjoker', '圣诞小丑.png', '', '', '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1019', '23', '亡灵书', '', '1', '1', '3', '', 'bookofdead', '亡灵书.png', '', '', '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1020', '23', '黄金之帆', '', '1', '1', '3', '', 'sailsofgold', '黄金之帆.png', '', '', '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1021', '23', '金色商队', '', '1', '1', '3', '', 'goldencaravan', '金色商队.png', '', '', '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1022', '23', '恶魔的诱惑', '', '1', '1', '3', '', 'leprechaungoestohell', '恶魔的诱惑.png', '', '', '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1023', '23', '飞翔小猪', '', '1', '1', '3', '', 'flyingpigs', '飞翔小猪.png', '', '', '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1024', '23', '虫虫派对', '', '1', '1', '3', '', 'bugsparty', '虫虫派对.png', '', '', '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1025', '23', '财运之轮', '', '1', '1', '3', '', 'moneywheel', '财运之轮.png', '', '', '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1026', '23', '维京人世界', '', '1', '1', '3', '', 'vikingrunecraft', '维京人世界.png', '', '', '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1027', '23', '夏日庆典', '', '1', '1', '3', '', 'matsuri', '夏日庆典.png', '', '', '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1028', '23', '七宗罪', '', '1', '1', '3', '', 'sevensins', '七宗罪.png', '', '', '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1029', '23', '超级旋转', '', '1', '1', '3', '', 'superwheel', '超级旋转.png', '', '', '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1030', '23', '淘气 小公主', '', '1', '1', '3', '', 'prissyprincess', '淘气 小公主.png', '', '', '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1031', '23', '翡翠魔术师', '', '1', '1', '3', '', 'jademagician', '翡翠魔术师.png', '', '', '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1032', '23', '节庆假日', '', '1', '1', '3', '', 'holidayseason', '节庆假日.png', '', '', '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1033', '23', '水果 多多 81', '', '1', '1', '3', '', 'multifruit81', '水果 多多 81.png', '', '', '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1034', '23', '阿斯德克战士公主', '', '1', '1', '3', '', 'aztecwarriorprincess', '阿斯德克战士公主.png', '', '', '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1035', '23', '月亮守护者', '', '1', '1', '3', '', 'moonprincess', '月亮守护者.png', '', '', '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1036', '23', '甜蜜 27', '', '1', '1', '3', '', 'sweet27', '甜蜜 27.png', '', '', '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1037', '25', '阿珂魔塔', null, '1', '1', '3', '553', null, 'bb553.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1038', '25', '阿肯色廿一', null, '1', '1', '3', '81', null, 'bb81.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1039', '25', '安其拉', null, '1', '1', '3', '747', null, 'bb747.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1040', '25', '百家乐', null, '1', '1', '3', '13', null, 'bb13.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1041', '25', '宝光魅影', null, '1', '1', '3', '52', null, 'bb52.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1042', '25', '不速之客', null, '1', '1', '3', '155', null, 'bb155.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1043', '25', '藏宝阁', null, '1', '1', '3', '158', null, 'bb158.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1044', '25', '曾几何时', null, '1', '1', '3', '193', null, 'bb193.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1045', '25', '禅机宝果', null, '1', '1', '3', '512', null, 'bb512.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1046', '25', '朝阳曙光', null, '1', '1', '3', '548', null, 'bb548.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1047', '25', '传统凯诺', null, '1', '1', '3', '8', null, 'bb8.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1048', '25', '传统轮盘', null, '1', '1', '3', '209', null, 'bb209.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1049', '25', '传统扑克', null, '1', '1', '3', '23', null, 'bb23.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1050', '25', '床底魅影', null, '1', '1', '3', '308', null, 'bb308.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1051', '25', '达者为先', null, '1', '1', '3', '226', null, 'bb226.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1052', '25', '大斑巨鳄', null, '1', '1', '3', '231', null, 'bb231.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1053', '25', '大预言者', null, '1', '1', '3', '91', null, 'bb91.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1054', '25', '弹弹糖', null, '1', '1', '3', '402', null, 'bb402.png', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1055', '25', '电影风云', null, '1', '1', '3', '266', null, 'bb266.png', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1056', '25', '谍海特工', null, '1', '1', '3', '180', null, 'bb180.png', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1057', '25', '枓学怪人', null, '1', '1', '3', '159', null, 'bb159.png', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1058', '25', '赌场大亨', null, '1', '1', '3', '210', null, 'bb210.png', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1059', '25', '赌场杰克大亨', null, '1', '1', '3', '63', null, 'bb63.png', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1060', '25', '赌城周末热潮', null, '1', '1', '3', '590', null, 'bb590.png', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1061', '25', '多元凯诺', null, '1', '1', '3', '107', null, 'bb107.png', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1062', '25', '法老大帝', null, '1', '1', '3', '134', null, 'bb134.png', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1063', '25', '蕃国宝藏', null, '1', '1', '3', '190', null, 'bb190.png', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1064', '25', '反手云覆手雨', null, '1', '1', '3', '249', null, 'bb249.png', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1065', '25', '疯狂局末', null, '1', '1', '3', '29', null, 'bb29.png', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1066', '25', '疯狂凯诺', null, '1', '1', '3', '15', null, 'bb15.png', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1067', '25', '疯狂寿司', null, '1', '1', '3', '288', null, 'bb288.png', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1068', '25', '蜂回露宝', null, '1', '1', '1', '122', null, 'bb122.png', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1069', '25', '高成低定', null, '1', '1', '1', '74', null, 'bb74.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1070', '25', '恭喜发财 or鸿运当头 or 才高八斗', null, '1', '1', '1', '700', null, 'bb700.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1071', '25', '古宝寻奇', null, '1', '1', '1', '104', null, 'bb104.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1072', '25', '鬼牌百搭', null, '1', '1', '1', '30', null, 'bb30.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1073', '25', '鬼牌精灵', null, '1', '1', '1', '147', null, 'bb147.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1074', '25', '海滨假日', null, '1', '1', '1', '597', null, 'bb597.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1075', '25', '海盗廿一', null, '1', '1', '1', '82', null, 'bb82.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1076', '25', '海底沉宝', null, '1', '1', '1', '259', null, 'bb259.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1077', '25', '黑金帝国', null, '1', '1', '1', '256', null, 'bb256.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1078', '25', '黑七风暴', null, '1', '1', '1', '44', null, 'bb44.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1079', '25', '红利万岁', null, '1', '1', '1', '137', null, 'bb137.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1080', '25', '洪荒时代', null, '1', '1', '1', '224', null, 'bb224.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1081', '25', '猴王称霸', null, '1', '1', '1', '33', null, 'bb33.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1082', '25', '虎机老爷', null, '1', '1', '1', '179', null, 'bb179.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1083', '25', '华丽人生', null, '1', '1', '1', '173', null, 'bb173.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1084', '25', '欢乐满堂', null, '1', '1', '1', '221', null, 'bb221.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1085', '25', '皇家卷轴', null, '1', '1', '1', '223', null, 'bb223.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1086', '25', '皇室扑克', null, '1', '1', '1', '142', null, 'bb142.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1087', '25', '毁灭者', null, '1', '1', '1', '262', null, 'bb262.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1088', '25', '火红火热', null, '1', '1', '1', '94', null, 'bb94.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1089', '25', '火热廿一', null, '1', '1', '1', '64', null, 'bb64.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1090', '25', '火树银花', null, '1', '1', '1', '323', null, 'bb323.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1091', '25', '吉普赛玫瑰', null, '1', '1', '1', '478', null, 'bb478.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1093', '25', '加勒比海扑克', null, '1', '1', '1', '12', null, 'bb12.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1094', '25', '椒风蒜雨', null, '1', '1', '1', '225', null, 'bb225.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1095', '25', '角斗勇士', null, '1', '1', '1', '178', null, 'bb178.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1096', '25', '杰基尔博士和海德先生', null, '1', '1', '1', '500', null, 'bb500.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1097', '25', '金星异客', null, '1', '1', '1', '274', null, 'bb274.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1098', '25', '金弋独专', null, '1', '1', '1', '444', null, 'bb444.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1099', '25', '金弋无敌', null, '1', '1', '1', '222', '', 'bb222.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1100', '25', '精灵城堡', null, '1', '1', '1', '136', '', 'bb136.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1101', '25', '局末红利', null, '1', '1', '1', '145', '', 'bb145.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1102', '25', '巨奖旋风', null, '1', '1', '1', '41', '', 'bb41.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1103', '25', '巨钻巨奖', null, '1', '1', '1', '32', '', 'bb32.png', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1104', '25', '聚焦轮盘', null, '1', '1', '1', '278', '', 'bb278.png', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1105', '25', '科帕酒店', null, '1', '1', '1', '300', '', 'bb300.png', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1106', '25', '浪漫巴黎', null, '1', '1', '1', '234', '', 'bb234.png', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1107', '25', '两小无猜', null, '1', '1', '1', '309', '', 'bb309.png', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1108', '25', '烈焰钢铁', null, '1', '1', '1', '755', '', 'bb755.png', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1109', '25', '龙争虎鬥', null, '1', '1', '1', '9', '', 'bb9.png', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1110', '25', '落日大亨', null, '1', '1', '1', '295', '', 'bb295.png', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1137', '25', '拾始拾终', null, '1', '1', '3', '146', null, 'bb146.png', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1112', '25', '绿洲扑克', null, '1', '1', '1', '203', '', 'bb203.png', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1114', '25', '马亚传奇', null, '1', '1', '1', '277', '', 'bb277.png', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1115', '25', '吗吗咪呀', null, '1', '1', '1', '238', '', 'bb238.png', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1116', '25', '美式轮盘风云', null, '1', '1', '1', '5', '', 'bb5.png', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1117', '25', '魅影重重', null, '1', '1', '1', '156', '', 'bb156.png', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1118', '25', '梦钻成真', null, '1', '1', '1', '21', '', 'bb21.png', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1119', '25', '魔化入迷', null, '1', '1', '1', '207', '', 'bb207.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1120', '25', '魔幻真境', null, '1', '1', '1', '236', '', 'bb236.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1121', '25', '魔力棒', null, '1', '1', '1', '751', '', 'bb751.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1122', '25', '木偶记', null, '1', '1', '1', '520', '', 'bb520.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1123', '25', '欧式轮盘风云', null, '1', '1', '1', '79', '', 'bb79.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1124', '25', '欧式廿一', null, '1', '1', '1', '191', '', 'bb191.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1125', '25', '派钱船长', null, '1', '1', '1', '18', '', 'bb18.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1126', '25', '扑朔迷离', null, '1', '1', '1', '375', '', 'bb375.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1127', '25', '七海维京', null, '1', '1', '1', '228', '', 'bb228.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1128', '25', '七海纵横', null, '1', '1', '1', '194', '', 'bb194.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1129', '25', '七重极乐天', null, '1', '1', '1', '229', '', 'bb229.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1130', '25', '人傑地灵', null, '1', '1', '1', '31', '', 'bb31.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1131', '25', '人鱼明珠', null, '1', '1', '1', '20', '', 'bb20.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1132', '25', '叁家分晋', null, '1', '1', '1', '43', '', 'bb43.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1133', '25', '叁卡风云', null, '1', '1', '1', '244', '', 'bb244.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1134', '25', '叁元及第', null, '1', '1', '1', '3', '', 'bb3.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1136', '25', '圣诞颂歌', null, '1', '1', '3', '619', null, 'bb619.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1138', '25', '史加彻斯', null, '1', '1', '3', '143', null, 'bb143.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1141', '25', '双鬼拍门', null, '1', '1', '3', '144', null, 'bb144.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1140', '25', '双倍红利', null, '1', '1', '3', '139', null, 'bb139.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1135', '25', '奢丽人生', null, '1', '1', '3', '554', null, 'bb554.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1139', '25', '淑女魔姬', null, '1', '1', '3', '426', null, 'bb426.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1142', '25', '松朋鼠党', null, '1', '1', '3', '243', null, 'bb243.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1143', '25', '素学怪人', null, '1', '1', '3', '247', null, 'bb247.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1144', '25', '贪婪妖精', null, '1', '1', '3', '341', null, 'bb341.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1145', '25', '天使传奇', null, '1', '1', '3', '258', null, 'bb258.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1146', '25', '无法无天', null, '1', '1', '3', '133', null, 'bb133.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1147', '25', '虚幻赛马', null, '1', '1', '3', '199', null, 'bb199.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1148', '25', '寻宝人生', null, '1', '1', '3', '80', null, 'bb80.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1149', '25', '寻芝问宝', null, '1', '1', '3', '88', null, 'bb88.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1150', '25', '摇滚巨星', null, '1', '1', '3', '220', null, 'bb220.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1151', '25', '遗失一族', null, '1', '1', '3', '248', null, 'bb248.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1152', '25', '异域风云', null, '1', '1', '3', '47', null, 'bb47.png', null, null, '0', '0', '100', '2017-09-16 19:17:47', '2017-09-16 19:17:47');
INSERT INTO `game_lists` VALUES ('1153', '25', '勇幹神探', null, '1', '1', '3', '384', null, 'bb384.png', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1154', '25', '游猎乐园', null, '1', '1', '3', '280', null, 'bb280.png', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1155', '25', '掷骰抢宝', null, '1', '1', '3', '10', null, 'bb10.png', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1156', '25', '重磅美式轮盘', null, '1', '1', '3', '198', null, 'bb198.png', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1157', '25', '重磅欧式轮盘', null, '1', '1', '3', '197', null, 'bb197.png', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1158', '25', '钻光灿影', null, '1', '1', '3', '534', null, 'bb534.png', null, null, '0', '0', '100', '2017-09-16 19:17:46', '2017-09-16 19:17:46');
INSERT INTO `game_lists` VALUES ('1228', '6', '古怪猴子', null, '1', '1', '3', 'fm', null, null, null, null, '0', '0', '1000', '2018-01-07 04:54:16', '2018-01-07 04:54:29');

-- ----------------------------
-- Table structure for game_record
-- ----------------------------
DROP TABLE IF EXISTS `game_record`;
CREATE TABLE `game_record` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rowid` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `billNo` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '注单流水号',
  `api_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '接口平台 如 AG MG',
  `playerName` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '玩家各平台账号',
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '玩家账号',
  `member_id` int(11) NOT NULL COMMENT '用户 ID',
  `agentCode` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '代理商编号',
  `gameCode` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '游戏局号',
  `netAmount` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '玩家输赢额度',
  `betTime` timestamp NULL DEFAULT NULL COMMENT '投注时间',
  `gameType` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '游戏类型',
  `betAmount` decimal(16,2) DEFAULT '0.00' COMMENT '投注金额',
  `validBetAmount` decimal(16,2) DEFAULT '0.00' COMMENT '有效投注额度',
  `flag` int(11) DEFAULT '0' COMMENT '结算状态',
  `playType` int(11) DEFAULT '0' COMMENT '游戏玩法',
  `currency` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '货币类型',
  `tableCode` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '桌子编号',
  `loginIP` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '玩家IP',
  `recalcuTime` timestamp NULL DEFAULT NULL COMMENT '注单重新派彩时间',
  `platformId` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '平台编号',
  `platformType` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '平台类型',
  `stringex` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '产品附注(通常为空)',
  `remark` text COLLATE utf8mb4_unicode_ci COMMENT '返回信息',
  `round` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `copyFlag` int(11) DEFAULT '0' COMMENT '更新标志',
  `filePath` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '文件路径',
  `prefix` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '站点前缀',
  `result` text COLLATE utf8mb4_unicode_ci COMMENT '返回信息',
  `reAmount` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '备用',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `game_record_billno_index` (`billNo`),
  KEY `game_record_api_type_index` (`api_type`),
  KEY `game_record_playername_index` (`playerName`),
  KEY `game_record_bettime_index` (`betTime`),
  KEY `game_record_gametype_index` (`gameType`),
  KEY `copyFlag` (`copyFlag`),
  KEY `prefix` (`prefix`)
) ENGINE=MyISAM AUTO_INCREMENT=2714 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of game_record
-- ----------------------------

-- ----------------------------
-- Table structure for members
-- ----------------------------
DROP TABLE IF EXISTS `members`;
CREATE TABLE `members` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `real_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qq` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `original_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '原始密码',
  `gender` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0男1女',
  `is_daili` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1为代理',
  `top_id` int(11) NOT NULL DEFAULT '0' COMMENT '上级 id',
  `invite_code` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '邀请注册码',
  `qk_pwd` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '取款密码',
  `money` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '中心账户余额',
  `fs_money` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '反水账户余额',
  `total_amount` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '平台总投注额',
  `score` int(11) NOT NULL DEFAULT '0' COMMENT '积分',
  `register_ip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '注册IP',
  `last_login_ip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '最后登录ip',
  `last_login_at` timestamp NULL DEFAULT NULL COMMENT '最后登录时间',
  `bank_username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '开户人名字',
  `bank_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '银行名称',
  `bank_branch_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '开户行网点',
  `bank_card` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '银行卡号',
  `bank_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '开户地址',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态',
  `is_login` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 未登录 1已登录',
  `o_password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '登录密码',
  `agent_uri` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '代理链接',
  `agent_uri_pre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '代理链接前缀',
  `last_session` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `members_name_unique` (`name`),
  UNIQUE KEY `members_invite_code_unique` (`invite_code`)
) ENGINE=MyISAM AUTO_INCREMENT=89 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of members
-- ----------------------------
INSERT INTO `members` VALUES ('87', 'q787936634', '方块', '123456@qq.com', '123456789', '123456', '$2y$10$jC1qMgfURK6vgXDUP/CXguDPq6tM362kwWuciJQ4j5LDE9Q2GY1uK', '1833321548', '0', '0', '0', '8sz3PLL', '123456', '0.00', '0.00', '0.00', '0', '58.22.113.45', null, null, null, null, null, null, null, '0', '0', 'q787936634', null, null, 'GYd4wErmFmUEEKSLsLP0oqgqrDkJTJRDw6aUBW1n', null, '2018-02-17 15:05:35', '2018-02-17 15:05:35', null);
INSERT INTO `members` VALUES ('88', 'zlinf777', '', null, null, null, '$2y$10$Gpa0aZtoWurEYvuFBxrGLuWr0a3tPDoKCVzlAypqVu5NOISi08DsC', '8c4dd08958', '0', '0', '0', '75caIYq', '123456', '0.00', '0.00', '0.00', '0', '117.172.55.206', null, null, null, null, null, null, null, '0', '0', '123456789', null, null, 'cTF0gBhPCUNoE5W1BfGX7Sl0MayWmrefLVqxqM6d', null, '2018-02-19 17:21:32', '2018-02-19 17:21:32', null);

-- ----------------------------
-- Table structure for member_api
-- ----------------------------
DROP TABLE IF EXISTS `member_api`;
CREATE TABLE `member_api` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `api_id` int(11) NOT NULL,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '平台账号',
  `password` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '平台密码',
  `money` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '平台余额',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '描述',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=141 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='会员-api';

-- ----------------------------
-- Records of member_api
-- ----------------------------
INSERT INTO `member_api` VALUES ('1', '1', '4', 'ceshi456', '205d82d2c5', '0.00', null, '2017-09-22 03:41:49', '2017-09-22 03:41:49');
INSERT INTO `member_api` VALUES ('2', '1', '5', 'ceshi456', '205d82d2c5', '0.00', null, '2017-09-22 03:43:21', '2017-09-22 03:43:21');
INSERT INTO `member_api` VALUES ('3', '1', '12', 'ceshi456', '205d82d2c5', '0.00', null, '2017-09-22 03:47:38', '2017-09-22 03:47:38');
INSERT INTO `member_api` VALUES ('4', '1', '8', 'ceshi456', '205d82d2c5', '0.00', null, '2017-09-22 03:47:39', '2017-09-22 03:47:39');
INSERT INTO `member_api` VALUES ('5', '1', '13', 'ceshi456', '205d82d2c5', '0.00', null, '2017-09-22 03:47:41', '2017-09-22 03:47:41');
INSERT INTO `member_api` VALUES ('6', '1', '9', 'ceshi456', '205d82d2c5', '0.00', null, '2017-09-22 03:47:43', '2017-09-22 03:47:43');
INSERT INTO `member_api` VALUES ('7', '1', '10', 'ceshi456', '205d82d2c5', '0.00', null, '2017-09-22 03:47:45', '2017-09-22 03:47:45');
INSERT INTO `member_api` VALUES ('8', '1', '14', 'ceshi456', '205d82d2c5', '0.00', null, '2017-09-22 03:47:45', '2017-09-22 03:47:45');
INSERT INTO `member_api` VALUES ('9', '1', '11', 'ceshi456', '205d82d2c5', '0.00', null, '2017-09-22 03:47:47', '2017-09-22 03:47:47');
INSERT INTO `member_api` VALUES ('10', '1', '3', 'ceshi456', '205d82d2c5', '0.00', null, '2017-09-23 01:35:26', '2017-09-23 01:35:26');
INSERT INTO `member_api` VALUES ('11', '4', '4', 'ceshi123', '23a5747c77', '0.00', null, '2017-10-04 19:48:32', '2017-10-19 02:16:49');
INSERT INTO `member_api` VALUES ('12', '4', '12', 'ceshi123', '23a5747c77', '0.00', null, '2017-10-04 19:53:36', '2017-10-19 02:16:35');
INSERT INTO `member_api` VALUES ('13', '4', '11', 'ceshi123', '23a5747c77', '0.00', null, '2017-10-04 19:53:48', '2017-10-19 02:16:42');
INSERT INTO `member_api` VALUES ('14', '4', '9', 'ceshi123', '23a5747c77', '0.00', null, '2017-10-04 20:03:03', '2017-10-19 02:16:46');
INSERT INTO `member_api` VALUES ('15', '4', '3', 'ceshi123', '23a5747c77', '20.00', null, '2017-10-04 20:11:24', '2017-10-29 17:11:13');
INSERT INTO `member_api` VALUES ('16', '4', '6', 'ceshi123', '23a5747c77', '0.00', null, '2017-10-04 21:09:01', '2017-10-04 21:09:01');
INSERT INTO `member_api` VALUES ('17', '4', '8', 'ceshi123', '23a5747c77', '0.00', null, '2017-10-04 22:01:05', '2017-10-04 22:01:06');
INSERT INTO `member_api` VALUES ('18', '4', '24', 'ceshi123', '23a5747c77', '15.00', null, '2017-10-04 22:01:09', '2017-12-16 09:58:09');
INSERT INTO `member_api` VALUES ('19', '4', '22', 'ceshi123', '23a5747c77', '0.30', null, '2017-10-04 22:01:12', '2017-10-06 05:23:54');
INSERT INTO `member_api` VALUES ('20', '4', '25', 'ceshi123', '23a5747c77', '0.00', null, '2017-10-04 22:01:12', '2017-10-19 19:58:25');
INSERT INTO `member_api` VALUES ('21', '4', '10', 'ceshi123', '23a5747c77', '0.00', null, '2017-10-04 22:01:13', '2017-10-04 22:01:13');
INSERT INTO `member_api` VALUES ('22', '4', '7', 'ceshi123', '23a5747c77', '0.00', null, '2017-10-04 22:01:14', '2017-10-19 02:16:48');
INSERT INTO `member_api` VALUES ('23', '4', '23', 'ceshi123', '23a5747c77', '0.00', null, '2017-10-04 22:01:15', '2017-10-19 02:16:33');
INSERT INTO `member_api` VALUES ('24', '4', '14', 'ceshi123', '23a5747c77', '0.00', null, '2017-10-04 22:01:26', '2017-10-19 02:16:34');
INSERT INTO `member_api` VALUES ('25', '4', '13', 'ceshi123', '23a5747c77', '0.00', null, '2017-10-06 06:18:12', '2017-10-06 06:18:12');
INSERT INTO `member_api` VALUES ('26', '3', '13', 'vip118net', '7d5087b959', '0.00', null, '2017-10-06 06:18:53', '2017-10-27 22:34:17');
INSERT INTO `member_api` VALUES ('27', '3', '23', 'vip118net', '7d5087b959', '3.00', null, '2017-10-06 08:59:56', '2017-10-27 22:34:17');
INSERT INTO `member_api` VALUES ('28', '3', '6', 'vip118net', '7d5087b959', '20.00', null, '2017-10-06 09:08:53', '2017-10-27 22:34:17');
INSERT INTO `member_api` VALUES ('29', '3', '25', 'vip118net', '7d5087b959', '13.68', null, '2017-10-06 09:17:19', '2017-12-21 22:31:37');
INSERT INTO `member_api` VALUES ('30', '3', '24', 'vip118net', '7d5087b959', '39.70', null, '2017-10-06 09:37:42', '2017-10-24 17:20:11');
INSERT INTO `member_api` VALUES ('31', '3', '3', 'vip118net', '7d5087b959', '0.00', null, '2017-10-06 09:37:44', '2017-10-06 09:37:44');
INSERT INTO `member_api` VALUES ('32', '3', '22', 'vip118net', '7d5087b959', '10.00', null, '2017-10-06 09:37:44', '2017-10-28 13:11:39');
INSERT INTO `member_api` VALUES ('33', '3', '8', 'vip118net', '7d5087b959', '30.00', null, '2017-10-06 09:37:45', '2017-10-25 22:02:12');
INSERT INTO `member_api` VALUES ('34', '3', '7', 'vip118net', '7d5087b959', '3.70', null, '2017-10-06 09:37:48', '2017-12-21 20:54:33');
INSERT INTO `member_api` VALUES ('35', '3', '9', 'vip118net', '7d5087b959', '17.00', null, '2017-10-06 09:37:48', '2017-10-27 22:34:11');
INSERT INTO `member_api` VALUES ('36', '3', '5', 'vip118net', '7d5087b959', '0.00', null, '2017-10-06 09:37:49', '2017-10-27 22:34:11');
INSERT INTO `member_api` VALUES ('37', '3', '10', 'vip118net', '7d5087b959', '108.50', null, '2017-10-06 09:37:51', '2017-10-27 22:34:10');
INSERT INTO `member_api` VALUES ('38', '3', '11', 'vip118net', '7d5087b959', '0.00', null, '2017-10-06 09:37:51', '2017-10-21 11:34:07');
INSERT INTO `member_api` VALUES ('39', '3', '14', 'vip118net', '7d5087b959', '10.00', null, '2017-10-06 09:37:51', '2017-10-19 19:53:51');
INSERT INTO `member_api` VALUES ('40', '3', '12', 'vip118net', '7d5087b959', '51.00', null, '2017-10-06 09:39:29', '2017-12-21 20:54:52');
INSERT INTO `member_api` VALUES ('41', '3', '4', 'vip118net', '7d5087b959', '1.00', null, '2017-10-06 09:39:34', '2017-10-28 13:17:36');
INSERT INTO `member_api` VALUES ('42', '4', '5', 'ceshi123', '23a5747c77', '0.00', null, '2017-10-06 09:58:05', '2017-10-19 02:16:48');
INSERT INTO `member_api` VALUES ('43', '6', '23', 'ceshi12345', '1b6db71451', '0.00', null, '2017-10-10 19:07:56', '2017-10-10 19:07:56');
INSERT INTO `member_api` VALUES ('44', '6', '25', 'ceshi12345', '1b6db71451', '0.00', null, '2017-10-10 19:08:11', '2017-10-10 19:08:11');
INSERT INTO `member_api` VALUES ('45', '7', '25', 'ceshi1231', '2596c7b3df', '0.00', null, '2017-10-10 19:12:36', '2017-10-10 19:12:36');
INSERT INTO `member_api` VALUES ('46', '7', '23', 'ceshi1231', '2596c7b3df', '0.00', null, '2017-10-11 03:30:42', '2017-10-11 03:30:42');
INSERT INTO `member_api` VALUES ('47', '8', '25', 'ceshi888', '6bf67db081', '0.00', null, '2017-10-11 03:32:54', '2017-10-11 03:32:54');
INSERT INTO `member_api` VALUES ('48', '8', '23', 'ceshi888', '6bf67db081', '10.00', null, '2017-10-11 03:34:27', '2017-10-11 04:35:01');
INSERT INTO `member_api` VALUES ('49', '7', '14', 'ceshi1231', '2596c7b3df', '0.00', null, '2017-10-11 04:03:29', '2017-10-11 04:03:29');
INSERT INTO `member_api` VALUES ('50', '8', '3', 'ceshi888', '6bf67db081', '0.00', null, '2017-10-11 04:52:42', '2017-10-11 04:52:42');
INSERT INTO `member_api` VALUES ('51', '12', '10', 'dior8988', '1e1eec31dc', '0.00', null, '2017-10-15 20:37:06', '2017-10-15 20:37:06');
INSERT INTO `member_api` VALUES ('52', '12', '4', 'dior8988', '1e1eec31dc', '0.00', null, '2017-10-15 20:38:02', '2017-10-15 20:38:02');
INSERT INTO `member_api` VALUES ('53', '3', '26', 'vip118net', '7d5087b959', '0.10', null, '2017-10-19 00:42:11', '2017-10-27 22:34:10');
INSERT INTO `member_api` VALUES ('54', '4', '26', 'ceshi123', '23a5747c77', '0.00', null, '2017-10-19 02:04:41', '2017-10-19 19:58:24');
INSERT INTO `member_api` VALUES ('55', '15', '26', 'sas0001', 'a7a25d8c0b', '0.00', null, '2017-10-20 19:13:30', '2017-10-20 19:13:30');
INSERT INTO `member_api` VALUES ('56', '15', '22', 'sas0001', 'a7a25d8c0b', '0.00', null, '2017-10-20 19:14:48', '2017-10-20 19:14:48');
INSERT INTO `member_api` VALUES ('57', '15', '3', 'sas0001', 'a7a25d8c0b', '0.00', null, '2017-10-20 19:15:29', '2017-10-20 19:15:29');
INSERT INTO `member_api` VALUES ('58', '15', '10', 'sas0001', 'a7a25d8c0b', '0.00', null, '2017-10-20 19:16:38', '2017-10-20 19:16:38');
INSERT INTO `member_api` VALUES ('59', '15', '12', 'sas0001', 'a7a25d8c0b', '0.00', null, '2017-10-20 19:17:33', '2017-10-20 19:17:33');
INSERT INTO `member_api` VALUES ('60', '15', '24', 'sas0001', 'a7a25d8c0b', '0.00', null, '2017-10-20 19:18:22', '2017-10-20 19:18:22');
INSERT INTO `member_api` VALUES ('61', '15', '4', 'sas0001', 'a7a25d8c0b', '0.00', null, '2017-10-20 19:21:08', '2017-10-20 19:21:08');
INSERT INTO `member_api` VALUES ('62', '18', '4', 'ceshi8900', '87d0b0067f', '0.00', null, '2017-10-23 02:53:47', '2017-10-23 02:53:47');
INSERT INTO `member_api` VALUES ('63', '18', '22', 'ceshi8900', '87d0b0067f', '0.00', null, '2017-10-23 02:55:14', '2017-10-23 02:55:14');
INSERT INTO `member_api` VALUES ('64', '18', '3', 'ceshi8900', '87d0b0067f', '0.00', null, '2017-10-23 02:55:20', '2017-10-23 02:55:20');
INSERT INTO `member_api` VALUES ('65', '22', '3', 'aa123456', '00c3516770', '0.00', null, '2017-10-27 19:47:33', '2017-10-27 19:47:33');
INSERT INTO `member_api` VALUES ('66', '22', '10', 'aa123456', '00c3516770', '0.00', null, '2017-10-27 19:49:11', '2017-10-27 19:49:11');
INSERT INTO `member_api` VALUES ('67', '22', '4', 'aa123456', '00c3516770', '0.00', null, '2017-10-27 19:49:29', '2017-10-27 19:49:29');
INSERT INTO `member_api` VALUES ('68', '22', '6', 'aa123456', '00c3516770', '0.00', null, '2017-10-27 23:43:40', '2017-10-27 23:43:40');
INSERT INTO `member_api` VALUES ('69', '24', '3', 'qqwwee111', '270294b65e', '0.00', null, '2017-10-28 11:51:25', '2017-10-28 11:51:25');
INSERT INTO `member_api` VALUES ('70', '24', '12', 'qqwwee111', '270294b65e', '0.00', null, '2017-10-28 11:52:16', '2017-10-28 11:52:16');
INSERT INTO `member_api` VALUES ('71', '25', '3', 'aa11223344', 'c6687f0317', '0.00', null, '2017-10-28 21:36:58', '2017-10-28 21:36:58');
INSERT INTO `member_api` VALUES ('72', '25', '7', 'aa11223344', 'c6687f0317', '0.00', null, '2017-10-28 21:38:03', '2017-10-28 21:38:03');
INSERT INTO `member_api` VALUES ('73', '25', '12', 'aa11223344', 'c6687f0317', '0.00', null, '2017-10-28 21:39:38', '2017-10-28 21:39:38');
INSERT INTO `member_api` VALUES ('74', '25', '6', 'aa11223344', 'c6687f0317', '0.00', null, '2017-10-28 21:40:39', '2017-10-28 21:40:39');
INSERT INTO `member_api` VALUES ('75', '25', '4', 'aa11223344', 'c6687f0317', '0.00', null, '2017-10-28 21:41:15', '2017-10-28 21:41:15');
INSERT INTO `member_api` VALUES ('76', '25', '11', 'aa11223344', 'c6687f0317', '0.00', null, '2017-10-28 21:42:52', '2017-10-28 21:42:52');
INSERT INTO `member_api` VALUES ('77', '25', '11', 'aa11223344', 'c6687f0317', '0.00', null, '2017-10-28 21:42:53', '2017-10-28 21:42:53');
INSERT INTO `member_api` VALUES ('78', '25', '11', 'aa11223344', 'c6687f0317', '0.00', null, '2017-10-28 21:42:53', '2017-10-28 21:42:53');
INSERT INTO `member_api` VALUES ('79', '25', '11', 'aa11223344', 'c6687f0317', '0.00', null, '2017-10-28 21:42:55', '2017-10-28 21:42:55');
INSERT INTO `member_api` VALUES ('80', '27', '10', 'qqwwee1', 'ef4078a6b5', '0.00', null, '2017-10-29 07:07:06', '2017-10-29 07:07:06');
INSERT INTO `member_api` VALUES ('81', '27', '4', 'qqwwee1', 'ef4078a6b5', '0.00', null, '2017-10-29 07:07:42', '2017-10-29 07:07:42');
INSERT INTO `member_api` VALUES ('82', '27', '12', 'qqwwee1', 'ef4078a6b5', '0.00', null, '2017-10-29 07:08:55', '2017-10-29 07:08:55');
INSERT INTO `member_api` VALUES ('83', '27', '6', 'qqwwee1', 'ef4078a6b5', '0.00', null, '2017-10-29 07:11:05', '2017-10-29 07:11:05');
INSERT INTO `member_api` VALUES ('84', '27', '7', 'qqwwee1', 'ef4078a6b5', '0.00', null, '2017-10-29 07:11:13', '2017-10-29 07:11:13');
INSERT INTO `member_api` VALUES ('85', '28', '3', 's456123078', '36fdcc6856', '0.00', null, '2017-10-30 10:19:00', '2017-10-30 10:19:00');
INSERT INTO `member_api` VALUES ('86', '28', '4', 's456123078', '36fdcc6856', '0.00', null, '2017-10-30 10:20:00', '2017-10-30 10:20:00');
INSERT INTO `member_api` VALUES ('87', '28', '9', 's456123078', '36fdcc6856', '0.00', null, '2017-10-30 10:21:13', '2017-10-30 10:21:13');
INSERT INTO `member_api` VALUES ('88', '28', '10', 's456123078', '36fdcc6856', '0.00', null, '2017-10-30 10:22:15', '2017-10-30 10:22:15');
INSERT INTO `member_api` VALUES ('89', '29', '4', 't251671366', '4342bff164', '0.00', null, '2017-10-30 23:08:50', '2017-10-30 23:08:50');
INSERT INTO `member_api` VALUES ('90', '29', '10', 't251671366', '4342bff164', '0.00', null, '2017-10-30 23:09:59', '2017-10-30 23:09:59');
INSERT INTO `member_api` VALUES ('91', '30', '10', 'dior89888', 'cfc1f1910f', '0.00', null, '2017-12-14 00:59:10', '2017-12-14 00:59:10');
INSERT INTO `member_api` VALUES ('92', '30', '10', 'dior89888', 'cfc1f1910f', '0.00', null, '2017-12-14 00:59:10', '2017-12-14 00:59:10');
INSERT INTO `member_api` VALUES ('93', '27', '9', 'qqwwee1', 'ef4078a6b5', '0.00', null, '2017-12-16 10:28:45', '2017-12-16 10:28:45');
INSERT INTO `member_api` VALUES ('94', '27', '3', 'qqwwee1', 'ef4078a6b5', '0.00', null, '2017-12-16 10:39:03', '2017-12-16 10:39:03');
INSERT INTO `member_api` VALUES ('95', '31', '3', 'test000', 'ee8fe372cc', '0.00', null, '2017-12-16 11:39:14', '2017-12-16 11:39:14');
INSERT INTO `member_api` VALUES ('96', '32', '10', 'qwe154660', 'd9ac807434', '0.00', null, '2017-12-17 04:15:33', '2017-12-17 04:15:33');
INSERT INTO `member_api` VALUES ('97', '32', '9', 'qwe154660', 'd9ac807434', '0.00', null, '2017-12-17 04:16:21', '2017-12-17 04:16:21');
INSERT INTO `member_api` VALUES ('98', '32', '4', 'qwe154660', 'd9ac807434', '0.00', null, '2017-12-17 04:17:04', '2017-12-17 04:17:04');
INSERT INTO `member_api` VALUES ('99', '32', '3', 'qwe154660', 'd9ac807434', '0.00', null, '2017-12-17 04:17:31', '2017-12-17 04:17:31');
INSERT INTO `member_api` VALUES ('100', '31', '24', 'test000', 'ee8fe372cc', '0.00', null, '2017-12-17 17:28:26', '2017-12-17 17:28:26');
INSERT INTO `member_api` VALUES ('101', '31', '24', 'test000', 'ee8fe372cc', '0.00', null, '2017-12-17 17:28:26', '2017-12-17 17:28:26');
INSERT INTO `member_api` VALUES ('102', '31', '24', 'test000', 'ee8fe372cc', '0.00', null, '2017-12-17 17:28:26', '2017-12-17 17:28:26');
INSERT INTO `member_api` VALUES ('103', '31', '10', 'test000', 'ee8fe372cc', '0.00', null, '2017-12-17 17:46:23', '2017-12-17 17:46:23');
INSERT INTO `member_api` VALUES ('104', '34', '10', 'kk999999', '2ca5d8c102', '0.00', null, '2017-12-18 13:20:13', '2017-12-18 13:20:13');
INSERT INTO `member_api` VALUES ('105', '34', '4', 'kk999999', '2ca5d8c102', '0.00', null, '2017-12-18 13:25:43', '2017-12-18 13:25:43');
INSERT INTO `member_api` VALUES ('106', '34', '3', 'v1kk999999', '2ca5d8c102', '0.00', null, '2017-12-18 16:33:57', '2017-12-18 16:33:57');
INSERT INTO `member_api` VALUES ('107', '35', '21', 'e2gliuyu121', '004695e147', '0.00', null, '2017-12-18 17:28:22', '2017-12-18 17:28:22');
INSERT INTO `member_api` VALUES ('108', '36', '3', 'a474070640', 'd268de3982', '0.00', null, '2017-12-18 19:38:08', '2017-12-18 19:38:08');
INSERT INTO `member_api` VALUES ('109', '37', '4', 'dsaqwe8', 'd7d8f6b172', '0.00', null, '2017-12-19 22:46:20', '2017-12-19 22:46:20');
INSERT INTO `member_api` VALUES ('110', '37', '3', 'dsaqwe8', 'd7d8f6b172', '0.00', null, '2017-12-19 22:47:06', '2017-12-19 22:47:06');
INSERT INTO `member_api` VALUES ('111', '38', '24', 'v1jsdfsd55', '6a7e8e598f', '0.00', null, '2017-12-20 02:35:44', '2017-12-20 02:35:45');
INSERT INTO `member_api` VALUES ('112', '39', '4', 'kk95888', 'ee4f912122', '0.00', null, '2017-12-20 19:42:33', '2017-12-20 19:42:33');
INSERT INTO `member_api` VALUES ('113', '40', '3', 'ssssvip', '6a8daad4e6', '0.00', null, '2017-12-21 14:02:04', '2017-12-21 14:02:04');
INSERT INTO `member_api` VALUES ('114', '40', '26', 'v1ssssvip', '6a8daad4e6', '0.00', null, '2017-12-21 13:59:23', '2017-12-21 13:59:23');
INSERT INTO `member_api` VALUES ('115', '40', '25', 'v1ssssvip', '6a8daad4e6', '0.00', null, '2017-12-21 13:59:24', '2017-12-21 13:59:25');
INSERT INTO `member_api` VALUES ('116', '41', '3', 'v1admin21', 'eb5eac3697', '0.00', null, '2017-12-22 14:05:09', '2017-12-22 14:05:09');
INSERT INTO `member_api` VALUES ('117', '49', '10', 'a4623505', 'ce7a418a06', '0.00', null, '2018-01-03 19:11:12', '2018-01-03 19:11:12');
INSERT INTO `member_api` VALUES ('118', '49', '10', 'a4623505', 'ce7a418a06', '0.00', null, '2018-01-03 19:11:17', '2018-01-03 19:11:17');
INSERT INTO `member_api` VALUES ('119', '49', '10', 'a4623505', 'ce7a418a06', '0.00', null, '2018-01-03 19:11:23', '2018-01-03 19:11:23');
INSERT INTO `member_api` VALUES ('120', '49', '4', 'a4623505', 'ce7a418a06', '0.00', null, '2018-01-03 19:39:52', '2018-01-03 19:39:52');
INSERT INTO `member_api` VALUES ('121', '72', '3', 'loucucu', 'a98e6e2574', '0.00', null, '2018-01-08 18:15:31', '2018-01-08 18:15:31');
INSERT INTO `member_api` VALUES ('122', '72', '14', 'loucucu', 'a98e6e2574', '0.00', null, '2018-01-08 18:15:59', '2018-01-08 18:15:59');
INSERT INTO `member_api` VALUES ('123', '72', '7', 'loucucu', 'a98e6e2574', '0.00', null, '2018-01-08 18:16:32', '2018-01-08 18:16:32');
INSERT INTO `member_api` VALUES ('124', '72', '4', 'loucucu', 'a98e6e2574', '0.00', null, '2018-01-08 18:16:38', '2018-01-08 18:16:38');
INSERT INTO `member_api` VALUES ('125', '73', '3', 'loucucu11', '08eeb719cd', '0.00', null, '2018-01-08 18:19:02', '2018-01-08 18:19:02');
INSERT INTO `member_api` VALUES ('126', '73', '10', 'loucucu11', '08eeb719cd', '0.00', null, '2018-01-08 18:19:12', '2018-01-08 18:19:12');
INSERT INTO `member_api` VALUES ('127', '73', '4', 'loucucu11', '08eeb719cd', '0.00', null, '2018-01-08 18:19:21', '2018-01-08 18:19:21');
INSERT INTO `member_api` VALUES ('128', '75', '3', 'hunny123', 'f35f033411', '0.00', null, '2018-01-10 09:29:23', '2018-01-10 09:29:23');
INSERT INTO `member_api` VALUES ('129', '75', '10', 'hunny123', 'f35f033411', '0.00', null, '2018-01-10 09:29:38', '2018-01-10 09:29:38');
INSERT INTO `member_api` VALUES ('130', '75', '4', 'hunny123', 'f35f033411', '0.00', null, '2018-01-10 09:29:44', '2018-01-10 09:29:44');
INSERT INTO `member_api` VALUES ('131', '35', '3', 'liuyu121', '004695e147', '0.00', null, '2018-01-11 14:17:07', '2018-01-11 14:17:07');
INSERT INTO `member_api` VALUES ('132', '35', '11', 'liuyu121', '004695e147', '0.00', null, '2018-01-11 14:17:20', '2018-01-11 14:17:20');
INSERT INTO `member_api` VALUES ('133', '35', '23', 'liuyu121', '004695e147', '0.00', null, '2018-01-11 14:17:29', '2018-01-11 14:17:29');
INSERT INTO `member_api` VALUES ('134', '82', '22', 'shan123', 'b70808f34b', '0.00', null, '2018-01-13 19:52:59', '2018-01-13 19:52:59');
INSERT INTO `member_api` VALUES ('135', '83', '6', 'abc123123', '21284322ba', '0.00', null, '2018-01-18 11:09:50', '2018-01-18 11:09:50');
INSERT INTO `member_api` VALUES ('136', '83', '24', 'abc123123', '21284322ba', '0.00', null, '2018-01-19 15:58:14', '2018-01-19 15:58:14');
INSERT INTO `member_api` VALUES ('137', '83', '9', 'abc123123', '21284322ba', '0.00', null, '2018-01-19 15:58:33', '2018-01-19 15:58:33');
INSERT INTO `member_api` VALUES ('138', '83', '3', 'abc123123', '21284322ba', '0.00', null, '2018-01-19 16:10:29', '2018-01-19 16:10:29');
INSERT INTO `member_api` VALUES ('139', '86', '3', 'a12345678', 'c5371f1d7a', '0.00', null, '2018-01-26 00:07:04', '2018-01-26 00:07:04');
INSERT INTO `member_api` VALUES ('140', '86', '7', 'a12345678', 'c5371f1d7a', '0.00', null, '2018-01-26 00:07:13', '2018-01-26 00:07:13');

-- ----------------------------
-- Table structure for member_daili_apply
-- ----------------------------
DROP TABLE IF EXISTS `member_daili_apply`;
CREATE TABLE `member_daili_apply` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `msn_qq` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '申请状态',
  `fail_reason` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of member_daili_apply
-- ----------------------------

-- ----------------------------
-- Table structure for member_login_log
-- ----------------------------
DROP TABLE IF EXISTS `member_login_log`;
CREATE TABLE `member_login_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `ip` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '登录ip',
  `is_login` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0登录 1登出',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=374 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of member_login_log
-- ----------------------------
INSERT INTO `member_login_log` VALUES ('372', '87', '58.22.113.45', '0', '2018-02-17 15:05:35', '2018-02-17 15:05:35');
INSERT INTO `member_login_log` VALUES ('373', '88', '117.172.55.206', '0', '2018-02-19 17:21:32', '2018-02-19 17:21:32');

-- ----------------------------
-- Table structure for member_message
-- ----------------------------
DROP TABLE IF EXISTS `member_message`;
CREATE TABLE `member_message` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `is_read` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0未读1已读',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='会员-message';

-- ----------------------------
-- Records of member_message
-- ----------------------------

-- ----------------------------
-- Table structure for messages
-- ----------------------------
DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '标题 系统公告 活动公告',
  `content` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '公告内容',
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '跳转链接',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of messages
-- ----------------------------

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('1', '2014_10_12_000000_create_users_table', '1');
INSERT INTO `migrations` VALUES ('2', '2016_12_26_171107_create_member_table', '1');
INSERT INTO `migrations` VALUES ('3', '2017_04_24_103644_create_finances_table', '1');
INSERT INTO `migrations` VALUES ('4', '2017_04_24_143636_create_transfers_table', '1');
INSERT INTO `migrations` VALUES ('5', '2017_04_25_134304_create_game_record_table', '1');
INSERT INTO `migrations` VALUES ('6', '2017_04_25_163600_create_system_config_table', '1');
INSERT INTO `migrations` VALUES ('7', '2017_04_26_124242_create_activities_table', '1');
INSERT INTO `migrations` VALUES ('8', '2017_04_28_195111_create_api_tables', '1');
INSERT INTO `migrations` VALUES ('9', '2017_05_08_090943_create_roles_table', '1');
INSERT INTO `migrations` VALUES ('10', '2017_05_19_132945_create_game_list_table', '1');
INSERT INTO `migrations` VALUES ('11', '2017_07_29_125746_create_pay_records_table', '1');
INSERT INTO `migrations` VALUES ('12', '2017_08_16_195057_create_game_lists_table', '1');

-- ----------------------------
-- Table structure for pay_records
-- ----------------------------
DROP TABLE IF EXISTS `pay_records`;
CREATE TABLE `pay_records` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_no` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '订单号 唯一',
  `opeNo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '支付订单号',
  `money` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '金额',
  `member_id` int(11) NOT NULL COMMENT '用户 ID',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态',
  `payTime` timestamp NULL DEFAULT NULL COMMENT '支付时间',
  `pay_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1网银支付2扫码支付',
  `bankId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '若为银行卡支付 银行代码',
  `typeId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '若为扫码 1支付宝2微信',
  `clientIp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `before_request_result` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '1',
  `after_request_result` text COLLATE utf8mb4_unicode_ci COMMENT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pay_records_order_no_unique` (`order_no`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of pay_records
-- ----------------------------
INSERT INTO `pay_records` VALUES ('1', '201801050046268Tt15', null, '32213.00', '49', '0', null, '1', 'ICBC', null, '116.9.39.235', '{\"version\":\"1.0\",\"customerid\":null,\"notifyurl\":\"http:\\/\\/1.js886.cn\\/pay\\/notify\",\"returnurl\":\"http:\\/\\/1.js886.cn\\/pay\\/return\",\"sdorderno\":\"201801050046268Tt15\",\"total_fee\":\"32213.00\",\"paytype\":\"alipay\",\"bankcode\":\"ICBC\",\"get_code\":0,\"remark\":null,\"sign', null, '2018-01-05 00:46:26', '2018-01-05 00:46:26');
INSERT INTO `pay_records` VALUES ('2', '20180108162739x5CWy', null, '658.00', '71', '0', null, '1', 'ICBC', null, '14.116.137.168', '{\"version\":\"1.0\",\"customerid\":null,\"notifyurl\":\"http:\\/\\/4.js886.cn\\/pay\\/notify\",\"returnurl\":\"http:\\/\\/4.js886.cn\\/pay\\/return\",\"sdorderno\":\"20180108162739x5CWy\",\"total_fee\":\"658.00\",\"paytype\":\"weixin\",\"bankcode\":\"ICBC\",\"get_code\":0,\"remark\":null,\"sign\":', null, '2018-01-08 16:27:39', '2018-01-08 16:27:39');
INSERT INTO `pay_records` VALUES ('3', '20180108162750aIcCE', null, '600.00', '71', '0', null, '1', 'ICBC', null, '14.116.137.168', '{\"version\":\"1.0\",\"customerid\":null,\"notifyurl\":\"http:\\/\\/4.js886.cn\\/pay\\/notify\",\"returnurl\":\"http:\\/\\/4.js886.cn\\/pay\\/return\",\"sdorderno\":\"20180108162750aIcCE\",\"total_fee\":\"600.00\",\"paytype\":\"weixin\",\"bankcode\":\"ICBC\",\"get_code\":0,\"remark\":null,\"sign\":', null, '2018-01-08 16:27:50', '2018-01-08 16:27:50');
INSERT INTO `pay_records` VALUES ('4', '20180108181445T4vHy', null, '100.00', '72', '0', null, '1', 'ICBC', null, '183.25.168.236', '{\"version\":\"1.0\",\"customerid\":null,\"notifyurl\":\"http:\\/\\/1.js886.cn\\/pay\\/notify\",\"returnurl\":\"http:\\/\\/1.js886.cn\\/pay\\/return\",\"sdorderno\":\"20180108181445T4vHy\",\"total_fee\":\"100.00\",\"paytype\":\"weixin\",\"bankcode\":\"ICBC\",\"get_code\":0,\"remark\":null,\"sign\":', null, '2018-01-08 18:14:45', '2018-01-08 18:14:45');

-- ----------------------------
-- Table structure for recharge
-- ----------------------------
DROP TABLE IF EXISTS `recharge`;
CREATE TABLE `recharge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bill_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '交易流水号',
  `member_id` int(11) NOT NULL COMMENT '用户id',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '充值人名称、昵称',
  `money` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '充值金额',
  `payment_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '转账类型 1支付宝2微信3银行转账',
  `account` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '账户 支付宝账户 微信账户 银行卡号 例子 ： 11231@qq.com',
  `payment_desc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1待确认2充值成功3充值失败',
  `diff_money` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '赠送金额',
  `before_money` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '充值前金额',
  `after_money` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '充值后金额',
  `score` decimal(8,2) NOT NULL DEFAULT '0.00' COMMENT '积分',
  `fail_reason` text COLLATE utf8mb4_unicode_ci COMMENT '失败原因',
  `hk_at` timestamp NULL DEFAULT NULL COMMENT '客户填写的汇款时间',
  `confirm_at` timestamp NULL DEFAULT NULL COMMENT '确认转账时间',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '管理员ID',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of recharge
-- ----------------------------

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '描述',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='管理组';

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', '普通管理员', null, '2017-02-03 09:52:51', '2017-02-03 09:52:51');
INSERT INTO `roles` VALUES ('2', '高级管理员', '高级管理员', '2017-10-23 02:52:24', '2017-10-23 02:52:24');

-- ----------------------------
-- Table structure for role_router
-- ----------------------------
DROP TABLE IF EXISTS `role_router`;
CREATE TABLE `role_router` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `router` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=384 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='管理组-权限';

-- ----------------------------
-- Records of role_router
-- ----------------------------
INSERT INTO `role_router` VALUES ('170', '2', 'send_fs.index', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('169', '2', 'fs_level.index', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('168', '2', 'yj_level.destroy', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('167', '2', 'yj_level.update', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('166', '2', 'yj_level.store', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('165', '2', 'send_daili_money.store', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('164', '2', 'member_daili.destroy', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('163', '2', 'member_daili.update', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('162', '2', 'member_daili.store', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('161', '2', 'member_daili_apply.update', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('160', '2', 'member_daili_apply.show', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('159', '2', 'yj_level.index', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('158', '2', 'daili_money_log.index', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('157', '2', 'send_daili_money.index', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('156', '2', 'member_offline_game_record.index', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('155', '2', 'member_offline_dividend.index', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('154', '2', 'member_offline_drawing.index', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('153', '2', 'member_offline_recharge.index', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('152', '2', 'member_offline.index', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('151', '2', 'member_daili.index', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('150', '2', 'member_daili_apply.index', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('149', '2', 'drawing.update', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('148', '2', 'drawing.confirm', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('147', '2', 'recharge.update', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('146', '2', 'recharge.confirm', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('145', '2', 'drawing.index', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('144', '2', 'recharge.index', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('143', '2', 'member.post_assign', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('142', '2', 'member.destroy', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('141', '2', 'member.check', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('140', '2', 'member.update2', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('139', '2', 'member.update', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('138', '2', 'member.show', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('137', '2', 'transfer.index', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('136', '2', 'game_record.index', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('135', '2', 'member_login_log.index', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('134', '2', 'dividend.index', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('133', '2', 'member.index', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('132', '2', 'black_list_ip.destroy', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('131', '2', 'black_list_ip.update', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('130', '2', 'black_list_ip.store', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('129', '2', 'system_config.update', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('128', '2', 'bank_card.destroy', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('127', '2', 'bank_card.update', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('126', '2', 'bank_card.store', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('125', '2', 'role.update', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('124', '2', 'role.update', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('123', '2', 'role.store', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('122', '2', 'user.update', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('121', '2', 'user.store', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('120', '2', 'black_list_ip.index', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('119', '2', 'system_config.index', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('118', '2', 'admin_action_money_log.index', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('117', '2', 'bank_card.index', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('116', '2', 'role.index', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('115', '2', 'user.index', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('381', '1', 'send_fs.store', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('380', '1', 'fs_level.destroy', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('379', '1', 'fs_level.update', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('377', '1', 'fs.index', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('378', '1', 'fs_level.store', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('376', '1', 'send_fs.index', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('375', '1', 'fs_level.index', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('171', '2', 'fs.index', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('172', '2', 'fs_level.store', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('173', '2', 'fs_level.update', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('174', '2', 'fs_level.destroy', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('175', '2', 'send_fs.store', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('176', '2', 'activity.index', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('177', '2', 'activity.create', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('178', '2', 'activity.store', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('179', '2', 'activity.destroy', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('180', '2', 'system_notice.index', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('181', '2', 'message.index', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('182', '2', 'system_notice.store', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('183', '2', 'system_notice.update', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('184', '2', 'system_notice.destroy', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('185', '2', 'message.store', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('186', '2', 'message.update', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('187', '2', 'message.destroy', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('188', '2', 'api.index', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('189', '2', 'game_list.index', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('190', '2', 'api.store', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('191', '2', 'api.update', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('192', '2', 'api.destroy', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('193', '2', 'tcg_game_list.store', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('194', '2', 'tcg_game_list.update', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('195', '2', 'tcg_game_list.destroy', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('196', '2', 'user.personal.post', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('197', '2', 'feedback.index', '2018-01-10 22:02:57', '2018-01-10 22:02:57');
INSERT INTO `role_router` VALUES ('374', '1', 'yj_level.destroy', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('372', '1', 'yj_level.store', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('373', '1', 'yj_level.update', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('371', '1', 'send_daili_money.store', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('369', '1', 'member_daili.update', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('370', '1', 'member_daili.destroy', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('368', '1', 'member_daili.store', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('367', '1', 'member_daili_apply.update', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('365', '1', 'yj_level.index', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('366', '1', 'member_daili_apply.show', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('364', '1', 'daili_money_log.index', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('363', '1', 'send_daili_money.index', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('362', '1', 'member_offline_game_record.index', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('361', '1', 'member_offline_dividend.index', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('360', '1', 'member_offline_drawing.index', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('359', '1', 'member_offline_recharge.index', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('358', '1', 'member_offline.index', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('357', '1', 'member_daili.index', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('356', '1', 'member_daili_apply.index', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('355', '1', 'drawing.update', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('354', '1', 'drawing.confirm', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('353', '1', 'recharge.update', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('352', '1', 'recharge.confirm', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('351', '1', 'drawing.index', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('350', '1', 'recharge.index', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('349', '1', 'member.post_assign', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('348', '1', 'member.destroy', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('347', '1', 'member.check', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('346', '1', 'member.update2', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('345', '1', 'member.update', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('344', '1', 'member.show', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('343', '1', 'transfer.index', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('342', '1', 'game_record.index', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('341', '1', 'dividend.index', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('340', '1', 'member.index', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('339', '1', 'admin_action_money_log.index', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('382', '1', 'user.personal.post', '2018-01-12 01:15:59', '2018-01-12 01:15:59');
INSERT INTO `role_router` VALUES ('383', '1', 'feedback.index', '2018-01-12 01:15:59', '2018-01-12 01:15:59');

-- ----------------------------
-- Table structure for system_config
-- ----------------------------
DROP TABLE IF EXISTS `system_config`;
CREATE TABLE `system_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网站logo',
  `m_site_logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '手机站网站logo',
  `site_name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网站名称',
  `site_title` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网站标题',
  `keyword` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '关键字',
  `phone1` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '客户电话1',
  `phone2` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '客户电话2',
  `site_domain` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网站主域名',
  `qq` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'qq',
  `down_name` tinyint(4) NOT NULL DEFAULT '0',
  `down_desc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '客服链接',
  `is_maintain` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 正常1维护',
  `maintain_desc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '维护提示语',
  `active_member_money` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '活跃用户月充值金额',
  `alipay_nickname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '支付宝昵称',
  `alipay_account` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '支付宝账号',
  `alipay_qrcode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '支付宝 二维码图片',
  `wechat_nickname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '微信昵称',
  `wechat_account` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '微信账号',
  `wechat_qrcode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '微信 二维码图片',
  `is_alipay_on` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0上线1下线',
  `is_wechat_on` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0上线1下线',
  `is_bankpay_on` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0上线1下线',
  `is_thirdpay_on` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0上线1下线',
  `third_version` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '版本',
  `third_userid` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'id',
  `third_userkey` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'key',
  `third_pay_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '提交链接',
  `web_domain` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网站域名',
  `is_sms_on` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0上线1下线',
  `sms_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '短信 ID',
  `sms_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '短信 key',
  `sms_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '短信 token',
  `sms_temp_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '模板 ID',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_wapalert_on` tinyint(1) NOT NULL DEFAULT '1',
  `alertwap_img` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_alert_on` tinyint(1) NOT NULL DEFAULT '1',
  `alert_img` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_left_on` tinyint(1) NOT NULL DEFAULT '1',
  `left_img` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_right_on` tinyint(1) NOT NULL DEFAULT '1',
  `right_img` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `left_img_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `right_img_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_qq_on` tinyint(1) NOT NULL DEFAULT '0',
  `qqpay_nickname` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qqpay_account` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qqpay_qrcode` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of system_config
-- ----------------------------
INSERT INTO `system_config` VALUES ('1', '/uploads/2018-01-08/61395801c7ee6e38e048e2284f87ba5b95bd3c61.png', '/uploads/2018-01-08/871b433c208b095c70ab81a77cedeb6fec8f5cb8.png', '澳门威尼斯人', '澳门威尼斯人', null, '787936634', '787936634', '7web.xinyu886.com', '787936634', '0', null, 'http://wpa.qq.com/msgrd?v=3&uin=787936634&site=qq&menu=yes', '0', null, '200.00', null, null, null, null, null, null, '0', '0', '0', '0', '1.0', null, null, 'http://pay.1977.cm/apisubmit', null, '0', null, null, null, null, '2017-02-03 09:52:51', '2018-02-17 18:26:09', '0', '/uploads/2018-01-11/0553e28f770d1a8efbc6647f727d3b2021d8f1a3.jpg', '0', '/uploads/2018-01-11/84c1513ec2930d3a68e2cdfbd056ab8ee5780d9e.jpg', '0', '/uploads/2018-01-09/964496ba95770367a278904b222ce681d4c99360.jpg', '0', '/uploads/2018-01-09/f678beb08258cc912c8dc3931cb5655dea94f0a6.jpg', 'http://www.baidu.com', 'http://www.hao123.com', '0', 'QQ昵称', '25658545656', '/uploads/2018-01-11/6258690a1907362f86ffa5d3ba9c0503e279e6c0.jpg');

-- ----------------------------
-- Table structure for system_notice
-- ----------------------------
DROP TABLE IF EXISTS `system_notice`;
CREATE TABLE `system_notice` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '标题 系统公告 活动公告',
  `content` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '公告内容',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `on_line` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0上线 1下线',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of system_notice
-- ----------------------------
INSERT INTO `system_notice` VALUES ('1', '公告', '各位新老客户，微信支付宝可以充值了', '1', '菠菜网站', '0', '2017-10-14 22:52:39', '2017-12-18 16:37:47');

-- ----------------------------
-- Table structure for tcg_game_list
-- ----------------------------
DROP TABLE IF EXISTS `tcg_game_list`;
CREATE TABLE `tcg_game_list` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `displayStatus` tinyint(4) NOT NULL DEFAULT '0',
  `gameType` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '游戏类型',
  `gameName` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '游戏名称',
  `tcgGameCode` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '游戏代码',
  `productCode` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '产品名称',
  `platform` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '支持的平台 flash,html5',
  `productType` int(11) DEFAULT NULL COMMENT '产品编号',
  `sort` int(11) NOT NULL DEFAULT '1000' COMMENT '排序',
  `on_line` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0上线1下线',
  `client_type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pc' COMMENT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of tcg_game_list
-- ----------------------------

-- ----------------------------
-- Table structure for transfers
-- ----------------------------
DROP TABLE IF EXISTS `transfers`;
CREATE TABLE `transfers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bill_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 AG账户 2BBIN 账户 3MG账户',
  `member_id` int(11) NOT NULL COMMENT '用户ID',
  `transfer_type` tinyint(4) DEFAULT '0' COMMENT '0 转入游戏 1转出游戏',
  `type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '转换类型 1 中心账户转入MG账户',
  `money` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '用户填写的转换金额',
  `diff_money` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '差价金额，即红利',
  `real_money` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '实际转换金额',
  `before_money` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '转账前的金额',
  `after_money` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '转账后的金额',
  `transfer_in_account` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '转入账户',
  `transfer_out_account` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '转出账户',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `result` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=50 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of transfers
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_super_admin` tinyint(4) NOT NULL DEFAULT '0',
  `role_id` int(11) NOT NULL DEFAULT '1' COMMENT '角色 id 1游客',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='后台用户表';

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('3', 'admin@qq.com', 'admin@qq.com', '$2y$10$ZUsEjPqSPFp3C/udAy8oMOITMfoj/8Yvaz.PxuYMOjy.v3tZTDCL6', '1', '1', 'YyRhscEusyvqgqMndz3VDUhDstgSkbz7W9k456aoRBsItxe80gRXRvhk5vXt', '2017-02-03 09:52:51', '2018-01-12 01:11:32', null);
INSERT INTO `users` VALUES ('7', '123@qq.com', '123@qq.com', '$2y$10$FS3LnWJ/ULqcnl470MCTz.rva.EuIvpjb9f71mv.5MvixNLXt7eFS', '0', '1', 'MtN4m3PKP6Hslo29hDfs6dRb29oU7Yg7Dx2JzwwnLADIDCM0QIjXFwqwZcBU', '2018-01-12 01:08:47', '2018-01-12 01:08:47', null);

-- ----------------------------
-- Table structure for yj_level
-- ----------------------------
DROP TABLE IF EXISTS `yj_level`;
CREATE TABLE `yj_level` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `level` tinyint(4) NOT NULL DEFAULT '1' COMMENT '等级',
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '等级名称',
  `num` int(11) NOT NULL DEFAULT '0' COMMENT '活跃人数',
  `min` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '最小金额',
  `max` decimal(16,2) DEFAULT NULL COMMENT '最大金额',
  `rate` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '佣金比例',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of yj_level
-- ----------------------------
